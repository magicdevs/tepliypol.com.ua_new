$(document).ready(function(){
	/*nav_size();*/
	to_top();
	about();
	active_tab();

	if($(window).width() < 992) {
			header_scroll();
	}

	/*change category*/

	$('.category-btn').on({
		click: function(){
			var section= $(this).parents('.category');
			var s_index = section.index()-3;
            $('.owl-cat-'+s_index).trigger('destroy.owl.carousel');

			$(this).parents('.category--categories').find('.category-btn').removeClass('active');
			$(this).addClass('active');
			section.find('.owl-carousel').html(section.find('.category-tab').eq($(this).index()).html());				
console.log(s_index);
			slider(s_index);
		}
	});
	function active_tab(){
		for(var i = 0; i < $('.category').length; i++){
			var btn = $($('.category')[i]).find('.category-btn.active');
			$($('.category')[i]).find('.owl-carousel').html($($('.category')[i]).find('.category-tab').eq(btn.index()).html());
		}
		for(var i=0; i<$('.owl-categ').length; i++){
			slider(i);
		}
	};
	function slider(index){
		$('.owl-cat-'+index).owlCarousel({
		    loop: false,
		    nav: true,
		    dots: true,
            navText: ['<span class="owl-arrow transform-180"></span>', '<span class="owl-arrow"></span>'],
		    responsive:{
		        0:{
		            items:1.5,
		        	margin: 20,
		            touchDrag: true,
		            mouseDrag: true,
		   			autoWidth: true,
		        },
		        767:{
		    		items: 4,
		   			autoWidth: true,
		        	margin: 15,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        992:{
		    		items: 4,
		        	margin: 5,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        1200:{
		    		items: 4,
		    		margin: 30,
		            touchDrag: true,
		            mouseDrag: true,
		        }
		    }
		});
	}


	$('.a-t_column p, .comparison-table p, .page--about p').hyphenate();
	$('input[type^=tel]').inputmask("+38(099)999-99-99"); 
	$('input[type^=tel]').mouseover(function(){
		$(this).attr('placeholder', ' ');
	});
	/*search open/close*/
	$('#search').on('click', '.btn', function(){
		if ($('#search').hasClass('active')){
            // $('body').on('click', '#search.active button', function() {
                var url = $('base').attr('href') + 'index.php?route=product/search';
                console.log("on");
                var value = $('header #search input[name=\'search\']').val();

                if (value) {
                    url += '&search=' + encodeURIComponent(value);
                }

                location = url;
            // });
		} else {
            $('#search').toggleClass('active');
            $('.product-filter').stop().fadeOut();
		}

	});

	/*change dropdown menu img*/
	$('.dropdown-nav--menu li').mouseenter(function(){
		var active_img = $(this).index();
		$('.dropdown-nav--img img').removeClass('active');
        $('.dropdown-nav--img img').eq(active_img).addClass('active');
    });

	/*smooth scroll*/
	$("a[href^='#']").on("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 50
        }, 750);
        e.preventDefault();
        return false;
    });

	/*category info open/close*/
	$('.category--categories .icon-info').mouseout(function(){
		close_categoryInfo();
	}).mouseover(function(){
		category_infoActive(this);
	});
	$('.category--categories').on('click', '.icon-info', function(){
		if($(this).hasClass('active')){
			close_categoryInfo();
		}else{
			category_infoActive(this);
		}
	});

	$('.icon-info-y').click(function(){
		$(this).parents('.category--categories').find('.category-btn').removeClass('active');
		$(this).addClass('active');
		close_categoryInfo();
	});
	/*category info open/close*/
	$(document).on('mouseout','.icon-info-y',function(){
		$('.icon-info-y').removeClass('active');
		$('.form-info').stop().fadeOut();
	}).on('mouseover','.icon-info-y',function(){
		$(this).addClass('active');
		$(this).parent().find('.form-info').stop().fadeIn();
	});
	$('.icon-info-y').on('click', function(){
		if($('.form-info').is(':visible')){
			$('.icon-info-y').removeClass('active');
			$('.form-info').stop().fadeOut();
		}else if(!$('.form-info').is(':visible')){
			$('.icon-info-y').addClass('active');
			$('.form-info').stop().fadeIn();
		}
	});


	/*product card buttons*/
	/*model*/
	var btn;
	var modelFormsBtn;
	$('body').delegate('.btn-model-select', 'click', function(){
		openPage_form('.model-form', $('.model-form .form--table .table--content .t-row_model a'));
		btn = $(this);
		//modelFormsBtn = $('.model-form .form--table .table--content .t-row_model a');
		//console.log(modelFormsBtn);
		$('.model-form').addClass('opened');
	});

	// modelFormsBtn.click(function(e) {
	// 		e.preventDefault();
	// 		// let value = $(this).text();
	// 		// $('.btn-model-select').text(value);
	// 		console.log(true);
	// 	}); 

	$('.table--content .t-row_model a').click(function(e){
		e.preventDefault();
		close_form();
		btn.addClass('selected');
		btn.html( $(this).find('>div:nth-child(2)').text()+'<div class="arrow"><span></span><span></span></div>')
	});

	$('.btn-comparison').click(function(){
		$('.info-box--content').hide();
		$('.info-box').stop().fadeIn();
		$('.product-comparison').show();
	});
	$('.to-cart').click(function(){
		$('.info-box--content').hide();
		$('.info-box').stop().fadeIn();
		$('.product-added').show();
	});
	$('.close_info-box').click(function(){
		$('.info-box').stop().fadeOut();
		$('.info-box--content').delay(750).hide();
	});
	
	$('.menu-btn').on({
		click: function(){
			if($(window).width()<=991){
				$('.dropdown-nav').stop().slideToggle();
			}else if($(window).width()>=992){
				/*$('.menu-btn').find('.arrow').toggleClass('active');*/
				$('.dropdown-nav').stop().fadeToggle();
			}
		}
	})

	/*cart-btn on mouseover*/
	$('.cart-btn').on({
		click: function(){
			$('.cart-block').stop().fadeToggle();
			cart_scroll();
			close_burger();
			$('.product-filter').stop().fadeOut();
			$('#search').removeClass('active');
		}
	});
	$(document).on({
		mouseup: function (e){ 
			var div = $(".cart-block");
			if (!div.is(e.target) && div.has(e.target).length === 0) { 
				div.stop().fadeOut();
			}
			var m_dropdown = $(".dropdown-nav");
			if (!m_dropdown.is(e.target) && m_dropdown.has(e.target).length === 0) { 
				$('.menu-btn').find('.arrow').removeClass('active');
				m_dropdown.stop().fadeOut();
			}
		},
		scroll: function(){
			if($(window).width() < 992) {
				header_scroll();
			}
		}
	});


	/*common forms*/
	$('.feedback-btn').click(function(){
		openCommon_form('.feedback-form');
		close_burger();
		$('#search').removeClass('active');
	});

	/*close form*/
    $('.forms').on('click', '.overlay, .close-form', function(){
        close_form();
    });
    $(document).on('click','.form input[type=submit]', function(e){
        e.preventDefault();
        let mainClass = $(this).parents('.form');
        if(mainClass.hasClass('feedbackform')){
            mainClass.find('.has-error').removeClass('has-error');
            let form = mainClass;
            if (form.find(".agree input[type=checkbox]").prop("checked")) {
                $.ajax({
                    url: 'index.php?route=extension/module/IMCallMeAskMe/sendMessage',//$(".call-form").attr('action'),
                    type: 'post',
                    cache: false,
                    data: IMCallMeAskMe_collectParams(mainClass),
                    dataType: 'json',
                    beforeSend: function () {
                    },
                    complete: function () {
                    },
                    success: function (json) {

                        if (json['error']) {
                            if (json['warning']) {
                                mainClass.prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            }
                            for (i in json['messages']) {
                                var element = mainClass.find('input[name="' + i + '"], textarea[name="' + i + '"]');
                                element.addClass('has-error');
                            }
                        }
                        else {
                            $('.feedback-form').hide();
                            $('.info-form').stop().fadeIn();
                            form.find("button[type=reset]").click();
                            // info_form__change('Запрос на обратный звонок успешно отправлен', 'В скором времени с Вами свяжется оператор.');
                            return false;

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            } else {
                form.find(".agree").addClass("has-error");
            }
            return false;
        }
    });

	$('header').on('click', '.burger-btn', function(){
		$('.burger-btn, .header-menu, .tel-tabs, .dropdown-nav').toggleClass('active');
		$('body').css('overflow-y', 'hidden');
		$('#search').removeClass('active');
		$('.product-filter').stop().fadeOut();

		if(!$('.burger-btn').hasClass('active')){
			$('body').css('overflow-y', 'auto');
		}
	});

	$('.to-top').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 1000);
	});

	/*results page*/
	$('.res-row').on('click','.btn', function(){
		if($(this).hasClass('to-cart')){
			$('.info-box--content').hide();
			$('.info-box').stop().fadeIn();
			$('.product-added').show();
			return false;
		}else{
			return false;
		}
	});

	/*product page images*/
	$('.product--imgs').on('click', 'div', function(){
		var img_link = $('.product--img').find('img').attr('src');
		$('.product--img').find('img').attr('src', $(this).find('img').attr('src'));
		$(this).find('img').attr('src', img_link);
	});
    $(".btn-display").removeClass("active");

	/*display catalog*/
	$('.btn-display').click(function(){
		$('.btn-display').removeClass('active');
		$(this).addClass('active');
		if($(this).is('#grid')){
			$('.product-list').removeClass('list_list');
			$('.product-list').addClass('list_grid');
            localStorage.setItem('display', 'grid')
		}else if($(this).is('#list')){
			$('.product-list').removeClass('list_grid');
			if($(window).width()<480){
				$('.product-list').removeClass('list_list');	
			}else{
				$('.product-list').addClass('list_list');
			}
            localStorage.setItem('display', 'list')
		}else if($(this).is('#cards')){
			$('.product-list').removeClass('list_list');
			$('.product-list').removeClass('list_grid');
            localStorage.setItem('display', 'cards')
		}
	});
    console.log(localStorage.getItem('display'));
    if (localStorage.getItem('display') == 'grid') {
        $("#grid").addClass("active");
        $('.product-list').removeClass('list_list');
        $('.product-list').addClass('list_grid');
    } else if (localStorage.getItem('display') == 'list') {
        $("#list").addClass("active");
        $('.product-list').removeClass('list_grid');
        if($(window).width()<480){
            $('.product-list').removeClass('list_list');
        }else{
            $('.product-list').addClass('list_list');
        }
    } else if (localStorage.getItem('display') == 'cards') {
        $("#cards").addClass("active");
        $('.product-list').removeClass('list_list');
        $('.product-list').removeClass('list_grid');
    }


	$('.page--content').on('click', '.filter-btn', function(){
		$('.product-filter').stop().fadeToggle();
	});

	$(document).on("focusout", ".input .inp-text", function(){
        if($(this).val() != ""){
            $(this).addClass("has-content");
        }else{
            $(this).removeClass("has-content");
        }
    })
});


function header_scroll(){
	var pos = $(this).scrollTop();

	if($(window).width()>=992){
		if(pos>0){
			$('header').addClass('fixed');
			$('.header-menu').removeClass('col-md-6 col-md-offset-3 col-lg-offset-4');
			$('.header-menu').addClass('col-md-5');
		}else{
			$('header').removeClass('fixed');
			$('.header-menu').addClass('col-md-6 col-md-offset-3 col-lg-offset-4');
			$('.header-menu').removeClass('col-md-5');
		}
	}
}

var close_burger = function(){
	if($(window).width()<=991){
		$('.burger-btn, .header-menu, .tel-tabs, .dropdown-nav').removeClass('active');
		$('.menu-btn').find('.dropdown-nav').stop().slideUp();
		$('body').css('overflow-y', 'auto');
	}
}
/*floor animation*/
var glowInterval = 2000;

setInterval(function(){
	$('.floor').toggleClass('active');/*, .floor-header, .floor-404*/
}, glowInterval);

/*let navigation = $('nav').html();*/
/*navigatiion size*/
/*var nav_size = function(){
	var nav_width = $('nav').width(),
		li_width = 0,
		li_num = $('nav>ul>li').length;
	if($(window).width()+17>=992){
		$('nav>ul>li').each(function(){
			li_width+=$(this).outerWidth(true);
			var li_index = $(this).index() + 1;
			if(li_width > nav_width){
				li_num++;
				$($('nav>ul>li')[$(this).index() - 1]).after(`<li class="more-nav">Еще
					<div class="arrow"><span></span>
							<span></span></div>
							<div class="dropdown-nav">
									<span class="nav-line"></span>

									<div class="flex-row"><ul class="dropdown-nav--menu"></ul></div>
							</div></li>`);
				while(li_num>li_index){
					$('.more-nav ul').append('<li>'+ $($('nav>ul>li')[li_num-1]).html()+'</li>');
					$($('nav>ul>li')[li_num-1]).remove();
					li_num--;
				}
				return false;
			}
		});
	}else{
		$('nav').html(navigation);
	}
}*/	
var to_top = function(){
	if($('main').height() <= '2000'){
		$('.to-top').hide();
	}
	if($(window).width()<='992'){
		if($('.to-top').is(':visible')){
			$('footer').css('margin-top', '130px');
		}
	}
};

/*close category info window*/
var close_categoryInfo = function(){
	$('.category--info').stop().fadeOut();
	$('.icon-info').removeClass('active');
};
var category_infoActive = function(t){
	close_categoryInfo();
	$(t).addClass('active');
	$(t).parents('.category').find('.category--info').html($(t).parents('.category-btn').find('.text--info').html());
	$(t).parents('.category').find('.category--info').stop().fadeIn();
}
/*open form*/
var openCommon_form = function(formName){
	$('body').css('overflow-y', 'hidden');
	$('.common-forms, .common-forms .overlay, ' + formName).stop().fadeIn();
};
var openPage_form = function(formName){
	let arrow = `<div class="arrow"><span></span><span></span></div>`;
	$('body').css('overflow-y', 'hidden');
	$('.page-forms:not(.checkout-payment), .page-forms:not(.checkout-payment) .overlay, ' + formName).stop().fadeIn();
	setTimeout(function() {
		$(formName).find('.form--table').find('.table').find('.table--content').find('.t-row_model > a').click(function(e) {
			e.preventDefault();
			console.log('true');
			if($(this).find('> div').attr('data-measure') === 'м.кв') {
				console.log('work');
				$('.btn-model-select').addClass('selected').text($(this).find('> div').find('p').text()).append(arrow);
				close_form();
			}
		});
	}, 250);
};
/*close form*/
var close_form = function(){
	$('.forms .form').stop().fadeOut();
	setTimeout(function(){
		$('body').css('overflow-y', 'auto');
		$('.overlay, .forms').stop().fadeOut();
	},300)
}
/*cart scrollbar (calculate margin)*/
var cart_scroll = function(){
	if($('.cart-block--scroll').height() == 375){
		$('.cart-block--row').css('margin-right','20px');
	}
};
var order_cart_scroll = function(){
	if($('.order-form .cart-block--scroll').height() == 375){
		$('.cart-block--row').css('margin-right','20px');
	}
}();

/*style for page-about*/
var about = function(){
	var top_height 		= $('.text-top_right').outerHeight(),
		bot_height 		= $('.text-bot_left').outerHeight(),
		col_height 		= $('.bot-cols').outerHeight(),
		about_height 	= $('.page--about').outerHeight();

	if(top_height < about_height){
		console.log($('.text-bot_left').outerHeight());
		console.log(about_height);
		console.log(top_height);
		console.log(about_height - top_height + 35);
		if($(window).width()>=992){
			$('.text-bot_left').css('margin-top', '-'+ (about_height - top_height) +'px');
		}else if($(window).width()<992){
			$('.text-bot_left').css('margin-top', '-'+ (top_height+20) +'px');
		}else if($(window).width()<768){
			return false;
		}
	}else if(top_height >= about_height){
		$('.text-bot_left').css('margin-top', '-'+ (top_height-about_height) +'px');
	}
}

$(window).resize(function(){
	/*nav_size();*/
	to_top();
	cart_scroll();
	about();
	if($(window).width()<=992){
		$('body').css('overflow-y', 'auto');
	}
	if($(window).width()<480 && $('.priduct-list').hasClass('list_list')){
		$('.product-list').removeClass('list_list');	
	}
});

// $(document).ready(function() {
// 		console.log($('.category-block .category-item button'))

// 		$('.category-block .category-item button').click(function() {
// 			console.log(true)
			
// 				if($(this).hasClass('open')) {
// 							console.log(true)
// 						$(this).removeClass('open').parent().next().slideDown();
// 				} else {
// 							console.log(false)
// 						$(this).addClass('open').parent().next().slideUp();
// 				}
// 		});
// });