$(document).ready(function(){
	nav_size();
	to_top();
	about();
	active_tab();

	/*change category*/
	var floor 			= 	$('.owl-floor'),
		other 			= 	$('.owl-other'),
		thermostat		=	$('.owl-thermostat');

	$('.category-btn').on({
		click: function(){
			var section= $(this).parents('.category');

			if(section.find('.category--categories').is('#floor-tab')){
				floor.trigger('destroy.owl.carousel');
			}else if(section.find('.category--categories').is('#thermostat-tab')){
				thermostat.trigger('destroy.owl.carousel');
			}else{
				other.trigger('destroy.owl.carousel');
			}
			

			

			$(this).parents('.category--categories').find('.category-btn').removeClass('active');
			$(this).addClass('active');
			section.find('.owl-carousel').html(section.find('.category-tab').eq($(this).index()).html());				

			if(section.find('.category--categories').is('#floor-tab')){
				floorslider();
			}else if(section.find('.category--categories').is('#thermostat-tab')){
				thermostatslider();
			}else{
				otherslider();
			}
		}
	});
	function active_tab (){
		for(var i = 0; i < $('.category').length; i++){
			var btn = $($('.category')[i]).find('.category-btn.active');
			$($('.category')[i]).find('.owl-carousel').html($($('.category')[i]).find('.category-tab').eq(btn.index()).html());
		}
		floorslider();
		otherslider();
		thermostatslider();
	};
	function floorslider(){
		$('.owl-floor').owlCarousel({
		    loop: false,
		    nav: false,
		    dots: false,
		    responsive:{
		        0:{
		            items:1.5,
		        	margin: 30,
		            touchDrag: true,
		            mouseDrag: true,
		   			autoWidth: true,
		        },
		        767:{
		    		items: 4,
		   			autoWidth: true,
		        	margin: 40,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        992:{
		    		items: 4,
		        	margin: 5,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        1200:{
		    		items: 4,
		    		margin: 30,
		            touchDrag: false,
		            mouseDrag: false,
		        }
		    }
		});
	}
	function otherslider(){
		$('.owl-other').owlCarousel({
		    loop: false,
		    nav: false,
		    dots: false,
		    responsive:{
		        0:{
		            items:1.5,
		        	margin: 30,
		            touchDrag: true,
		            mouseDrag: true,
		   			autoWidth: true,
		        },
		        767:{
		    		items: 4,
		   			autoWidth: true,
		        	margin: 40,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        992:{
		   			items: 4,
		        	margin: 5,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        1200:{
		    		items: 4,
		    		margin: 30,
		            touchDrag: false,
		            mouseDrag: false,
		        }
		    }
		});
	}
	function thermostatslider(){
		$('.owl-thermostat').owlCarousel({
		    loop: false,
		    nav: false,
		    dots: false,
		    responsive:{
		        0:{
		            items:1.5,
		        	margin: 30,
		            touchDrag: true,
		            mouseDrag: true,
		   			autoWidth: true,
		        },
		        767:{
		    		items: 4,
		   			autoWidth: true,
		        	margin: 40,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        992:{
		    		items: 4,
		        	margin: 5,
		            touchDrag: true,
		            mouseDrag: true,
		        },
		        1200:{
		   			items: 4,
		    		margin: 30,
		            touchDrag: false,
		            mouseDrag: false,
		        }
		    }
		});
	}


	$('.a-t_column p, .comparison-table p, .page--about p').hyphenate();
	$('input[type^=tel]').inputmask("+38(099)999-99-99"); 
	$('input[type^=tel]').mouseover(function(){
		$(this).attr('placeholder', ' ');
	});
	/*search open/close*/
	$('#search').on('click', '.btn', function(){
		$('#search').toggleClass('active');
		$('.product-filter').stop().fadeOut();
	});

	/*change dropdown menu img*/
	$('.dropdown-nav--menu li').hover(function(){
		var active_img = $(this).index();
		$('.dropdown-nav--img img').removeClass('active');
		$($('.dropdown-nav--img img')[active_img]).addClass('active');
	});

	/*smooth scroll*/
	$("a[href^='#']").on("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 50
        }, 750);
        e.preventDefault();
        return false;
    });

	/*category info open/close*/
	$('.category--categories .icon-info').mouseout(function(){
		close_categoryInfo();
	}).mouseover(function(){
		category_infoActive(this);
	});
	$('.category--categories').on('click', '.icon-info', function(){
		if($(this).hasClass('active')){
			close_categoryInfo();
		}else{
			category_infoActive(this);
		}
	});

	$('.icon-info-y').click(function(){
		$(this).parents('.category--categories').find('.category-btn').removeClass('active');
		$(this).addClass('active');
		close_categoryInfo();
	});
	/*category info open/close*/
	$('.icon-info-y').mouseout(function(){
		$('.icon-info-y').removeClass('active');
		$('.form-info').stop().fadeOut();
	}).mouseover(function(){
		$('.icon-info-y').addClass('active');
		$('.form-info').stop().fadeIn();
	});
	$('.icon-info-y').on('click', function(){
		if($('.form-info').is(':visible')){
			$('.icon-info-y').removeClass('active');
			$('.form-info').stop().fadeOut();
		}else if(!$('.form-info').is(':visible')){
			$('.icon-info-y').addClass('active');
			$('.form-info').stop().fadeIn();
		}
	});


	/*product card buttons*/
	/*model*/
	var btn;
	$('.btn-model-select').click(function(){
		openPage_form('.model-form');
		btn = $(this);
	});
	$('.table--content .t-row_model').click(function(){
		close_form();
		btn.addClass('selected');
		btn.html( $(this).find('>div:nth-child(2)').text()+'<div class="arrow"><span></span><span></span></div>')
	});

	$('.btn-comparison').click(function(){
		$('.info-box--content').hide();
		$('.info-box').stop().fadeIn();
		$('.product-comparison').show();
	});
	$('.to-cart').click(function(){
		$('.info-box--content').hide();
		$('.info-box').stop().fadeIn();
		$('.product-added').show();
	});
	$('.close_info-box').click(function(){
		$('.info-box').stop().fadeOut();
		$('.info-box--content').delay(750).hide();
	});
	
	/*menu js*/
	$('nav').on('mouseover', '.more-nav, .menu-btn', function(){
		/*calculate grey line position*/
		if($(window).width()>=992){
			$(this).find('.arrow').addClass('active');
			if($(this).hasClass('more-nav')){
				$(this).find('.nav-line').css('right', $(this).outerWidth()/2);
			}else{
				$(this).find('.nav-line').css('left', $(this).outerWidth()/2);
			}
			$(this).find('.dropdown-nav').stop().fadeIn();
		}
	}).on('mouseout', '.more-nav, .menu-btn', function(){
		if($(window).width()>=992){
			$(this).find('.arrow').removeClass('active');
			$(this).find('.dropdown-nav').stop().fadeOut();
		}
	});
	$('nav').on('click', '.menu-btn',function(){
		if($(window).width()<=991){
			$('.menu-btn').find('.dropdown-nav').stop().slideToggle();
		}
	})

	/*cart-btn on mouseover*/
	$('.cart-btn').hover(function(){
		$('.cart-block').stop().fadeIn();
		cart_scroll();
		close_burger();
		$('.product-filter').stop().fadeOut();
		$('#search').removeClass('active');
	}, function(){
		$('.cart-block').stop().fadeOut();
	});

	/*common forms*/
	$('.feedback-btn').click(function(){
		openCommon_form('.feedback-form');
		close_burger();
		$('#search').removeClass('active');
	});

	/*close form*/
    $('.forms').on('click', '.overlay, .close-form', function(){
        close_form();
    });
    $(document).on('click','.form input[type=submit]', function(e){
    	e.preventDefault();
        let mainClass = $(this).parents('.form');
    	if(mainClass.hasClass('feedbackform')){
            mainClass.find('.has-error').removeClass('has-error');
            let form = mainClass;
            if (form.find(".agree input[type=checkbox]").prop("checked")) {
                $.ajax({
                    url: 'index.php?route=extension/module/IMCallMeAskMe/sendMessage',//$(".call-form").attr('action'),
                    type: 'post',
                    cache: false,
                    data: IMCallMeAskMe_collectParams(mainClass),
                    dataType: 'json',
                    beforeSend: function () {
                    },
                    complete: function () {
                    },
                    success: function (json) {

                        if (json['error']) {
                            if (json['warning']) {
                                mainClass.prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            }
                            for (i in json['messages']) {
                                var element = mainClass.find('input[name="' + i + '"], textarea[name="' + i + '"]');
                                element.addClass('has-error');
                            }
                        }
                        else {
                            $('.feedback-form').hide();
                            $('.info-form').stop().fadeIn();
                            form.find("button[type=reset]").click();
                            // info_form__change('Запрос на обратный звонок успешно отправлен', 'В скором времени с Вами свяжется оператор.');
                            return false;

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            } else {
                form.find(".agree").addClass("has-error");
            }
            return false;
        }
    });

	$('header').on('click', '.burger-btn', function(){
		$('.burger-btn, nav, .phones').toggleClass('active');
		$('body').css('overflow-y', 'hidden');
		$('#search').removeClass('active');
		$('.product-filter').stop().fadeOut();

		if(!$('.burger-btn').hasClass('active')){
			$('body').css('overflow-y', 'auto');
		}
	});

	$('.to-top').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 1000);
	});

	/*results page*/
	$('.res-row').on('click','.btn', function(){
		if($(this).hasClass('to-cart')){
			$('.info-box--content').hide();
			$('.info-box').stop().fadeIn();
			$('.product-added').show();
			return false;
		}else{
			return false;
		}
	});

	/*product page images*/
	$('.product--imgs').on('click', 'div', function(){
		var img_link = $('.product--img').find('img').attr('src');
		$('.product--img').find('img').attr('src', $(this).find('img').attr('src'));
		$(this).find('img').attr('src', img_link);
	});

	/*display catalog*/
	$('.btn-display').click(function(){
		$('.btn-display').removeClass('active');
		$(this).addClass('active');
		if($(this).is('#grid')){
			$('.product-list').removeClass('list_list');
			$('.product-list').addClass('list_grid');
		}else if($(this).is('#list')){
			$('.product-list').removeClass('list_grid');
			if($(window).width()<480){
				$('.product-list').removeClass('list_list');	
			}else{
				$('.product-list').addClass('list_list');
			}
		}else if($(this).is('#cards')){
			$('.product-list').removeClass('list_list');
			$('.product-list').removeClass('list_grid');
		}
	});
	$('.page--content').on('click', '.filter-btn', function(){
		$('.product-filter').stop().fadeToggle();
	});

	$(document).on("focusout", ".input .inp-text", function(){
		if($(this).val() != ""){
			$(this).addClass("has-content");
		}else{
			$(this).removeClass("has-content");
		}
	})
});


var close_burger = function(){
	if($(window).width()<=991){
		$('.burger-btn, nav, .phones').removeClass('active');
		$('.menu-btn').find('.dropdown-nav').stop().slideUp();
		$('body').css('overflow-y', 'auto');
	}
}
/*floor animation*/
var glowInterval = 1000;

setInterval(function(){
	$('.floor').toggleClass('active');/*, .floor-header, .floor-404*/
}, glowInterval);

let navigation = $('nav').html();
/*navigatiion size*/
var nav_size = function(){
	var nav_width = $('nav').width(),
		li_width = 0,
		li_num = $('nav>ul>li').length;
	if($(window).width()+17>=992){
		$('nav>ul>li').each(function(){
			li_width+=$(this).outerWidth(true);
			var li_index = $(this).index() + 1;
			if(li_width > nav_width){
				li_num++;
				$($('nav>ul>li')[$(this).index() - 1]).after(`<li class="more-nav">Еще
					<div class="arrow"><span></span>
							<span></span></div>
							<div class="dropdown-nav">
									<span class="nav-line"></span>

									<div class="flex-row"><ul class="dropdown-nav--menu"></ul></div>
							</div></li>`);
				while(li_num>li_index){
					$('.more-nav ul').append('<li>'+ $($('nav>ul>li')[li_num-1]).html()+'</li>');
					$($('nav>ul>li')[li_num-1]).remove();
					li_num--;
				}
				return false;
			}
		});
	}else{
		$('nav').html(navigation);
	}
}	
var to_top = function(){
	if($('main').height() <= '2000'){
		$('.to-top').hide();
	}
	if($(window).width()<='992'){
		if($('.to-top').is(':visible')){
			$('footer').css('margin-top', '130px');
		}
	}
};

/*close category info window*/
var close_categoryInfo = function(){
	$('.category--info').stop().fadeOut();
	$('.icon-info').removeClass('active');
};
var category_infoActive = function(t){
	close_categoryInfo();
	$(t).addClass('active');
	$(t).parents('.category').find('.category--info').html($(t).parents('.category-btn').find('.text--info').html());
	$(t).parents('.category').find('.category--info').stop().fadeIn();
}
/*close form*/
var close_form = function(){
	$('.forms .form').stop().fadeOut();
	setTimeout(function(){
		$('body').css('overflow-y', 'auto');
		$('.overlay, .forms').stop().fadeOut();
	},300)
}
/*open form*/
var openCommon_form = function(formName){
	$('body').css('overflow-y', 'hidden');
	$('.common-forms, .common-forms .overlay, ' + formName).stop().fadeIn();
};
var openPage_form = function(formName){
	$('body').css('overflow-y', 'hidden');
	$('.page-forms, .page-forms .overlay, ' + formName).stop().fadeIn();
};

/*cart scrollbar (calculate margin)*/
var cart_scroll = function(){
	if($('.cart-block--scroll').height() == 375){
		$('.cart-block--row').css('margin-right','20px');
	}
};
var order_cart_scroll = function(){
	if($('.order-form .cart-block--scroll').height() == 375){
		$('.cart-block--row').css('margin-right','20px');
	}
}();

/*style for page-about*/
var about = function(){
	var top_height 		= $('.text-top_right').outerHeight(),
		bot_height 		= $('.text-bot_left').outerHeight(),
		col_height 		= $('.bot-cols').outerHeight(),
		about_height 	= $('.page--about').outerHeight();

	if(top_height < about_height){
		if($(window).width()>=992){
			$('.text-bot_left').css('margin-top', '-'+ (top_height - 60) +'px');
		}else if($(window).width()<992){
			$('.text-bot_left').css('margin-top', '-'+ (top_height-10) +'px');
		}else if($(window).width()<768){
			return false;
		}
	}else if(top_height >= about_height){
		$('.text-bot_left').css('margin-top', '-'+ (top_height-about_height) +'px');
	}
}

$(window).resize(function(){
	nav_size();
	to_top();
	cart_scroll();
	about();
	if($(window).width()<480 && $('.priduct-list').hasClass('list_list')){
		$('.product-list').removeClass('list_list');	
	}
})