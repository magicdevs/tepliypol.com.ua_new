<?php 
 if(isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false && $_SERVER['REQUEST_METHOD'] === 'GET'){
    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')){
        if(file_exists('seoshield-client/main.php'))
        {
            include_once('seoshield-client/main.php');
            if(function_exists('seo_shield_start_cms')){
                seo_shield_start_cms();
            }
            if(function_exists('seo_shield_out_buffer')){
                ob_start('seo_shield_out_buffer');
            }
        }
    }
}
$curr_uri = $_SERVER['REQUEST_URI'];
          $cities_arr = array(
           'kiev' => ['nomin' => 'Киев','dative' => 'Киеву'],
            'nikolaev' => ['nomin' => 'Николаев','dative' => 'Николаеву'],
            'kherson' => ['nomin' => 'Херсон','dative' => 'Херсону'],
            'poltava' => ['nomin' => 'Полтава','dative' => 'Полтаве'],
            'chernigov' => ['nomin' => 'Чернигов','dative' => 'Чернигову'],
            'zhitomir' => ['nomin' => 'Житомир','dative' => 'Житомиру'],
            'rovno' => ['nomin' => 'Ровно','dative' => 'Ровно'],
            'ternopol' => ['nomin' => 'Тернополь','dative' => 'Тернополю'],
            'lutck' => ['nomin' => 'Луцк','dative' => 'Луцку'],
            'harkov' => ['nomin' => 'Харьков','dative' => 'Харькову'],
            'odessa' => ['nomin' => 'Одесса','dative' => 'Одессе'],
            'zaporozhe' => ['nomin' => 'Запорожье','dative' => 'Запорожью'],
            'lvov' => ['nomin' => 'Львов','dative' => 'Львову'],
            'krivoi-rog' => ['nomin' => 'Кривой рог','dative' => 'Кривому Рогу'],
            'vinnitca' => ['nomin' => 'Винница','dative' => 'Виннице'],
            'cherkassy' => ['nomin' => 'Черкассы','dative' => 'Черкассам'],
            'summy' => ['nomin' => 'Сумы','dative' => 'Сумам'],
            'khmelnitckii' => ['nomin' => 'Хмельницкий','dative' => 'Хмельницкому'],
            'kropivnitcki' => ['nomin' => 'Кропивницкий','dative' => 'Кропивницкому'],
            'ivano-frankovske' => ['nomin' => 'Ивано-Франковск','dative' => 'Ивано-Франковску'],
            'uzhgorod' => ['nomin' => 'Ужгород','dative' => 'Ужгороду'],
            'dnepr' => ['nomin' => 'Днепр','dative' => 'Днепру'],
        );
          $style = '<style>.cities_list span {
                        display: inline-block;
                        padding: 0px 10px 5px 5px;
                    }</style>';
          $h1_replacer = '';
          $breadcrumb_replacer = '';

          $cities_ul = '<p><div class="cities_list"><span style="font-size: 18px;">Интернет магазин «Теплый Пол» осуществляет доставку по всем городам Украины:</span> ';
          foreach ($cities_arr as $cities_slug => $cities_rus) {
              if ($curr_uri == '/'.$cities_slug){
                $h1_replacer = 'Оплата и доставка в '.$cities_rus['dative'];
                $breadcrumb_replacer = '<li><a href="/'.$cities_slug.'">'.$cities_rus['nomin'].'</a></li>';
                continue;
              } else {
                $cities_ul .= '<span style="display: inline-block;"><a href="/'.$cities_slug.'">'.$cities_rus['nomin'].'</a></span>';
              }
          } 
          $cities_ul .= '</div></p>';
          ?>

<!DOCTYPE html>
<head>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5LHXFN9');</script>

    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Интернет-магазин Теплых Полов в Кривой Рог «Ин Терм Киев»  - Интернет-магазин теплых полов Tepliypol</title>
    <?=$style?>
      
    <base href="https://www.tepliypol.com.ua/"/>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="plugins/owl.carousel.min.css">
    <link rel="stylesheet" href="plugins/owl.theme.default.min.css">
    <link rel="stylesheet"
          href="css/bootstrap-select.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="plugins/owl.carousel.min.js"></script>
        <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
        <link href="https://www.tepliypol.com.ua/image/catalog/favicon.png" rel="icon"/>
        <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/ac-style.css">
        <script src="assets/js/1-fd0765c4d3f622e8dc7a38fb5f16ca5d-nitro-combined-1580813642-8238159163ccba47480ae4e31592907c.js" type="text/javascript"></script>
        <script src="js/script.js"></script>
        <script type='text/javascript'>
(function(){ document.jivositeloaded=0;var widget_id = 'xdWRsELdgq';var d=document;var w=window;function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}//эта строка обычная для кода JivoSite
function zy(){
    //удаляем EventListeners
    if(w.detachEvent){//поддержка IE8
        w.detachEvent('onscroll',zy);
        w.detachEvent('onmousemove',zy);
        w.detachEvent('ontouchmove',zy);
        w.detachEvent('onresize',zy);
    }else {
        w.removeEventListener("scroll", zy, false);
        w.removeEventListener("mousemove", zy, false);
        w.removeEventListener("touchmove", zy, false);
        w.removeEventListener("resize", zy, false);
    }
    //запускаем функцию загрузки JivoSite
    if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}
    //Устанавливаем куку по которой отличаем первый и второй хит
    var cookie_date = new Date ( );
    cookie_date.setTime ( cookie_date.getTime()+60*60*28*1000); //24 часа для Москвы
    d.cookie = "JivoSiteLoaded=1;path=/;expires=" + cookie_date.toGMTString();
}
if (d.cookie.search ( 'JivoSiteLoaded' )<0){//проверяем, первый ли это визит на наш сайт, если да, то назначаем EventListeners на события прокрутки, изменения размера окна браузера и скроллинга на ПК и мобильных устройствах, для отложенной загрузке JivoSite.
    if(w.attachEvent){// поддержка IE8
        w.attachEvent('onscroll',zy);
        w.attachEvent('onmousemove',zy);
        w.attachEvent('ontouchmove',zy);
        w.attachEvent('onresize',zy);
    }else {
        w.addEventListener("scroll", zy, {capture: false, passive: true});
        w.addEventListener("mousemove", zy, {capture: false, passive: true});
        w.addEventListener("touchmove", zy, {capture: false, passive: true});
        w.addEventListener("resize", zy, {capture: false, passive: true});
    }
}else {zy();}
})();</script>        <style>
        .video-banner a {
            width: 100%;
            height: 100%;
        }
    </style>
        <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(84842521, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
       });
    </script>
<!--seoshield--version--site--rus-->
</head>
<body class="information-information-34">
<noscript><div><img src="https://mc.yandex.ru/watch/84842521" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LHXFN9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<input type="hidden" value=" грн" id="curRight">
<header class="fixed">
    <div class="container">
        <div class="row">
            <div class="col-xs-2 col-sm-1 hidden-md hidden-lg">
                <div class="burger">
                    <div class="burger-btn"><span></span><span></span><span></span></div>
                </div>
            </div>
            <div class="col-xs-8 col-sm-10 col-md-3 header-logo">
                                <a href="https://www.tepliypol.com.ua/" class="logo"><img src="https://www.tepliypol.com.ua/image/catalog/inTerm-logo-RGB(1).png.webp" title="Электрический и водяной теплый пол от импортера. Все виды теплого пола. "
                                                                 alt="Электрический и водяной теплый пол от импортера. Все виды теплого пола. "/></a>
                            </div>
            <div class="col-xs-4 col-sm-2 col-lg-5 col-xs-offset-0 col-lg-offset-1 header-menu col-md-5">
                <div class="row row--p-5">
                    <div class="col-md-3">
                        <div class="inline m-15">
                            <div id="search" class="input-group">
  <input type="text" name="search" value="" placeholder="Поиск" class="form-control"/>
  <div class="input-group-btn">
    <button type="submit" class="btn btn-lg"></button>
  </div>
</div>                            <div id="cart" class="cart-btn">
  <div class="product-num"><span>0</span></div>
</div>                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="menu-btn">
                                                                                    <div class="nav-btn">
                                Каталог<div class="arrow"><span></span><span></span></div>
                            </div>
                                                                                                                                                                                                                                                                                                                                                                        </div>
                    </div>
                                                            <div class="col-md-6 tel-tabs">
                        <div class="tab-content">
                                                        <a href="tel:(097) 011 61 39">(097) 011 61 39</a>
                                                        <a href="tel: (095) 144 80 18"> (095) 144 80 18</a>
                                                        <a href="tel: (063) 52 23 111"> (063) 52 23 111</a>
                                                    </div>
                    </div>
                                    </div>
                <div class="row row--p-5">
                    <nav>
                        <ul>
                                                                                                                                                                                            <li><a  href="/about_us">Преимущества</a></li>
                                                                                                                                <li><a  href="/montazh">Монтаж</a></li>
                                                                                                                                <li><a  href="/statji/">Вопросы/ответы</a></li>
                                                                                                                                <li><a  href="/akcija">Акции</a></li>
                                                                                                                                <li><a  href="/contact">Контакты</a></li>
                                                                                    </ul>
                    </nav>
                </div>
            </div>
            <div class="info-box" id="info">
                <div class="info-box--content product-added"></div>
                <div class="info-box--content product-comparison"></div>
            </div>
                                    <div  class="dropdown-nav">
                <div class="flex-row">
                    <ul class="dropdown-nav--menu">
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/kabelnyj-te/">Кабельный теплый пол в стяжку</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/tonkij-teplyj-pol-pod-plitku/">Тонкий теплый пол под плитку</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-maty-pod-plitku/">Нагревательные маты под плитку</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/teplyj-pol-pod-laminat/">Теплый пол под ламинат</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/analogovye-termostaty/">Аналоговые и цифровые термостаты для теплого пола</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/programmiruemye-termostaty-dlya-teplogo-pola/">Программируемые термостаты для теплого пола</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/meteostancii-dlya-naruzhnogo-obogreva/">Термостаты для наружного обогрева</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/obogrev-vodotokov/">Обогрев водостоков</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">Обогрев труб</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/obogrev-naruzhnyh-ploschadej/">Обогрев наружных площадей</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-paneli/">Нагревательные панели</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/termostaty-dlya-vodyanogo-pola/">Термостаты для водяного пола</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/termostaty-dlya-kotlov/">Термостаты для котлов отопления</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/termostaty-dlya-cirkulyacionnyh-nasosov/">Термостаты для циркуляционных насосов</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-kovriki/">Нагревательные коврики</a></li>
                                                <li><a href="https://www.tepliypol.com.ua/catalogs/laminat/">Ламинат</a></li>
                                            </ul>
                    <div class="dropdown-nav--img">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/HemBR-IMBR-IM-Z-min-254x254.jpg.webp" alt="Кабельный теплый пол в стяжку"
                             class="active">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/HemDR-min-254x254.jpg.webp" alt="Тонкий теплый пол под плитку"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/HemDH-min-254x254.jpg.webp" alt="Нагревательные маты под плитку"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/in-termmat-800x600-254x254.jpg.webp" alt="Теплый пол под ламинат"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/in-thermrtc70-min-254x254.JPG" alt="Аналоговые и цифровые термостаты для теплого пола"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/in-therme51-min-254x254.JPG" alt="Программируемые термостаты для теплого пола"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/eberleem52490-min-254x254.jpg.webp" alt="Термостаты для наружного обогрева"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/DAS-min-254x254.jpg.webp" alt="Обогрев водостоков"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/FS-min-254x254.jpg.webp" alt="Обогрев труб"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/kabelADPSV-254x254.jpg.webp" alt="Обогрев наружных площадей"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/s-trio_2a-254x254.jpg.webp" alt="Нагревательные панели"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/COMPUTHERMT30-254x254.jpg.webp" alt="Термостаты для водяного пола"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/COMPUTHERMTR-010-254x254.jpg.webp" alt="Термостаты для котлов отопления"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/COMPUTHERMWPR-90GD-254x254.jpg.webp" alt="Термостаты для циркуляционных насосов"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/2_50_30_k2-reg-600x600-254x254.jpg.webp" alt="Нагревательные коврики"
                             class="">
                                                <img src="https://www.tepliypol.com.ua/image/cache/catalog/BannerLaminat4-254x254.png" width="254" height="254" alt="Ламинат"
                             class="">
                                            </div>
                </div>
            </div>
                                                                                                                                                            <div id="cart">
  <div class="cart-block">
  <div class="cart-block--header">
    <div class="h3-style">Корзина товаров</div>
    <p>В корзине <span class="strong-style">0</span> товаров на сумму <span class="strong-style">0 грн</span></p>
  </div>
  <div class="cart-block--content">
    <div class="cart-block--scroll">
            <div class="cart-block--row text-center">Ваша корзина пуста!</div>
          </div>
  </div>
  <div class="cart-block--footer">
    <a href="https://www.tepliypol.com.ua/checkout/" class="btn btn-light_y">Оформить заказ</a>
    <a href="https://www.tepliypol.com.ua/cart/" class="btn btn-yellow">Перейти в корзину</a>
  </div>
</div>
</div>        </div>
    </div>
</header>
<main><div class="container">
    <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1> <?=$h1_replacer?></h1>
      <ul class="breadcrumbs">
                        <li><a href="https://www.tepliypol.com.ua/">Интернет-магазин</a></li>
                                <li><a href="https://www.tepliypol.com.ua/<?=$_SERVER['REQUEST_URI']?>"> Интернет-магазин Теплых Полов в  «Ин Терм Киев» </a></li>
                      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 page--content">
    <?=$cities_ul?>
    <address>
        <p>Магазин находится по адресу: город Киев, улица Радищева 12/16</p>
        <p>Время работы: Пн-пт: 9:00 - 18:00</p>
        <p>Cб: Выходной</p>
        <p>Вс: Выходной</p>
    </address>
<p>Телефоны для связи:&nbsp;<a href="tel:(067) 35 33 999; (095) 77 77 147; (093) 170 33 66">(067) 35 33 999; (095) 77 77 147; (093) 170 33 66</a></p>
    </div>
  </div>
  </div>
</main>
<footer>
  <div class="container">
    <!--Button to top-->
    <div class="to-top">
      <span class="btn to-top-btn"><span class="owl-arrow transform-90 black"></span></span>
      <span class="text">Наверх</span>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-5 col-md-3 organization">
        <a href="/" class="logo footer--title"><img src="img/logo-w.png" alt="logo"></a>
        <p>Электрический и водяной теплый пол от импортера. Все виды теплого пола. </p>
        <div>
            <span>г. Киев</span>
            <span>ул. Радищева 12/16</span>
        </div>
        <div>
            <span>г. Харьков</span>
            <span>пр. Московский 247</span>
      <div><span>Получить консультацию по теплому полу:</span></div>
          <div><span><a href="tel:(067) 35 33 999">(067) 35 33 999;</a> <a href="tel:(099) 251 43 41">(099) 251 43 41;</a></span></div>
      <div><span><a href="tel:(063) 522 3 111">(063) 522 3 111</a></span></div>
      <div><span>По вопросам дилерства:</span></div>
          <div><span><a href="tel:(067) 573 71 44">(067) 573 71 44;</a> <a href="tel:(095) 420 64 83">(095) 420 64 83;</a></span></div>
      <div><span><a href="tel:(093) 99 44 665">(093) 99 44 665.</a> </span></div>

        </div>
                        <a href="tel:(097) 011 61 39; (095) 144 80 18; (063) 52 23 111"><span>(097) 011 61 39; (095) 144 80 18; (063) 52 23 111</span></a>
                        <!--<div class="socials">
          <a href="" target="_blank" class="icon icon-inst-white"></a>
          <a href="" target="_blank" class="icon icon-twit-white"></a>
          <a href="" target="_blank" class="icon icon-face-white"></a>
        </div>-->
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer--title">Навигация</div>
        <ul class="nav">
                            <li><a href="https://www.tepliypol.com.ua/akcija">Акция</a></li>
                            <li><a href="https://www.tepliypol.com.ua/montazh">Монтаж</a></li>
                            <li><a href="https://www.tepliypol.com.ua/about_us">Преимущества</a></li>
                            <li><a href="https://www.tepliypol.com.ua/delivery">Доставка и оплата</a></li>
                        <li><a href="https://www.tepliypol.com.ua/contact/">Контакты</a></li>
          <li><a href="/sitemap/">Карта сайта</a></li>
        </ul>
                <!--<a href="javascript:void(0);" class="btn white-btn">Расчет стоимости</a>-->
      </div>
      <div class="col-xs-12 col-sm-5 col-md-3">
        <div class="footer--title">Представительства</div>
        <a href="https://www.tepliypol.com.ua/kiev" class="footer--article">Киев</a>
        <a href="https://www.tepliypol.com.ua/harkov" class="footer--article">Харьков</a>
        <a href="/vinnitca" class="footer--article">Винница</a>
        <a href="/dnepr" class="footer--article">Днепр</a>
        <a href="/zhitomir" class="footer--article">Житомир</a>
        <a href="/zaporozhe" class="footer--article">Запорожье</a>
        <a href="/ivano-frankovske" class="footer--article">И.Франковск</a>
        <a href="/kropivnitcki" class="footer--article">Кропивницкий</a>
        <a href="/lutck" class="footer--article">Луцк</a>
        <a href="/lvov" class="footer--article">Львов</a>
        <a href="/nikolaev" class="footer--article">Николаев</a>
        <a href="/odessa" class="footer--article">Одесса</a>
        <a href="/poltava" class="footer--article">Полтава</a>
          <a href="/rovno" class="footer--article">Ровно</a>
          <a href="/summy" class="footer--article">Суммы</a>
          <a href="/ternopol" class="footer--article">Тернополь</a>
          <a href="/uzhgorod" class="footer--article">Ужгород</a>
          <a href="/kherson" class="footer--article">Херсон</a>
          <a href="/khmelnitckii" class="footer--article">Хмельницкий</a>
          <a href="/chernigov" class="footer--article">Чернигов</a>
          <a href="/cherkassy" class="footer--article">Черкассы</a>
          <a href="/krivoi-rog" class="footer--article">Кривой Рог</a>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="footer--title">Новостная рассылка</div>
        <p>Подпишитесь на рассылку и будьте всегда в курсе последних событий</p>
        <form class="input newsletter" >
          <input class="inp-text" id="input-newsletter" type="email" name="email" placeholder="">
          <label>Электронная почта</label>
          <span id="error-msg"></span>
          <span class="focus-border"></span>

          <button type="submit" id="subcribe" class="btn send-btn"><span class="owl-arrow black"></span></button>
        </form>
        <!--a href="#" onclick="document.getElementById('pricelist').submit();"  class="download">Скачать прайс.xls</a-->
        <a href="/pricelist.xls" class="download">Скачать прайс.xls</a>
        <form id="pricelist" action="/index.php?route=tool/export_import/download" method="post" enctype="multipart/form-data" style="display:none">
          <input type="hidden" name="export_type" value="p" checked="checked" />
        </form>
        <div class="site-info">
          <p>© Теплые полы 2018</p>
        </div>
      </div>
    </div>
  </div>
    <div class="container shield__footers_module_block__wrapper"><div class="shield__footers_module_block__wrapper__card shield__footers_module_block__wrapper__shadow shield__footers_module_block__wrapper__mb-3"><div class="shield__footers_module_block__wrapper__card-body"><div class="shield__footers_module_block__wrapper__top shield__footers_module_block__wrapper__mb-3"><button data-custom-switch="false" switch-block target="#top-categories" class="shield__footers_module_block__wrapper__btn shield__footers_module_block__wrapper__btn-sm shield__footers_module_block__wrapper__btn-success shield__footers_module_block__wrapper__mr-1" type="submit">ТОП Категории</button><button data-custom-switch="true" switch-block target="#top-categories" class="shield__footers_module_block__wrapper__btn shield__footers_module_block__wrapper__btn-sm shield__footers_module_block__wrapper__btn-light shield__footers_module_block__wrapper__mr-1" type="submit">Города</button><button data-custom-switch="false" switch-block target="#top-products" class="shield__footers_module_block__wrapper__btn shield__footers_module_block__wrapper__btn-sm shield__footers_module_block__wrapper__btn-light shield__footers_module_block__wrapper__mr-1" type="submit">ТОП Товары</button><button data-custom-switch="false" switch-block target="#sentences" class="shield__footers_module_block__wrapper__btn shield__footers_module_block__wrapper__btn-sm shield__footers_module_block__wrapper__btn-light shield__footers_module_block__wrapper__mr-1" type="submit">Предложения</button></div><div class="shield__footers_module_block__wrapper__bottom"> <div class="shield__footers_module_block__wrapper__row"> <div class="shield__footers_module_block__wrapper__right shield__footers_module_block__wrapper__col-12 shield__footers_module_block__wrapper__col-12"><div block-target class="shield__footers_module_block__wrapper__card shield__footers_module_block__wrapper__shadow shield__footers_module_block__wrapper__mb-3 shield__footers_active" id="top-categories"><div class="shield__footers_module_block__wrapper__card-body"><div class="shield__footers_module_block__wrapper__row"><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><span><a href="https://www.tepliypol.com.ua/catalogs/termostaty-dlya-kotlov/">Регулятор температуры для котлов</a></span> <span city-info><a href="https://www.tepliypol.com.ua/dnepr">Днепр</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/kabelnyj-te/">Кабельный обогрев пола</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/termostaty-dlya-vodyanogo-pola/">Никополь</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/kabelnyj-te/">Электрический кабельный теплый</a></span> <span city-info><a href="https://www.tepliypol.com.ua/summy">Сумы</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/teplyj-pol-pod-laminat/">Инфракрасные полы</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/obogrev-vodotokov/">Александрия</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/teplyj-pol-pod-laminat/">Электропол под ламинат</a></span> <span city-info><a href="https://www.tepliypol.com.ua/harkov">Харьков</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-paneli/">Ик керамический нагреватель</a></span> <span city-info><a href="https://www.tepliypol.com.ua/kiev">Киев</a></span></div><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><span><a href="https://www.tepliypol.com.ua/termostaty-dlya-kotlov">Термостат для котла wifi</a></span> <span city-info><a href="https://www.tepliypol.com.ua/cherkassy">Черкассы</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">Греющие кабели</a></span> <span city-info><a href="https://www.tepliypol.com.ua/nikolaev">Николаев</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-maty-pod-plitku/">Тёплый мат</a></span> <span city-info><a href="https://www.tepliypol.com.ua/poltava">Полтава</a></span><br/><span><a href="https://www.tepliypol.com.ua/termostaty-analogovye">Механические термостаты</a></span> <span city-info><a href="https://www.tepliypol.com.ua/kropivnitcki">Кропивницкий</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/analogovye-termostaty/vid-termostatov/tsifrovye-termostaty/">Цифровые термостаты</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/termostaty-dlya-cirkulyacionnyh-nasosov/">Каменец-Подольский</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/obogrev-vodotokov/">Системы обогрева водостоков</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/meteostancii-dlya-naruzhnogo-obogreva/">Бровары</a></span></div><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><span><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-maty-pod-plitku/">Пол нагревательные маты</a></span> <span city-info><a href="https://www.tepliypol.com.ua/chernigov">Чернигов</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/programmiruemye-termostaty-dlya-teplogo-pola/vid-termostatov/programmiruemye-termostaty/">Электронный термостат</a></span> <span city-info><a href="https://www.tepliypol.com.ua/lvov">Львов</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/teplyj-pol-pod-laminat/">Пленочный подогрев пола</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-paneli/">Белая Церковь</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/programmiruemye-termostaty-dlya-teplogo-pola/vid-termostatov/termogolovki-na-radiatory/">Термоголовки на радиаторы отопления</a></span> <span city-info><a href="https://www.tepliypol.com.ua/kherson">Херсон</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">Шнур обогрева труб</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-maty-pod-plitku/">Лисичанск</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">Электроподогрев водопроводных труб</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/kabelnyj-te/">Мариуполь</a></span></div><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><span><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-maty-pod-plitku/">Нагревательные маты</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">Черновцы</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/programmiruemye-termostaty-dlya-teplogo-pola/vid-termostatov/termogolovki-na-radiatory/">Термоголовки на радиатор отопления</a></span> <span city-info><a href="https://www.tepliypol.com.ua/zhitomir">Житомир</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/teplyj-pol-pod-laminat/">Пленочные теплые полы</a></span> <span city-info><a href="https://www.tepliypol.com.ua/vinnitca">Винница</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/teplyj-pol-pod-laminat/vid-teplogo-pola-pod-laminat/aljuminievye-maty/">Алюминиевый мат</a></span> <span city-info><a href="https://www.tepliypol.com.ua/ivano-frankovske">Ивано-Франковск</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/obogrev-naruzhnyh-ploschadej/">Подогрев ступеней крыльца</a></span> <span city-info><a href="https://www.tepliypol.com.ua/lutck">Луцк</a></span><br/><span><a href="https://www.tepliypol.com.ua/catalogs/teplyj-pol-pod-laminat/">Электрополы под ламинат</a></span> <span city-info><a href="https://www.tepliypol.com.ua/catalogs/analogovye-termostaty/">Константиновка</a></span></div></div></div></div><div block-target class="shield__footers_module_block__wrapper__card shield__footers_module_block__wrapper__shadow shield__footers_module_block__wrapper__mb-3 shield__footers_inactive" id="top-products"><div class="shield__footers_module_block__wrapper__card-body"><div class="shield__footers_module_block__wrapper__row"><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><a href="https://www.tepliypol.com.ua/catalogs/in-therm-t-8-0-m2-1760-vt">In-Therm T 8,0 м2 1760 Вт</a><br/><a href="https://www.tepliypol.com.ua/catalogs/hemstedt-br-im-17">Hemstedt BR-IM 17</a><br/><a href="https://www.tepliypol.com.ua/catalogs/computherm-t30rf">COMPUTHERM T30RF</a><br/><a href="https://www.tepliypol.com.ua/catalogs/ecosun-s-12">ECOSUN S+ 12</a><br/><a href="https://www.tepliypol.com.ua/catalogs/ecosun-s-36">ECOSUN S+ 36</a><br/><a href="https://www.tepliypol.com.ua/catalogs/terneo-k2">Terneo k2</a><br/><a href="https://www.tepliypol.com.ua/catalogs/salli-512-32-8mm">Салли 512/32/8мм</a><br/><a href="https://www.tepliypol.com.ua/catalogs/tuya-zigbee">Tuya Zigbee</a></div><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><a href="https://www.tepliypol.com.ua/catalogs/computherm-q7-rf">COMPUTHERM Q7 RF</a><br/><a href="https://www.tepliypol.com.ua/catalogs/dub-original-405-32-8-mm">Дуб Оригинал 405/32/8 мм</a><br/><a href="https://www.tepliypol.com.ua/catalogs/in-therm-pwt-002-china">IN-THERM PWT 002</a><br/><a href="https://www.tepliypol.com.ua/catalogs/in-therm-pwt-003">IN-THERM PWT 003</a><br/><a href="https://www.tepliypol.com.ua/catalogs/terneo-rtp">Terneo rtp</a><br/><a href="https://www.tepliypol.com.ua/catalogs/computherm-b300">Computherm B300</a><br/><a href="https://www.tepliypol.com.ua/catalogs/warm-life-et-81-w">WARM LIFE ET 81 W</a><br/><a href="https://www.tepliypol.com.ua/catalogs/zigbee-trv">Zigbee Trv SH4</a></div><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><a href="https://www.tepliypol.com.ua/catalogs/pablo-526-32-8-mm">Пабло 526/32/8 мм</a><br/><a href="https://www.tepliypol.com.ua/catalogs/eberle-fre-525-31">Eberle FRE 525-31</a><br/><a href="https://www.tepliypol.com.ua/catalogs/warm-life">WARM LIFE</a><br/><a href="https://www.tepliypol.com.ua/catalogs/in-therm-e60">In-therm E60</a><br/><a href="https://www.tepliypol.com.ua/catalogs/ajsberg-472-32-8mm">Айсберг 472/32/8мм</a><br/><a href="https://www.tepliypol.com.ua/catalogs/inuit-551-32-8mm">Инуит 551/32/8мм</a><br/><a href="https://www.tepliypol.com.ua/catalogs/computherm-b300-rf">Computherm B300 RF</a><br/><a href="https://www.tepliypol.com.ua/catalogs/vulkan-542-32-8mm">Вулкан 542/32/8мм</a></div><div class="shield__footers_module_block__wrapper__col-3 shield__footers_module_block__wrapper__col-3"><a href="https://www.tepliypol.com.ua/catalogs/fenix-adsv-18-1500-vt-8-3-m-kv">Fenix ADSV 18/1500 Вт 8,3 м кв.</a><br/><a href="https://www.tepliypol.com.ua/catalogs/eberle-em524-89">Eberle ЕМ524 90</a><br/><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnyj-kovrik-c-podogrevom-100x100-econom">Нагревательный коврик c подогревом 100x100 Econom</a><br/><a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnyj-kovrik-c-podogrevom-50x8100-econom">Нагревательный коврик c подогревом 50x100 Econom</a><br/><a href="https://www.tepliypol.com.ua/catalogs/in-therm-e51">In-therm Е51</a><br/><a href="https://www.tepliypol.com.ua/catalogs/memfis-554-32-8mm">Мемфис 554/32/8мм</a><br/><a href="https://www.tepliypol.com.ua/catalogs/alikarte-518-32-8mm">Аликарте 518/32/8мм</a></div></div></div></div><div block-target class="shield__footers_module_block__wrapper__card shield__footers_module_block__wrapper__shadow shield__footers_module_block__wrapper__mb-3 shield__footers_inactive" id="sentences"><div class="shield__footers_module_block__wrapper__card-body"><ul style="list-style-type: none; padding: 0px;"><li>В нашем интернет-магазине Вы можете выбрать <a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">кабель для прогрева труб</a>, а также можете заказать мат для теплого пола с доставкой по Кривому Рогу.</li><li>Вас приятно удивит цена на терморегулятор для циркуляционных насосов, закажите <a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/"> кабель для труб с доставкой</a> от нашего интернет магазина по Житомиру</li><li>На нашем сайте лучшая цена на терморегулятор для газового котла, заказывайте <a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">обогревающий провод</a> с доставкой в Мелитополь.</li><li>В городе Павлоград Вы можете <a href="https://www.tepliypol.com.ua/catalogs/nagrevatelnye-maty-pod-plitku/"> купить теплый пол маты</a> в нашем магазине или посмотреть другие предложения на тёплые полы маты.</li><li>Хотите выбрать нагревательный мат теплый, либо приобрести <a href="https://www.tepliypol.com.ua/catalogs/obogrev-trub/">нагревательный кабель для труб</a>, заходите на сайт tepliypol.com.ua.</li></ul></div></div></div></div></div></div></div></div>
</footer>
<script type="text/javascript" language="javascript">
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
    $(document).ready(function() {
        //footer
        $('#subcribe').click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $('#error-msg').html('');
            var email = $('#input-newsletter').val();
            if (email == '') {
                var error = 'Введите e-mail';
            }
            if (!validateEmail(email)) {
                var error = 'Не верно введен e-mail';
            }
            if (error != null) {
                $('#error-msg').html('');
                $('#error-msg').append('<b style=\"color:red\">' + error + '</b>');
            } else {
                var dataString = 'email=' + email;
                $.ajax({
                    url: 'index.php?route=common/footer/addToNewsletter',
                    type: 'post',
                    data: dataString,
                    success: function (html) {
                        $('#error-msg').append('<b style=\"color:green\">' + html + '</b>');
                    }
                });
            }
        });
    });
    $(document).on("click",'.close_info-box',function(){
        $('.info-box').stop().fadeOut();
        $('.info-box--content').delay(750).hide();
    });
</script>

<script>
  let form = '<form action="" class="form feedbackform feedback-form_main">' +
  '<div class="same-col-width">' +
  '<div class="input">' +
  '        <input class="inp-text" type="text" name="name" placeholder="">' +
  '            <label>Введите имя</label>' +
  '            <span class="focus-border">' +
  '            <i></i>' +
  '            </span>' +
  '        </div>' +
  '        <div class="input">' +
  '        <input class="inp-text" type="tel" name="tel" placeholder="">' +
  '            <label>Введите телефон</label>' +
  '            <span class="focus-border">' +
  '            <i></i>' +
  '            </span>' +
  '        </div>' +
  '</div>' +
  '<div class="same-col-width align-end">' +
  '<div class="agree">' +
  '<input type="checkbox" id=\'agree_main\'><label for="agree_main">Даю согласие на обработку личных данных</label>' +
  '</div>' +
  '<div>' +
  '<button type="reset" class="hidden"></button>' +
  '<input type="submit" name="send" value="Отправить" class="btn btn-yellow submit">' +
  '</div>' +
  '</div>' +
  '</form>';
  $(document).ready(function () {
    if ($(".formfeed").length) {
        $(".formfeed").after(form).remove();
    }
  });


</script>

<div class="forms common-forms">
  <div class="flex-center">
    <div class="overlay"></div>
    <form action class="form feedback-form feedbackform">
      <div class="form--title">
        <div class="h2-style">Обратный звонок</div><div class="close-form"><span></span><span></span></div>
      </div>
      <div class="form--scroll">
        <div class="form--description">
          <p>                                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. Ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint.                                                                                                                                                 </p>
        </div>
        <div class="form--content">
          <div class="same-col-width">
            <div class="input">
              <input class="inp-text" type="text" name="name" placeholder="">
              <label>Введите имя</label>
              <span class="focus-border"></span>
            </div>
            <div class="input">
              <input class="inp-text" type="tel" name="tel" placeholder="">
              <label>Введите телефон</label>
              <span class="focus-border"></span>
            </div>
          </div>
          <div class="same-col-width align-end">
            <div class="agree">
              <input type="checkbox" required id='agree'><label for="agree">Даю согласие на обработку личных данных</label>
            </div>
            <div>
              <button type="reset" class="hidden"></button>
              <input type="submit" name="send" value="Отправить" class="btn btn-yellow">
            </div>
          </div>
        </div>
      </div>
    </form>
    <div class="form info-form">
      <div class="form--title">
        <div class="h2-style">Ваша заявка была удачно отправлена!</div><div class="close-form"><span></span><span></span></div>
      </div>
      <div class="form--description">
        <p>Форма обратной связи успешно отправлена. В течении нескольких минут с Вами свяжется наш оператор.</p>
      </div>
    </div>
  </div>
</div>

<div class="forms page-forms" id="msimilar">
  <div class="flex-center">
    <div class="overlay"></div>
    <div class="insert"></div>
  </div>
</div>

<div class="forms page-forms checkout-payment" id="payment">
  <div class="flex-center">
    <div class="overlay"></div>
    <div class="insert"></div>
  </div>
</div>

<style>
  .has-error{
    color: #f00!important;
    border: solid 1px #f00!important;
  }
  .has-error * {
    color: #f00!important;
  }
</style>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
<script src='plugins/jquery.hyphen.ru.min.js'></script>
<script src='plugins/jquery.inputmask.min.js'></script>
<script src="catalog/view/javascript/lazyload/jquery.lazyload.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('img.lazy-load').lazyload({
            effect: "fadeIn",
            effectspeed: 500,
            threshold: 100,
            placeholder : "catalog/view/javascript/lazyload/loading.gif"
        });
    });
</script>
<script>
    function similar(product_id) {
        $( "#msimilar .flex-center .insert" ).load( "index.php?route=extension/module/similar&product_id="+product_id );
    }
</script>

<script type="text/javascript"><!--
var live_search = {
    selector: '#search input[name=\'search\']',
    text_no_matches: 'Нет товаров, которые соответствуют критериям поиска.',
    height: '50px'
}

$(document).ready(function() {
    var html = '';
    html += '<div class="live-search table_search">';
    html += '   <div class="live-search--content">';
    html += '   </div>';
    html += '<div class="live-search--text"></div>';
    html += '</div>';

    //$(live_search.selector).parent().closest('div').after(html);
    $(live_search.selector).after(html);

    $(live_search.selector).autocomplete({
        'source': function(request, response) {
            var filter_name = $(live_search.selector).val();
            var live_search_min_length = '3';
            if (filter_name.length < live_search_min_length) {
                $('.live-search').css('display','none');
            }
            else{
                var html = '';
                html += '<div class="live-search--row">';
                html += '<img class="loading" src="catalog/view/theme/default/image/loading.gif" />';
                html += '</div>';
                $('.live-search .live-search--content').html(html);
                $('.live-search').css('display','block');
/*
*<div class="live-search table_search">
            <div class="live-search--content">
              <div class="live-search--row">


                <div class="live-search--img"><img src="img/products/4.png" alt=""></div>
                <a href="/product.html">Тонкий нагревательный мат Hemstedt DH 1,5 м кв./225 Вт</a>
                <big>3 487 грн</big>


              </div>
            </div>
            <div class="live-search--text">
              <a href="/search-result.html" class="button btn-light_y">Показать все результаты</a>
            </div>
          </div>

* */
                $.ajax({
                    url: 'index.php?route=product/live_search&filter_name=' +  encodeURIComponent(filter_name),
                    dataType: 'json',
                    success: function(result) {
                        var products = result.products;
                        $('.live-search .live-search--content *').remove();
                        $('.result-text').html('');
                        if (!$.isEmptyObject(products)) {
                            var show_image = 1;
                            var show_price = 1;
                            var show_description = 0;
                            $('.live-search--text').html('<a href="https://www.tepliypol.com.ua/search/?search='+filter_name+'" class="button btn-light_y">Показать все результаты ('+result.total+')</a>');

                            $.each(products, function(index,product) {
                                var html = '';

                                html += '<div class="live-search--row">';
                                if(product.image && show_image){
                                    html += '   <div class="live-search--img"><img src="' + product.image + '"></div>';
                                }
                                html += '<a href="' + product.url + '"><div class="product-name">'+ product.name +'</div></a>';

                                if(show_description){
                                    html += '<p>' + product.extra_info + '</p>';
                                }
                               // html += '</div>';//div s old.price /s p big new.price /big /p /div
                                if(show_price){
                                    if (product.special) {
                                        html += '   <div class="product-price"><s><big>' + product.price + '</big></s><span class="price">' + product.special + '</span class="price"></div>';
                                    } else {
                                        html += '   <big>' + product.price + '</big>';
                                    }
                                }
                                html += '</div>';
                                $('.live-search .live-search--content').append(html);
                            });
                        } else {
                            var html = '';
                            html += '<div class="live-search--row nogrid">';
                            html += live_search.text_no_matches;
                            html += '</div>';

                            $('.live-search .live-search--content').html(html);
                        }
                        // $('.live-search ul li').css('height',live_search.height);
                        $('.live-search').css('display','block');
                        return false;
                    }
                });
            }
        },
        'select': function(product) {
            $(live_search.selector).val(product.name);
        }
    });

    $(document).bind( "mouseup touchend", function(e){
        var container = $('.live-search');
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
});
//--></script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "https://www.tepliypol.com.ua",
        "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+38 (067) 35 33 999",
            "contactType": "customer service"
        },{
            "@type": "ContactPoint",
            "telephone": "+38 (099) 251 43 41",
            "contactType": "customer service"
        },{
            "@type": "ContactPoint",
            "telephone": "+38 (063) 522 3 111",
            "contactType": "customer service"
        }],
        "address" : [
            {
                "@type": "PostalAddress",
                "addressLocality": "Киев",
                "streetAddress": "ул. Радищева 12/16"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Харьков",
                "streetAddress": "проспект Московский, 247 оф.3"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Винница",
                "streetAddress": "ул. Соборная, 4"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Житомир",
                "streetAddress": "ул. Покровская, 73"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Ивано-Франковск",
                "streetAddress": "ул. Черновола 99"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Луцк",
                "streetAddress": "проспект Воли 6 оф.9"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Львов",
                "streetAddress": "ул. Княгини Ольги 120"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Николаев",
                "streetAddress": "проспект Центральный 67, оф. 420"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Одесса",
                "streetAddress": "ул. Мельницкая 3"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Полтава",
                "streetAddress": "ул. Фрунзе 146"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Сумы",
                "streetAddress": "ул. Перекопская 11"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Тернополь",
                "streetAddress": "ул. Живова 9"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Хмельницкий",
                "streetAddress": "проспект Мира, 63"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Черкассы",
                "streetAddress": "ул. Кирова 73"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Кривой Рог",
                "streetAddress": "ул. Харцызская, 15"
            }
        ]
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Store",
        "priceRange":"UAH",
        "name": "Интернет магазин “Теплый пол”",
        "image": "https://www.tepliypol.com.ua/image/catalog/inTerm-logo-RGB(1).png",
        "openingHoursSpecification": [
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница"
                ],
                "opens": "09:00",
                "closes": "18:00"
            },
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Суббота",
                    "Воскресенье"
                ],
                "opens": "10:00",
                "closes": "16:00"
            }],
        "telephone": "+38 (067) 35 33 999",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "ул. Радищева 12/16",
            "addressLocality": "Киев",
            "addressCountry": "Украина"
        }
    }
</script>
<!--ss_category_name:Интернет-магазин Теплых Полов в Кривой Рог «Ин Терм Киев»--><link rel="stylesheet" href="/seoshield-client/core/lib/footers_block.css?t=1642685847.1992"><script type="text/javascript" src="/seoshield-client/core/lib/footers_block.js"></script><!--SEOSHIELD_WARNING: selectors not matched [track_visits]--><!--SEOSHIELD_WARNING: selectors not matched [track_products_visits]--><!--URI:'//www.tepliypol.com.ua/krivoi-rog'--><!--{seo_shield_out_buffer}--><script type="text/javascript">
var nitro_xhr;
if (window.XMLHttpRequest){nitro_xhr=new XMLHttpRequest();}
else{nitro_xhr=new ActiveXObject("Microsoft.XMLHTTP");}
nitro_xhr.onreadystatechange=function(){if (nitro_xhr.readyState==4 && nitro_xhr.status==200){var nitroBarWrapper = document.createElement('div'); nitroBarWrapper.innerHTML = nitro_xhr.responseText; document.body.appendChild(nitroBarWrapper);}}
nitro_xhr.open("GET","index.php?route=tool/nitro/getwidget&cachefile=MS1lMWRlZWFjMGQyYjY2ZTM1MDdmYmEyMjRlYzQ5MTk5My5odG1s&render_time=0.19333291053772",true);
nitro_xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
nitro_xhr.send();
</script>
</body></html>