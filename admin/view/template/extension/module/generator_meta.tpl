<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-popup-checkout" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-popup-checkout" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="generator_meta_status" id="input-status" class="form-control">
                                <?php if ($generator_meta_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="background-color: lightgrey;">
                        <label class="col-sm-2 control-label"><?php echo $entry_shortcode; ?></label>
                        <div class="col-sm-10">
                            <div class="" style="margin-top: 10px;">
                                {h1} - H1 страницы
                                <br>
                                {category} - название родительской категории (если есть)
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane">
                        <ul class="nav nav-tabs" id="language">
                            <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                    <fieldset>
                                        <legend><?php echo $entry_legend_product; ?></legend>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-product-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="generator_meta_product_lang[title][<?php echo $language['language_id']; ?>]" value="<?php echo $generator_meta_product_lang['title'][$language['language_id']]; ?>" placeholder="<?php echo $entry_title; ?>" id="input-product-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-product-text<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="generator_meta_product_lang[description][<?php echo $language['language_id']; ?>]" rows="5" placeholder="<?php echo $entry_description; ?>" id="input-product-text<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($generator_meta_product_lang['description'][$language['language_id']]) ? $generator_meta_product_lang['description'][$language['language_id']] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend><?php echo $entry_legend_category; ?></legend>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-category-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="generator_meta_category_lang[title][<?php echo $language['language_id']; ?>]" value="<?php echo $generator_meta_category_lang['title'][$language['language_id']]; ?>" placeholder="<?php echo $entry_title; ?>" id="input-category-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-category-text<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="generator_meta_category_lang[description][<?php echo $language['language_id']; ?>]" rows="5" placeholder="<?php echo $entry_description; ?>" id="input-category-text<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($generator_meta_category_lang['description'][$language['language_id']]) ? $generator_meta_category_lang['description'][$language['language_id']] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend><?php echo $entry_legend_information; ?></legend>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-information-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="generator_meta_information_lang[title][<?php echo $language['language_id']; ?>]" value="<?php echo $generator_meta_information_lang['title'][$language['language_id']]; ?>" placeholder="<?php echo $entry_title; ?>" id="input-information-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-information-text<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="generator_meta_information_lang[description][<?php echo $language['language_id']; ?>]" rows="5" placeholder="<?php echo $entry_description; ?>" id="input-information-text<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($generator_meta_information_lang['description'][$language['language_id']]) ? $generator_meta_information_lang['description'][$language['language_id']] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $('#language a:first').tab('show');
    </script>
<?php echo $footer; ?>