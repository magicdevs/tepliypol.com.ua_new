<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-module" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
                <button type="button" class="btn btn-warning" data-toggle="tooltip" title="<?php echo $button_refresh; ?>" id="button-refresh"><i class="fa fa-refresh"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-general">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-module" class="form-horizontal">
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_alert; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="module_webp_status" id="input-status" class="form-control">
                                        <?php if ($module_webp_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-quality"><?php echo $entry_quality; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                          <button class="btn btn-default value-control" type="button" data-action="minus"><i class="fa fa-minus"></i></button>
                                        </span>
                                        <input type="number" name="module_webp_quality" value="<?php echo $module_webp_quality; ?>" min="1" max="100" placeholder="<?php echo $entry_quality; ?>" class="form-control text-center" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default value-control" type="button" data-action="plus"><i class="fa fa-plus"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on('click','.value-control',function(){
            var action = $(this).attr('data-action');
            var value  = parseFloat($('input[name=\'module_webp_quality\']').val());
            if (action == "plus" && value < 100) {
                value++;
            }
            if (action == "minus" && value > 1) {
                value--;
            }
            $('input[name=\'module_webp_quality\']').val(value);
        });

        $('#button-refresh').on('click', function() {
            $.ajax({
                url: 'index.php?route=extension/module/webp/refresh&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                beforeSend: function() {
                    $('#button-refresh').button('loading');
                },
                complete: function() {
                    $('#button-refresh').button('reset');
                },
            });
        });
    </script>
</div>
<?php echo $footer; ?>