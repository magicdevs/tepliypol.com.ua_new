<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
       <!-- <div class="form-group">
          <div id="map"></div>
          <style>
            #map {
              height: 400px;
              width: 100%;
              margin-top: 25px;
            }
          </style>
        </div>-->
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name2">Название магазина</label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="Название магазина" id="input-name2" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-city">Название города</label>
            <div class="col-sm-10">
              <input type="text" name="city" value="<?php echo $city; ?>" placeholder="Название города" id="input-city" class="form-control" />
            </div>
          </div>


          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address" value="<?php echo $address; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-geo">Введите координаты</label>
            <div class="col-sm-10">
              <input type="text" name="geo" value="<?php echo $geo; ?>" placeholder="Введите координаты" id="input-geo" class="form-control" />
              <?php if ($error_geo) { ?>
              <div class="text-danger"><?php echo $error_geo; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="Если несколько номеров - разделяйте через запятую ','">Номер телефона</span></label>
            <div class="col-sm-10">
              <input type="text" name="tel" value="<?php echo $tel; ?>" placeholder="Номер телефона" id="input-keyword" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="Разделитель через enter">Время работы</span></label>
            <div class="col-sm-10">
              <textarea style="min-height: 150px" type="text" name="time_work" value="<?php echo $time_work; ?>" placeholder="Время работы" id="input-q" class="form-control" ><?php echo $time_work; ?></textarea>
            </div>
          </div>
          <div class="form-group hidden">
            <label class="col-sm-2 control-label" for="input-keyword">Варианты оплаты</label>
            <div class="col-sm-10">
              <textarea style="min-height: 150px" type="text" name="cush" value="<?php echo $cush; ?>" placeholder="Варианты оплаты" id="input-keyword3" class="form-control" ><?php echo $cush; ?></textarea>
            </div>
          </div>
          <div class="form-group hidden">
            <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?> самовывоза</label>
            <div class="col-sm-10"> <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<!--<script src="/js/jquery.maskedinput.min.js"></script>

<script>
    $("input[name=tel]").mask("+380 (99) 999-99-99").attr("placeholder", "+380 (__) ___-__-__");//+380 (__) ___-__-__
</script>-->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCugTzX-TsN_KlZXXe4rzSO94a7dBBmdsk&libraries=places&callback=initAutocomplete" async defer></script>

<script>

    function initAutocomplete() {
//        debugger;
        var locate = $("input[name=geo]").val();
        var massLocate = locate.split(',');
        if (locate == '') {
            var latOne = 49.992744;
            var lngOne = 36.231788;
        } else {
            var latOne = +massLocate[0];
            var lngOne = +massLocate[1];
        }

        var map = new google.maps.Map($("#map"), {
            center: {lat: latOne, lng: lngOne},
            zoom: 14//,
            //mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.querySelector('input[name=address]');
        var searchBox = new google.maps.places.SearchBox(input);
        //map.controls[google.maps.ControlPosition.TOP_LEFT];

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];

        if (locate != ""){
            markers.push(new google.maps.Marker({
                map: map,
                //icon: icon,
                title: $("input[name=address]").val(),
                position: {lat: latOne, lng: lngOne}
            }));
        }
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));
                var coordinates =  place.geometry.location.lat()+", "+place.geometry.location.lng();
                $("input[name=geo]").val(coordinates);
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }



</script>-->

<?php echo $footer; ?>