<?php
// Heading
$_['heading_title']     = 'Генератор meta-тегов';
// Text
$_['text_extension']    = 'Расширения';
$_['text_success']      = 'Настройки успешно изменены!';
$_['text_edit']         = 'Настройки модуля';

// Entry
$_['entry_status']      = 'Статус';
$_['entry_shortcode']   = 'Шорткоды';
$_['entry_title']       = 'Title';
$_['entry_description'] = 'Description';
$_['entry_legend_product']      = 'Формула генерации для карточек товара';
$_['entry_legend_category']     = 'Формула генерации для категорий';
$_['entry_legend_information']  = 'Формула генерации для Информационных страниц';

// Error
$_['error_permission']  = 'У Вас нет прав для изменения модуля `Генератор meta-тегов`!';
