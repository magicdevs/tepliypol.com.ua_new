<?php
// Heading
$_['heading_title']    = 'SuperProducts лист модулей';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Вы успешно изменили SuperProducts модуль!';
$_['text_edit']        = 'Редактировать SuperProducts модуль';
$_['TEXT_MODULE_TYPE']   = 'Тип модуля';
$_['TEXT_SINGLE_MODULE']   = 'Единичный вывод';
$_['TEXT_TABS_MODULE']   = 'Множественный вывод';
$_['TEXT_FRONT_NAME']   = 'Название которок видят клиенты';
$_['TEXT_PRODUCT_GROUP']   = 'Группа товаров';
$_['TEXT_PRODUCTS_BY_CATEGORY']   = 'Товары из категории';
$_['TEXT_PRODUCTS_BY_MANUFACTURER']   = 'Товары из производителей';
$_['TEXT_PRODUCTS_BY_TAG']   = 'Товары по тегам';
$_['TEXT_PRODUCTS_POPULAR']   = 'Популярные товары';
$_['TEXT_PRODUCTS_RANDOM']   = 'Рандом товары';
$_['TEXT_PRODUCTS_LAST_VIEWED']   = 'Последние просматриваемые товары';
$_['TEXT_PRODUCTS_ALSO_BOUGHT']   = 'Клиенты также купили товары';
$_['TEXT_PRODUCTS_RELATED']   = 'Сопутствующие товары';
$_['TEXT_SELECT_CATEGORY']   = 'Выберете кагегорию';
$_['TEXT_FROM_ACTIVE_CATEGORY']   = 'Показать из активной категории';
$_['TEXT_DISPLAY_VIEW_ALL']   = 'Показать просмотреть все ссылки';
$_['TEXT_SELECT_BRAND']   = 'Выбрать бренд';
$_['TEXT_INPUT_TAG']   = 'Введите тег';
$_['TEXT_PRODUCT_TYPE']   = 'Тип отображаемых продуктов';
$_['TEXT_FROM_ACTIVE_BRAND']   = 'Показать из активного бренда';
$_['TEXT_SPECIAL_PRODUCTS']   = 'Акции';
$_['TEXT_LATEST_PRODUCTS']   = 'Последние';
$_['TEXT_BESTSELLER_PRODUCTS']   = 'Топ продаж';
$_['TEXT_RANDOM_PRODUCTS']   = 'Рандом';
$_['TEXT_POPULAR_PRODUCTS']   = 'Популярные';
$_['TEXT_FRONT_NAME_TAB']   = 'Название таба';
$_['TEXT_SORT_ORDER']   = 'Порядок соритировки';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_product']    = 'Товары';
$_['entry_limit']      = 'Лимит';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

// Help
$_['help_product']     = '(Автозаполнение)';

// Error
$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение модуля superproducts!';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Ширина обязательна!';
$_['error_height']     = 'Высота обязательна!';