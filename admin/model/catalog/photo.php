<?php
class ModelCatalogPhoto extends Model {
    public function addphoto($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "photo SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

        $photo_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "photo SET image = '" . $this->db->escape($data['image']) . "' WHERE photo_id = '" . (int)$photo_id . "'");
        }

        /*if (isset($data['photo_store'])) {
            foreach ($data['photo_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "photo_to_store SET photo_id = '" . (int)$photo_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'photo_id=" . (int)$photo_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('photo');*/
        $this->cache->delete('photo');
        $this->cache->delete('photo.0');

        return $photo_id;
    }

    public function editphoto($photo_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "photo SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE photo_id = '" . (int)$photo_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "photo SET image = '" . $this->db->escape($data['image']) . "' WHERE photo_id = '" . (int)$photo_id . "'");
        }

        /*$this->db->query("DELETE FROM " . DB_PREFIX . "photo_to_store WHERE photo_id = '" . (int)$photo_id . "'");

        if (isset($data['photo_store'])) {
            foreach ($data['photo_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "photo_to_store SET photo_id = '" . (int)$photo_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'photo_id=" . (int)$photo_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'photo_id=" . (int)$photo_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('photo');*/
        $this->cache->delete('photo');
        $this->cache->delete('photo.0');


    }

    public function deletephoto($photo_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "photo WHERE photo_id = '" . (int)$photo_id . "'");
        /*$this->db->query("DELETE FROM " . DB_PREFIX . "photo_to_store WHERE photo_id = '" . (int)$photo_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'photo_id=" . (int)$photo_id . "'");

        $this->cache->delete('photo');*/
        $this->cache->delete('photo');
        $this->cache->delete('photo.0');

    }

    public function getphoto($photo_id) {
//        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'photo_id=" . (int)$photo_id . "') AS keyword FROM " . DB_PREFIX . "photo WHERE photo_id = '" . (int)$photo_id . "'");
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "photo WHERE photo_id = '" . (int)$photo_id . "'");

        return $query->row;
    }

    public function getphotos($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "photo";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /*public function getphotoStores($photo_id) {
        $photo_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "photo_to_store WHERE photo_id = '" . (int)$photo_id . "'");

        foreach ($query->rows as $result) {
            $photo_store_data[] = $result['store_id'];
        }

        return $photo_store_data;
    }*/

    public function getTotalphotos() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "photo");

        return $query->row['total'];
    }
}
