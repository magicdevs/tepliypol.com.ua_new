<?php
class ModelCatalogAdress extends Model {
	public function addManufacturer($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "address_bar SET  address = '" . $this->db->escape($data['address']) . "', name = '" . $this->db->escape($data['name']) . "', city = '" . $this->db->escape($data['city']) . "', tel = '" . $this->db->escape($data['tel']) . "', time_work = '" . $this->db->escape($data['time_work']) . "', cush = '" . $this->db->escape($data['cush']) . "', geo = '" . $this->db->escape($data['geo']) . "', image = '" . $this->db->escape($data['image']) . "'");

		$id = $this->db->getLastId();
		return $id;
	}

	public function editManufacturer($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "address_bar SET  address = '" . $this->db->escape($data['address']) . "', name = '" . $this->db->escape($data['name']) . "', city = '" . $this->db->escape($data['city']) . "', tel = '" . $this->db->escape($data['tel']) . "', time_work = '" . $this->db->escape($data['time_work']) . "', cush = '" . $this->db->escape($data['cush']) . "', geo = '" . $this->db->escape($data['geo']) . "', image = '" . $this->db->escape($data['image']) . "' WHERE id = '" . (int)$id . "'");

	}

	public function deleteManufacturer($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "address_bar WHERE id = '" . (int)$id . "'");
	}

	public function getManufacturer($id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address_bar WHERE id = '" . (int)$id . "'");
		return $query->row;
	}

	public function getManufacturers() {//$data = array()
		$sql = "SELECT * FROM " . DB_PREFIX . "address_bar";


		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalManufacturers() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address_bar");

		return $query->row['total'];
	}
}
