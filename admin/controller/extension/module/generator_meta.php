<?php
class ControllerExtensionModuleGeneratorMeta extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('extension/module/generator_meta');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('generator_meta', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_shortcode'] = $this->language->get('entry_shortcode');
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_legend_product'] = $this->language->get('entry_legend_product');
        $data['entry_legend_category'] = $this->language->get('entry_legend_category');
        $data['entry_legend_information'] = $this->language->get('entry_legend_information');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/generator_meta', 'token=' . $this->session->data['token'], true)
        );

        $data['action'] = $this->url->link('extension/module/generator_meta', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        // status
        if (isset($this->request->post['generator_meta_status'])) {
            $data['generator_meta_status'] = $this->request->post['generator_meta_status'];
        } else {
            $data['generator_meta_status'] = $this->config->get('generator_meta_status');
        }

        // meta product lang
        if (isset($this->request->post['generator_meta_product_lang'])) {
            $data['generator_meta_product_lang'] = $this->request->post['generator_meta_product_lang'];
        } else {
            $data['generator_meta_product_lang'] = $this->config->get('generator_meta_product_lang');
        }

        // meta category lang
        if (isset($this->request->post['generator_meta_category_lang'])) {
            $data['generator_meta_category_lang'] = $this->request->post['generator_meta_category_lang'];
        } else {
            $data['generator_meta_category_lang'] = $this->config->get('generator_meta_category_lang');
        }

        // meta information lang
        if (isset($this->request->post['generator_meta_information_lang'])) {
            $data['generator_meta_information_lang'] = $this->request->post['generator_meta_information_lang'];
        } else {
            $data['generator_meta_information_lang'] = $this->config->get('generator_meta_information_lang');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/generator_meta', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/generator_meta')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}