<?php
// HTTP
$host = $_SERVER['HTTP_HOST'];
define('HTTP_SERVER', 'https://'.$host.'/admin/');
define('HTTP_CATALOG', 'https://'.$host.'/');

// HTTPS
define('HTTPS_SERVER', 'https://'.$host.'/admin/');
define('HTTPS_CATALOG', 'https://'.$host.'/');

// DIR
$dir = dirname(dirname(__FILE__));
//$dir = '/home/ps321793/tepliypol.com.ua/www/';
define('DIR_APPLICATION', $dir . '/admin/');
define('DIR_SYSTEM', $dir . '/system/');
define('DIR_IMAGE', $dir . '/image/');
define('DIR_LANGUAGE', $dir . '/admin/language/');
define('DIR_TEMPLATE', $dir . '/admin/view/template/');
define('DIR_CONFIG', $dir . '/system/config/');
define('DIR_CACHE', $dir . '/system/storage/cache/');
define('DIR_DOWNLOAD', $dir . '/system/storage/download/');
define('DIR_LOGS', $dir . '/system/storage/logs/');
define('DIR_MODIFICATION', $dir . '/system/storage/modification/');
define('DIR_UPLOAD', $dir . '/system/storage/upload/');
define('DIR_CATALOG', $dir . '/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'ps321793.mysql.tools');
define('DB_USERNAME', 'ps321793_pol');
define('DB_PASSWORD', '0sJAf5~~4c');
define('DB_DATABASE', 'ps321793_pol');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

/*define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '321321');
define('DB_DATABASE', 'floor');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');*/