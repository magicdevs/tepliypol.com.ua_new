<?php
class ControllerCheckoutCublCheckout extends Controller {
    private $error = array();

    public function index() {
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $this->response->redirect($this->url->link('checkout/cart'));
        }
        $this->language->load('checkout/cublcheckout');
        $data['button_remove'] 		= $this->language->get('button_remove');
        $data['text_empty'] 		= $this->language->get('text_empty');
        $data['text_continue'] 		= $this->language->get('text_continue');
        $data['title_shopping_cart'] 		= $this->language->get('title_shopping_cart');
        $this->load->model('tool/image');
        if (!empty($this->request->post['quantity'])) {
            foreach ($this->request->post['quantity'] as $key => $value) {
                $this->cart->update($key, $value);
            }
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_cart'),
            'href' => $this->url->link('checkout/cart')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('checkout/cublcheckout', '', true)
        );


        $data['cart'] = $this->url->link('checkout/cart');

        $data['heading_title']                          	= $this->language->get('heading_title');
        $data['text_delivery']                          	= $this->language->get('text_delivery');
        $data['text_deliveries']                            = $this->language->get('text_deliveries');
        $data['text_payment']                           	= $this->language->get('text_payment');
        $data['text_payments']                          	= $this->language->get('text_payments');
        $data['text_comments']                          	= $this->language->get('text_comments');
        $data['text_cart']                          		= $this->language->get('text_cart');
        $data['text_address']                           	= $this->language->get('text_address');
        $data['text_city']                          		= $this->language->get('text_city');
        $data['text_fio']                           		= $this->language->get('text_fio');
        $data['text_back_cart']                         	= $this->language->get('text_back_cart');
        $data['btn_send']                           		= $this->language->get('btn_send');
        $data['text_select']                           		= $this->language->get('text_select');
        $data['text_tel']                           		= $this->language->get('text_tel');

       

        $this->session->data['payment_address'] = [
            'country_id' => 220,
            'zone_id'    => 170
        ];

        if (isset($this->session->data['payment_address'])) {
            // Totals
            $totals = array();
            $taxes = $this->cart->getTaxes();
            $total = 0;

            // Because __call can not keep var references so we put them into an array.
            $total_data = array(
                'totals' => &$totals,
                'taxes'  => &$taxes,
                'total'  => &$total
            );

            $this->load->model('extension/extension');

            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            // Payment Methods
            $method_data = array();

            $this->load->model('extension/extension');

            $results = $this->model_extension_extension->getExtensions('payment');


            $recurring = $this->cart->hasRecurringProducts();

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/payment/' . $result['code']);

                    $method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

                    if ($method) {
                        if ($recurring) {
                            if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
                                $method_data[$result['code']] = $method;
                            }
                        } else {
                            $method_data[$result['code']] = $method;
                        }
                    }
                }
            }

            $sort_order = array();

            foreach ($method_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_data);


            $this->session->data['payment_methods'] = $method_data;
        }
        // print_r($method); die();

        if (isset($this->session->data['payment_methods'])) {
            $data['payment_methods'] = $this->session->data['payment_methods'];
        } else {
            $data['payment_methods'] = array();
        }

        if (isset($this->session->data['payment_method']['code'])) {
            $data['code_payment_methods'] = $this->session->data['payment_method']['code'];
        } else {
            $data['code_payment_methods'] = '';
        }

        $this->session->data['shipping_address'] = [
            'country_id' => 220,
            'zone_id'    => 170
        ];

        if (isset($this->session->data['shipping_address'])) {
            // Shipping Methods
            $method_data = array();

            $this->load->model('extension/extension');

            $results = $this->model_extension_extension->getExtensions('shipping');

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/shipping/' . $result['code']);

                    $quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

                    if ($quote) {
                        $method_data[$result['code']] = array(
                            'title'      => $quote['title'],
                            'quote'      => $quote['quote'],
                            'sort_order' => $quote['sort_order'],
                            'error'      => $quote['error']
                        );
                    }
                }
            }

            $sort_order = array();

            foreach ($method_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_data);

            $this->session->data['shipping_methods'] = $method_data;
        }

        if (isset($this->session->data['shipping_methods'])) {
            $data['shipping_methods'] = $this->session->data['shipping_methods'];
        } else {
            $data['shipping_methods'] = array();
        }
        if (isset($this->session->data['shipping_method']['code'])) {
            $data['code_shipping_method'] = $this->session->data['shipping_method']['code'];
        } else {
            $data['code_shipping_method'] = '';
        }



        if(isset($this->session->data['person']['fio'])){
            if(!empty($this->session->data['person']['fio'])) {
                $data['fio'] = $this->session->data['person']['fio'];
            } else {
                $data['fio'] = '';
            }
        } else {
            $data['fio'] = '';
        }

        if(isset($this->session->data['person']['city'])){
            if(!empty($this->session->data['person']['city'])) {
                $data['city'] = $this->session->data['person']['city'];
            } else {
                $data['city'] = '';
            }
        } else {
            $data['city'] = '';
        }

        if(isset($this->session->data['person']['address'])){
            if(!empty($this->session->data['person']['address'])) {
                $data['address'] = $this->session->data['person']['address'];
            } else {
                $data['address'] = '';
            }
        } else {
            $data['address'] = '';
        }

        if(isset($this->session->data['person']['tel'])){
            if(!empty($this->session->data['person']['tel'])) {
                $data['tel'] = $this->session->data['person']['tel'];
            } else {
                $data['tel'] = '';
            }
        } else {
            $data['tel'] = '';
        }

        if(isset($this->session->data['person']['comment'])){
            if(!empty($this->session->data['person']['comment'])) {
                $data['comment'] = $this->session->data['person']['comment'];
            } else {
                $data['comment'] = '';
            }
        } else {
            $data['comment'] = '';
        }


// мб дальше не нужно нет времени

        $data['lang_id'] 									= $this->config->get('config_language_id');
        $data['text_min_price'] 							= $this->language->get('text_min_price');
        $data['button_send'] 								= $this->language->get('button_send');
        $data['icon_send_fastorder'] 						= $this->config->get('config_icon_send_fastorder');
        $data['background_button_send_fastorder'] 			= $this->config->get('config_background_button_send_fastorder');
        $data['background_button_open_form_send_order_hover'] = $this->config->get('config_background_button_open_form_send_order_hover');
        $data['background_button_send_fastorder_hover'] 	= $this->config->get('config_background_button_send_fastorder_hover');
        $data['background_button_open_form_send_order'] 	= $this->config->get('config_background_button_open_form_send_order');
        $data['icon_open_form_send_order'] 					= $this->config->get('config_icon_open_form_send_order');
        $data['icon_open_form_send_order_size'] 			= $this->config->get('config_icon_open_form_send_order_size');
        $data['color_button_open_form_send_order'] 			= $this->config->get('config_color_button_open_form_send_order');
        $data['config_any_text_at_the_top'] 				= $this->config->get('config_any_text_at_the_top');
        $data['config_text_open_form_send_order'] 			= $this->config->get('config_text_open_form_send_order');
        $data['any_text_at_the_bottom_color'] 				= $this->config->get('config_any_text_at_the_bottom_color');
        $data['img_fastorder'] 								= $this->config->get('config_img_fastorder');
        $data['mask_phone_number'] 							= $this->config->get('config_mask_phone_number');
        $data['placeholder_phone_number'] 					= $this->config->get('config_placeholder_phone_number');
        $data['config_text_before_button_send'] 			= $this->config->get('config_text_before_button_send');
        $data['config_any_text_at_the_bottom'] 				= $this->config->get('config_any_text_at_the_bottom');
        $data['continue_shopping'] 							= $this->language->get('continue_shopping');
        $data['config_title_popup_cublcheckout'] 				= $this->config->get('config_title_popup_cublcheckout');
        $data['config_fields_firstname_requared'] 			= $this->config->get('config_fields_firstname_requared');
        $data['config_fields_phone_requared'] 				= $this->config->get('config_fields_phone_requared');
        $data['config_fields_email_requared'] 				= $this->config->get('config_fields_email_requared');
        $data['config_fields_comment_requared'] 			= $this->config->get('config_fields_comment_requared');
        $data['config_placeholder_fields_firstname'] 		= $this->config->get('config_placeholder_fields_firstname');
        $data['config_placeholder_fields_phone'] 			= $this->config->get('config_placeholder_fields_phone');
        $data['config_placeholder_fields_email'] 			= $this->config->get('config_placeholder_fields_email');
        $data['config_placeholder_fields_comment'] 			= $this->config->get('config_placeholder_fields_comment');

        $data['on_off_fields_firstname'] 					= $this->config->get('config_on_off_fields_firstname');
        $data['on_off_fields_phone'] 						= $this->config->get('config_on_off_fields_phone');
        $data['on_off_fields_comment'] 						= $this->config->get('config_on_off_fields_comment');
        $data['on_off_fields_email'] 						= $this->config->get('config_on_off_fields_email');


        if ($this->config->get('config_cart_weight')) {
            $data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
        } else {
            $data['weight'] = '';
        }
        $this->load->model('tool/image');
        $this->load->model('tool/upload');
        $this->load->model('catalog/product');

        $data['products'] = array();

        $products = $this->cart->getProducts();

        foreach ($products as $product) {

            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
            }

            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
            } else {
                $image = '';
            }

            $option_data = array();

            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            // Display prices
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            // Display prices
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']);
            } else {
                $total = false;
            }

            $recurring = '';

            if ($product['recurring']) {
                $frequencies = array(
                    'day'        => $this->language->get('text_day'),
                    'week'       => $this->language->get('text_week'),
                    'semi_month' => $this->language->get('text_semi_month'),
                    'month'      => $this->language->get('text_month'),
                    'year'       => $this->language->get('text_year'),
                );

                if ($product['recurring']['trial']) {
                    $recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
                }

                if ($product['recurring']['duration']) {
                    $recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                } else {
                    $recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                }
            }
            $product_info = $this->model_catalog_product->getProduct($product['product_id']);
            $data['products'][] = array(
                'product_id' => $product['product_id'],
                'cart_id'   => $product['cart_id'],
                'thumb'     => $image,
                'name'      => $product['name'],
                'location' => $product_info['location'],
                'manufacturer'     => $product_info['manufacturer'],
                'model'     => $product['model'],
                'option'    => $option_data,
                'recurring' => $recurring,
                'quantity'  => $product['quantity'],
                'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                'price'     => $price,
                'price_anal' => $product['price'],
                'total'     => $total,
                'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );
        }

        // Gift Voucher
        $data['vouchers'] = array();

        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $key => $voucher) {
                $data['vouchers'][] = array(
                    'key'         => $key,
                    'description' => $voucher['description'],
                    'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
                    'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
                );
            }
        }

        // Totals
        $this->load->model('extension/extension');

        $totals = array();
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = array(
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total
        );

        // Display prices
        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = array();

            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);
        }
        $data['text_total_qucik_ckeckout'] = sprintf($this->language->get('text_total_qucik_ckeckout'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
        $data['totals'] = array();
        $data['total_order'] = $total;
        foreach ($totals as $total) {
            $data['totals'][] = array(
                'title' => $total['title'],
                'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
            );
        }




        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['action'])) {

            if ($this->validate()) {
                $data = array();
                if (isset($this->request->post['name_fastorder'])) {
                    $data['name_fastorder'] = $this->request->post['name_fastorder'];
                } else {
                    $data['name_fastorder'] = '';
                }
                if (isset($this->request->post['phone'])) {
                    $data['phone'] = $this->request->post['phone'];
                } else {
                    $data['phone'] = '';
                }
                if (isset($this->request->post['comment_buyer'])) {
                    $data['comment_buyer'] = $this->request->post['comment_buyer'];
                } else {
                    $data['comment_buyer'] = '';
                }
                if (isset($this->request->post['email_buyer'])) {
                    $data['email_buyer'] = $this->request->post['email_buyer'];
                } else {
                    $data['email_buyer'] = '';
                }
                if (isset($this->request->post['url_site'])) {
                    $data['url_site'] = $this->request->post['url_site'];
                } else {
                    $data['url_site'] = '';
                }
                if (isset($this->request->post['price_shipping_value'])) {
                    $data['price_shipping_value'] = $this->request->post['price_shipping_value'];
                } else {
                    $data['price_shipping_value'] = '';
                }
                if (isset($this->request->post['price_shipping_text'])) {
                    $data['price_shipping_text'] = $this->request->post['price_shipping_text'];
                } else {
                    $data['price_shipping_text'] = '';
                }
                if (isset($this->request->post['shipping_title'])) {
                    $data['shipping_title'] = $this->request->post['shipping_title'];
                } else {
                    $data['shipping_title'] = '';
                }
                if (isset($this->request->post['shipping_code_cublcheckout'])) {
                    $data['shipping_code_cublcheckout'] = $this->request->post['shipping_code_cublcheckout'];
                } else {
                    $data['shipping_code_cublcheckout'] = '';
                }
                if (isset($this->request->post['tax_class_id_total'])) {
                    $data['tax_class_id_total'] = $this->request->post['tax_class_id_total'];
                } else {
                    $data['tax_class_id_total'] = '';
                }
                if (isset($this->request->post['payment_title'])) {
                    $data['payment_title'] = $this->request->post['payment_title'];
                } else {
                    $data['payment_title'] = '';
                }
                if (isset($this->request->post['payment_code_cublcheckout'])) {
                    $data['payment_code_cublcheckout'] = $this->request->post['payment_code_cublcheckout'];
                } else {
                    $data['payment_code_cublcheckout'] = '';
                }

                $order_data = array();

                $totals = array();
                $taxes = $this->cart->getTaxes();
                $total = 0;

                // Because __call can not keep var references so we put them into an array.
                $total_data = array(
                    'totals' => &$totals,
                    'taxes'  => &$taxes,
                    'total'  => &$total
                );

                $this->load->model('extension/extension');

                $sort_order = array();

                $results = $this->model_extension_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('extension/total/' . $result['code']);

                        // We have to put the totals in an array so that they pass by reference.
                        $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                    }
                }

                $sort_order = array();

                foreach ($totals as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $totals);

                $order_data['totals'] = $totals;


                $this->load->model('tool/image');

                foreach ($this->cart->getProducts() as $product) {
                    if ($product['image']) {
                        $image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
                    } else {
                        $image = '';
                    }
                    $option_data = array();

                    foreach ($product['option'] as $option) {
                        $option_data[] = array(
                            'product_option_id'       => $option['product_option_id'],
                            'product_option_value_id' => $option['product_option_value_id'],
                            'option_id'               => $option['option_id'],
                            'option_value_id'         => $option['option_value_id'],
                            'name'                    => $option['name'],
                            'value'                   => $option['value'],
                            'type'                    => $option['type']
                        );
                    }

                    $products_data[] = array(
                        'product_id' => $product['product_id'],
                        'thumb'     => $image,
                        'name'       => $product['name'],
                        'model'      => $product['model'],
                        'option'     => $option_data,
                        'download'   => $product['download'],
                        'quantity'   => $product['quantity'],
                        'subtract'   => $product['subtract'],
                        'price'      => $product['price'],
                        'total'      => $product['total'],
                        'price_fast' => $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')),
                        'total_fast' => $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'],
                        'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
                        'reward'     => $product['reward']
                    );
                }
                $data['products'] = $products_data;
                $data['total'] = $order_data;
                /*************************/
                $data['language_id'] = $this->config->get('config_language_id');
                $data['currency_id'] = $this->currency->getId($this->session->data['currency']);
                $data['currency_code'] = $this->session->data['currency'];
                $data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
                $data['ip'] = $this->request->server['REMOTE_ADDR'];
                $data['ip_store'] = $this->request->server['REMOTE_ADDR'];
                $data['store_id'] = $this->config->get('config_store_id');
                $data['store_name'] = $this->config->get('config_name');
                $data['customer_id'] = 0;
                $data['customer_group_id'] = 1;
                $data['config_tax'] = $this->config->get('config_tax');
                $data['store_url'] = HTTP_SERVER;

                if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                    $data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
                } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                    $data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
                } else {
                    $data['forwarded_ip'] = '';
                }

                if (isset($this->request->server['HTTP_USER_AGENT'])) {
                    $data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
                } else {
                    $data['user_agent'] = '';
                }

                if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                    $data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
                } else {
                    $data['accept_language'] = '';
                }
                if (isset($this->request->post['total_order'])) {
                    $data['total_order'] = $this->request->post['total_order'];
                } else {
                    $data['total_order'] = '';
                }


                $this->load->model('checkout/cublcheckout');
                $results = $this->model_extension_module_newfastordercart->addOrder($data);
                $config_on_off_send_buyer_mail = $this->config->get('config_on_off_send_buyer_mail');
                if($config_on_off_send_buyer_mail =='1'){
                    if($data['email_buyer'] !='') {
                        $this->sendMailBuyer($data);
                    }
                }
                $config_on_off_send_me_mail = $this->config->get('config_on_off_send_me_mail');
                if($config_on_off_send_me_mail =='1'){
                    $this->sendMailMe($data);
                }
                if($this->config->get('config_send_sms_on_off_fastorder') == '1'){
                    $this->sendSms($data);
                }
                $lang_id = $this->config->get('config_language_id');
                $config_complete_cublcheckout = $this->config->get('config_complete_cublcheckout');
                $ok = $config_complete_cublcheckout[$lang_id]['config_complete_cublcheckout'];
                if($ok !=''){
                    $json['success'] = $ok;
                } else {
                    $json['success'] = $this->language->get('ok');
                }
                $this->cache->delete('product.bestseller');
                $this->cart->clear();

            }else{
                $json['error'] = $this->error;
            }
            return $this->response->setOutput(json_encode($json));
        }
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $this->response->setOutput($this->load->view('checkout/cublcheckout', $data));
    }

    private function validate() {
        $this->language->load('extension/module/newfastorder');
        $config_fields_firstname_requared = $this->config->get('config_fields_firstname_requared');
        $config_on_off_fields_firstname = $this->config->get('config_on_off_fields_firstname');
        if(($config_fields_firstname_requared =='1') && $config_on_off_fields_firstname =='1'){
            if ((strlen(utf8_decode($this->request->post['name_fastorder'])) < 1) || (strlen(utf8_decode($this->request->post['name_fastorder'])) > 32)) {
                $this->error['name_fastorder'] = $this->language->get('mister');
            }
        }
        $config_fields_phone_requared = $this->config->get('config_fields_phone_requared');
        $config_on_off_fields_phone = $this->config->get('config_on_off_fields_phone');
        if(($config_fields_phone_requared =='1') && $config_on_off_fields_phone =='1'){
            if ((strlen(utf8_decode($this->request->post['phone'])) < 3) || (strlen(utf8_decode($this->request->post['phone'])) > 32)) {
                $this->error['phone'] = $this->language->get('error_phone');
            }
        }
        $config_fields_comment_requared = $this->config->get('config_fields_comment_requared');
        $config_on_off_fields_comment = $this->config->get('config_on_off_fields_comment');
        if(($config_fields_comment_requared =='1') && $config_on_off_fields_comment == '1'){
            if ((strlen(utf8_decode($this->request->post['comment_buyer'])) < 1) || (strlen(utf8_decode($this->request->post['comment_buyer'])) > 400)) {
                $this->error['comment_buyer'] = $this->language->get('comment_buyer_error');
            }
        }
        $config_fields_email_requared = $this->config->get('config_fields_email_requared');
        $config_on_off_fields_email = $this->config->get('config_on_off_fields_email');
        if(($config_fields_email_requared =='1') && $config_on_off_fields_email == '1'){
            if(!preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/", $this->request->post['email_buyer'])){
                $this->error['email_error'] =  $this->language->get('email_buyer_error');
            }
        }

        $this->load->model('catalog/product');

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
    private function getCustomFields($order_info, $varabliesd) {
        $instros = explode('~', $varabliesd);
        $instroz = "";
        foreach ($instros as $instro) {
            if ($instro == 'totals' || isset($order_info[$instro]) ){
                if ($instro == 'totals'){
                    $instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
                }
                if(isset($order_info[$instro])){
                    $instro_other = $order_info[$instro];
                }
            }
            else {
                $instro_other = nl2br(htmlspecialchars_decode($instro));
            }
            $instroz .=  $instro_other;
        }
        return $instroz;
    }
    /* private function sendMailBuyer($data) {

         $this->language->load('extension/module/newfastorder');
         $data['text_photo'] = $this->language->get('text_photo');
         $data['text_product'] = $this->language->get('text_new_product');
         $data['text_model'] = $this->language->get('text_new_model');
         $data['text_quantity'] = $this->language->get('text_new_quantity');
         $data['text_price'] = $this->language->get('text_new_price');
         $data['text_total'] = $this->language->get('text_new_total');
         foreach ($data['total']['totals'] as $result) {
             $data['totals'][] = array(
                 'title' => $result['title'],
                 'text'  => $this->currency->format($result['value'],$this->session->data['currency']),
             );
         }

         $text = '';
         $subject_buyer = $this->getCustomFields($data, $this->config->get('cublcheckout_subject' . $data['language_id']));
         if ((strlen(utf8_decode($subject_buyer)) > 5)){
             $subject = $subject_buyer;
         } else {
             $subject = $this->language->get('subject');
         }
         $html = $this->getCustomFields($data, $this->config->get('cublcheckout_description' . $data['language_id'])). "\n";
         $config_buyer_html_products = $this->config->get('config_buyer_html_products');
         if($config_buyer_html_products =='1'){
             $html .= $this->load->view('mail/cublcheckout', $data);
         }
         $mail = new Mail();
         $mail->protocol = $this->config->get('config_mail_protocol');
         $mail->parameter = $this->config->get('config_mail_parameter');
         $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
         $mail->smtp_username = $this->config->get('config_mail_smtp_username');
         $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
         $mail->smtp_port = $this->config->get('config_mail_smtp_port');
         $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

         $mail->setTo($data['email_buyer']);
         $mail->setFrom($this->config->get('config_email'));
         $mail->setSender(html_entity_decode($data['store_name'], ENT_QUOTES, 'UTF-8'));
         $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
         $mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
         $mail->setText($text);
         $mail->send();
     }*/
    private function sendMailMe($data) {
        $this->language->load('checkout/cublcheckout');
        $data['text_photo'] = $this->language->get('text_photo');
        $data['text_product'] = $this->language->get('text_new_product');
        //$data['text_model'] = $this->language->get('text_new_model');
        $data['text_quantity'] = $this->language->get('text_new_quantity');
        $data['text_price'] = $this->language->get('text_new_price');
        $data['text_total'] = $this->language->get('text_new_total');
        $data['text_href'] = $this->language->get('text_href');
        $data['text_href_product'] = $this->language->get('text_href_product');


        $text = '';
        $subject = $this->language->get('subject');
        $html = '';
        $html .= $this->load->view('mail/cublcheckout', $data);



        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($data['store_name'], ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
        $mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
        $mail->setText($text);
        $mail->send();
    }


    public function payment_method_save() {
        $this->load->language('checkout/checkout');

        $json = array();

        // Validate if payment address has been set.
        if (!isset($this->session->data['payment_address'])) {
            $json['redirect'] = $this->url->link('checkout/checkout', '', true);
        }

        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $json['redirect'] = $this->url->link('checkout/cart');
        }

        // Validate minimum quantity requirements.
        $products = $this->cart->getProducts();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $json['redirect'] = $this->url->link('checkout/cart');

                break;
            }
        }

        if (!isset($this->request->post['payment_method'])) {
            $json['error']['warning'] = $this->language->get('error_payment');
        } elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
            $json['error']['warning'] = $this->language->get('error_payment');
        }

        if ($this->config->get('config_checkout_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

            if ($information_info && !isset($this->request->post['agree'])) {
                $json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
            }
        }

        if (!$json) {
            $this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];
//                $this->session->data['comment'] = strip_tags($this->request->post['comment']);
            if(isset($this->session->data['payment_method']['instruction'])){
                $json['instruction'] = $this->session->data['payment_method']['instruction'];
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function shipping_method_save() {
        $this->load->language('checkout/checkout');

        $json = array();

        // Validate if shipping is required. If not the customer should not have reached this page.
        /* if (!$this->cart->hasShipping()) {
             $json['redirect'] = $this->url->link('checkout/checkout', '', true);
         }*/

        // Validate if shipping address has been set.
        if (!isset($this->session->data['shipping_address'])) {
            $json['redirect'] = $this->url->link('checkout/checkout', '', true);
        }

        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $json['redirect'] = $this->url->link('checkout/cart');
        }

        // Validate minimum quantity requirements.
        $products = $this->cart->getProducts();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $json['redirect'] = $this->url->link('checkout/cart');

                break;
            }
        }

        if (!isset($this->request->post['shipping_method'])) {
            $json['error']['warning'] = $this->language->get('error_shipping');
        } else {
            $shipping = explode('.', $this->request->post['shipping_method']);

            if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
                $json['error']['warning'] = $this->language->get('error_shipping');
            }
        }

        if (!$json) {
            $this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
//            $this->session->data['comment'] = strip_tags($this->request->post['comment']);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function saves(){
        if (isset($this->request->post['value']) && isset($this->request->post['name'])) {
            $this->load->language('checkout/checkout');

            $json = array();
            if($this->request->post['name'] != 'comment') {
                if (empty($this->request->post['value'])) {
                    $json['error']['warning'] = 'Это поле обязательно к заполнению!';
                }
                if (mb_strlen($this->request->post['value']) < 2) {
                    $json['error']['warning'] = 'Это поле должно содержать более 2 символов!';
                }
            }
            if (!$json) {
                $this->session->data['person'][$this->request->post['name']] = $this->request->post['value'];
            }
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function confirm(){
        $json = [];
        $this->load->language('checkout/checkout');

        $warning_required = 'Это поле должно содержать более 2 символов!';
        $warning_empty = 'Это поле обязательно к заполнению!';

        if (isset($this->session->data['person']['fio'])){
            if(!empty($this->session->data['person']['fio'])&&mb_strlen($this->session->data['person']['fio'])>2){
                $fio = $this->session->data['person']['fio'];
                $data['fio'] = $fio;
                $fios = explode(' ', $fio);
                $data['firstname'] = '';
                foreach ($fios as $key => $fi){
                    if($key == 0){
                        $data['lastname'] = $fi;
                    } else {
                        $data['firstname'] .= $fi . ' ';
                    }
                }


            } else {
                $json['error']['warning']['fio'] = $this->language->get('error_firstname');// $warning_required;
            }
        } else {
            $json['error']['warning']['fio'] = $this->language->get('error_firstname');// $warning_required;
        }

        if (isset($this->session->data['person']['city'])){
            if(!empty($this->session->data['person']['city'])&&mb_strlen($this->session->data['person']['city'])>2){
                $data['city'] = $this->session->data['person']['city'];
            } else {
                $json['error']['warning']['city'] = $this->language->get('error_city');//$warning_required;
            }
        } else {
            $json['error']['warning']['city'] = $this->language->get('error_city');//$warning_required;
        }

        if (isset($this->session->data['person']['tel'])){
            if(!empty($this->session->data['person']['tel'])&&mb_strlen($this->session->data['person']['tel'])>2){
                $data['tel'] = $this->session->data['person']['tel'];
            } else {
                $json['error']['warning']['tel'] = $this->language->get('error_telephone');//$warning_required;
            }
        } else {
            $json['error']['warning']['tel'] = $this->language->get('error_telephone');//$warning_required;
        }

        if (isset($this->session->data['person']['address'])){
            if(!empty($this->session->data['person']['address'])&&mb_strlen($this->session->data['person']['address'])>2){
                $data['address'] = $this->session->data['person']['address'];
            } else {
                $json['error']['warning']['address'] = $this->language->get('error_address_1');//$warning_required;
            }
        } else {
            $json['error']['warning']['address'] = $this->language->get('error_address_1');//$warning_required;
        }

        if (isset($this->session->data['person']['comment'])){
            $data['comment'] = $this->session->data['person']['comment'];
        } else {
            $data['comment'] = '';
        }

        if (!isset($this->session->data['payment_method']['code'])) {
            $json['error']['warning']['payment_method'] = $this->language->get('error_payment');
        }

        if (!isset($this->session->data['shipping_method']['code'])) {
            $json['error']['warning']['shipping_method'] = $this->language->get('error_shipping');
        }

        if(!$json){

            $data['language_id'] = $this->config->get('config_language_id');
            $data['currency_id'] = $this->currency->getId($this->session->data['currency']);
            $data['currency_code'] = $this->session->data['currency'];
            $data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
            $data['ip'] = $this->request->server['REMOTE_ADDR'];
            $data['ip_store'] = $this->request->server['REMOTE_ADDR'];
            $data['store_id'] = $this->config->get('config_store_id');
            $data['store_name'] = $this->config->get('config_name');
            $data['text_deliveries'] = $this->config->get('text_deliveries');
            $data['text_payments'] = $this->config->get('text_payments');
            $data['text_comments'] = $this->config->get('text_comments');
            $data['text_address'] = $this->config->get('text_address');
            $data['text_city'] = $this->config->get('text_city');
            $data['text_fio'] = $this->config->get('text_fio');
            $data['text_tel'] = $this->config->get('text_tel');
            $data['store_name'] = $this->config->get('config_name');
            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $data['forwarded_ip'] = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $data['user_agent'] = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $data['accept_language'] = '';
            }
            $data['customer_id'] = 0;
            $data['customer_group_id'] = 1;
            $data['config_tax'] = $this->config->get('config_tax');
            $data['store_url'] = HTTP_SERVER;
            $data['country_id'] = 220;
            $data['zone_id'] = 170;
            $data['shipping_title'] = $this->session->data['shipping_method']['title'];
            $data['shipping_code'] = $this->session->data['shipping_method']['code'];
            $data['payment_title'] = $this->session->data['payment_method']['title'];
            $data['payment_code'] = $this->session->data['payment_method']['code'];

            $this->load->model('extension/extension');

            $totals = array();
            $taxes = $this->cart->getTaxes();
            $total = 0;

            // Because __call can not keep var references so we put them into an array.
            $total_data = array(
                'totals' => &$totals,
                'taxes'  => &$taxes,
                'total'  => &$total
            );

            // Display prices
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $sort_order = array();

                $results = $this->model_extension_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result1) {
                    if ($this->config->get($result1['code'] . '_status')) {
                        $this->load->model('extension/total/' . $result1['code']);

                        // We have to put the totals in an array so that they pass by reference.
                        $this->{'model_extension_total_' . $result1['code']}->getTotal($total_data);
                    }
                }

                $sort_order = array();

                foreach ($totals as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $totals);
            }
            $data['totals'] = array();
            foreach ($totals as $total1) {
                $data['totals'][] = array(
                    'title' => $total1['title'],
                    'text'  => $this->currency->format($total1['value'], $this->session->data['currency'])
                );
                $data['total_order'] = $this->currency->format($total1['value'], $this->session->data['currency']);

            }

            $this->load->model('tool/image');

            foreach ($this->cart->getProducts() as $product) {
                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
                } else {
                    $image = '';
                }
                $option_data = array();

                foreach ($product['option'] as $option) {
                    $option_data[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'option_id' => $option['option_id'],
                        'option_value_id' => $option['option_value_id'],
                        'name' => $option['name'],
                        'value' => $option['value'],
                        'type' => $option['type']
                    );
                }

                $products_data[] = array(
                    'product_id' => $product['product_id'],
                    'thumb' => $image,
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'download' => $product['download'],
                    'quantity' => $product['quantity'],
                    'subtract' => $product['subtract'],
                    'price' => $product['price'],
                    'total' => $product['total'],
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    'price_fast' => $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')),
                    'total_fast' => $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'],
                    'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward' => $product['reward']
                );
            }
            $data['products'] = $products_data;


            $this->load->model('checkout/cublcheckout');
//            $json['error']['logs'] =
            $this->session->data['order_id'] = $this->model_checkout_cublcheckout->addOrder($data);
            if (isset($this->session->data['order_id'])) {
                $this->sendMailMe($data);
            }

            //unset($this->session->data['shipping_method']);
            //unset($this->session->data['shipping_methods']);
            //unset($this->session->data['payment_method']);
            //unset($this->session->data['payment_methods']);
            //unset($this->session->data['person']);

            $json['redirect'] = $this->url->link('checkout/success');

        }



        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}