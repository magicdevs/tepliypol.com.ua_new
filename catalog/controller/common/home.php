<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

        $this->load->model('catalog/review');

        $data['comments'] = array();


        $results = $this->model_catalog_review->getReviews();
        $this->load->model('tool/image');

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 80,80);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 80,80);
            }
            $data['comments'][] = array(
                'name'     => $result['author'],
                'thumb'  => $image,
                'description'       => nl2br($result['text']),
                'city'     => $result['city']
            );
        }


        $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
