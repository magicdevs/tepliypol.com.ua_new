<?php
class ControllerCommonFooter extends Controller {

    public function addToNewsletter(){

        $email = $this->request->post['email'];

        $this->load->language('common/footer');
        $this->load->model('account/customer');

        $this->createNewsletterTables();

        $count = $this->checkEmailSubscribe($email);

        if($count == 0){

            $newsletter_id = $this->model_account_customer->addToNewsletter($email);
            $msg = 'E-mail успешно подписан';

        } else {

            $msg = 'Такой e-mail уже зарегестрирован';
        }

        echo $msg;

    }

    public function createNewsletterTables() {

        $query = $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "newsletter (
                        `id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
                        `email` VARCHAR( 255 ) NOT NULL ,
                        `group` VARCHAR( 25 ) NOT NULL ,
                        `date_added` DATETIME NOT NULL ,
                        PRIMARY KEY ( `id` )
                        )");
    }

    public function checkEmailSubscribe($email){

        $this->load->model('account/customer');

        $count = $this->model_account_customer->checkEmailSubscribe($email);

        return $count;

    }

    public function index() {
        $this->load->language('product/search');
        $data['text_empty'] = $this->language->get('text_empty');

        $data['text_view_all_results'] = $this->config->get('live_search_view_all_results')[$this->config->get('config_language_id')]['name'];
        $data['live_search_ajax_status'] = $this->config->get('live_search_ajax_status');
        $data['live_search_show_image'] = $this->config->get('live_search_show_image');
        $data['live_search_show_price'] = $this->config->get('live_search_show_price');
        $data['live_search_show_description'] = $this->config->get('live_search_show_description');
        $data['live_search_href'] = $this->url->link('product/search', 'search=');
        $data['live_search_min_length'] = $this->config->get('live_search_min_length');

	    $this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');
 $data['lang'] = $this->language->get('code');
		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
        $data['address'] = html_entity_decode($this->config->get('config_address'), ENT_QUOTES, 'UTF-8');
        $data['config_name'] = html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');
        if(!empty($this->config->get('config_telephone'))){
            $data['tels'] = explode(',',$this->config->get('config_telephone'));
        } else {
            $data['tels'] = '';
        }

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
        $this->load->model('extension/module/IMCallMeAskMe');

        $data['callAskMe'] = $this->model_extension_module_IMCallMeAskMe->getSettings()[1];

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}


        $this->load->model('newsblog/article');

        $filter_data = array(
            'filter_category_id' => 1,
            'sort'               => 'a.date_available',
            'order'              => 'desc',
            'start'              => 0,
            'limit'              => 3
        );


        $results = $this->model_newsblog_article->getArticles($filter_data);
        $data['articles'] = [];
        foreach ($results as $result) {
            $data['articles'][] = array(
                'article_id'  		=> $result['article_id'],
                'name'        		=> $result['name'],
                'date'		  		=> date('d.m.Y', strtotime($result['date_available'])),
                'href'        		=> $this->url->link('newsblog/article', 'newsblog_path=' . 1 . '&newsblog_article_id=' . $result['article_id'])
            );
        }



		return $this->load->view('common/footer', $data);
	}
}
