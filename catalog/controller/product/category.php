<?php
class ControllerProductCategory extends Controller {
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.price';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
$data['lang'] = $this->language->get('code');
		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);

        $h1_postfix = '';
        if(isset( $this->request->get['filter_ocfilter'])){
            if(strpos($this->request->get['filter_ocfilter'], 'm:') !== false){
                if(count(explode(',', $this->request->get['filter_ocfilter'])) < 2 and count(explode(';', $this->request->get['filter_ocfilter'])) < 2){
                    $selected_manufacturer = explode(':', $this->request->get['filter_ocfilter']);
                    $manufacturer_res = $this->db->query("SELECT name FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id=".$selected_manufacturer[1]);
                    if(!empty($manufacturer_res->row['name']))
                        $h1_postfix .= ' - ' . $manufacturer_res->row['name'];
                }
                elseif(count(explode(',', $this->request->get['filter_ocfilter'])) < 2 and count(explode(';', $this->request->get['filter_ocfilter'])) > 1){
                    if(strpos($this->request->get['filter_ocfilter'], 'p:') === false){
                        $all_types_params = explode(';', $this->request->get['filter_ocfilter']);
                        $params_postfix = '';
                        $lastElement = end($all_types_params);
                        foreach($all_types_params as $one){
                        $end_of_string = ($lastElement == $one) ? '' : ', ';
                            if(strpos($one, 'm:') === false){
                                $selected_options1 = explode(':', $one);
                                $param_res = $this->db->query("SELECT name FROM " . DB_PREFIX . "ocfilter_option_description WHERE option_id=".$selected_options1[0]." AND language_id=".$this->config->get('config_language_id'));

                                if(strpos($selected_options1[1], '-') === false){

                                    $param_filter_val_res = $this->db->query("SELECT name FROM " . DB_PREFIX . "ocfilter_option_value_description WHERE value_id=".$selected_options1[1]." AND language_id=".$this->config->get('config_language_id'));
                                    if(!empty($param_filter_val_res->row['name']))
                                        $filter_option_value = $param_filter_val_res->row['name'];
                                    else
                                        $filter_option_value = '';
                                }else{
                                    $filter_option_value = $selected_options1[1];
                                }
                                if(!empty($param_res->row['name']))
                                    $params_postfix .=  $param_res->row['name'] . ' : ' . $filter_option_value  . $end_of_string;
                            }
                            else{
                                $manufacturer_id = explode(':', $one);
                                $manufacturer_res = $this->db->query("SELECT name FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id=".$manufacturer_id[1]);
                                $h1_postfix .= ' ' . $manufacturer_res->row['name'] . ', ';
                            }
                        }
                        $h1_postfix .= $params_postfix;
                    }
                }
            }
            elseif(strpos($this->request->get['filter_ocfilter'], 'p:') !== false ){
                if(count(explode(';', $this->request->get['filter_ocfilter'])) < 2){
                    $price_range = explode(':', $this->request->get['filter_ocfilter']);
                    $h1_postfix .= ' - цена: ' . $price_range[1];
                }
            }
            else {
                $all_types_params_exclud_m_p = explode(';', $this->request->get['filter_ocfilter']);
                $lastElement = end($all_types_params_exclud_m_p);
                $postfix_params_exclud = '';
                foreach ($all_types_params_exclud_m_p as $one){
                    $end_of_string = ($lastElement == $one) ? '' : ',';
                    $selected_options = explode(':', $one);
                    $param_res = $this->db->query("SELECT name FROM " . DB_PREFIX . "ocfilter_option_description WHERE option_id=".$selected_options[0]." AND language_id=".$this->config->get('config_language_id'));

                    if(strpos($selected_options[1], '-') === false ){
                        $several_one_type_filters = explode(',', $selected_options[1]);
                        $last_el = end($several_one_type_filters);
                        $filter_option_value = '';
                        foreach ($several_one_type_filters as $one_filter){
                            $end_of_str = ($last_el == $one_filter) ? '' : ', ';
                            $param_filter_val_res = $this->db->query("SELECT name FROM " . DB_PREFIX . "ocfilter_option_value_description WHERE value_id=".$one_filter." AND language_id=".$this->config->get('config_language_id'));
                            if(!empty($param_filter_val_res->row['name']))
                                $filter_option_value .= $param_filter_val_res->row['name'] . $end_of_str;
                            else
                                $filter_option_value = '';
                        }
                    }else{
                        $filter_option_value = $selected_options[1];
                    }
                    if(!empty($param_res->row['name']))
                        $postfix_params_exclud .= ' '. $param_res->row['name'] . ' : ' . $filter_option_value . $end_of_string;
                }
                $h1_postfix .= ',' . $postfix_params_exclud;
            }
        }

        $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'canonical');

		if ($category_info) {
			if ($page > 1) {
				$this->document->setTitle($category_info['meta_title'] . ' - Страница ' . $page);
			}else{
				$this->document->setTitle($category_info['meta_title']);
			}

            if(!empty($category_info['meta_description']))
                $this->document->setDescription($category_info['meta_description']);
            else
                $this->document->setDescription('Большой выбор - ' . $category_info['name'] . ', цены в каталоге интернет магазина “Теплый пол”⭐ Заказывайте сейчас!');

			$this->document->setKeywords($category_info['meta_keyword']);

			$data['heading_title'] = $category_info['name'] . $h1_postfix;

            if( $page > 1)
             	$data['heading_title'] = strpos($_SERVER['REQUEST_URI'], '/ua/') === false ? $data['heading_title']  . ' - страница № ' . $page : $data['heading_title']  . ' - сторінка № ' . $page;

            // generator meta tags
            if ($this->config->get('generator_meta_status')) {
                $generator_meta_category_lang = $this->config->get('generator_meta_category_lang');
                $lang_id = $this->config->get('config_language_id');

                if (isset($generator_meta_category_lang['title'][$lang_id])) {
                    $meta_title = $generator_meta_category_lang['title'][$lang_id];
                } else {
                    $meta_title = '';
                }

                if (isset($generator_meta_category_lang['description'][$lang_id])) {
                    $meta_description = $generator_meta_category_lang['description'][$lang_id];
                } else {
                    $meta_description = '';
                }

                $h1 = $data['heading_title'];

                $meta_title = trim(str_replace('{h1}', $h1, $meta_title));
                $meta_description = trim(str_replace('{h1}', $h1, $meta_description));

                $parent_category_info = $this->model_catalog_category->getParentCategory($category_id);

                if ($parent_category_info) {
                    $parent_category_name = $parent_category_info['name'];

                    $meta_title = trim(str_replace('{category}', $parent_category_name, $meta_title));
                    $meta_description = trim(str_replace('{category}', $parent_category_name, $meta_description));
                }

                if ($meta_title) {
                    $this->document->setTitle($meta_title);
                }

                if(empty($category_info['meta_description']) && $meta_description) {
                    $this->document->setDescription($meta_description);
                }
            }
            // generator meta tags

			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');
            $data['select_sqrt'] = $this->language->get('select_sqrt');

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);

			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

			//[dg]add new product name part
			
			if ($this->language->get('code')=='uk'){
				$prod_name_parts = array(
					'65' => 'Кабельна тепла підлога',
					'64' => 'Тепла підлога під плитку',
					'63' => 'Мат для теплої підлоги',
					'62' => 'Тепла підлога під ламінат',
					'69' => 'Терморегулятор для теплої підлоги',
					'70' => 'Програмований терморегулятор для теплої підлоги',
					'68' => 'Кабель для обігріву',
					'67' => 'Кабель для обігріву',
					'66' => 'Кабель для обігріву',
					'80' => 'Панель опалення',
				);
			}else{
				$prod_name_parts = array(
					'65' => 'Кабельный тёплый пол',
					'64' => 'Теплый пол под плитку',
					'63' => 'Нагревательный мат',
					'62' => 'Теплый пол под ламинат',
					'69' => 'Термостат для теплого пола',
					'70' => 'Программируемый терморегулятор для теплого пола',
					'68' => 'Греющий кабель',
					'67' => 'Греющий кабель',
					'66' => 'Греющий кабель',
					'80' => 'Панель отопления',
				);
			}
				if (isset($prod_name_parts[$category_id])){
				$data['dg_prod_name_part'] = $prod_name_parts[$category_id];
			}

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				$data['categories'][] = array(
					'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			}

			$data['products'] = array();

            $data['offset'] = ($page - 1) * $limit;

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$filter_data_faq = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'filter_sub_category' => true,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$products_for_faq = $this->model_catalog_product->getProducts($filter_data_faq);
			shuffle($products_for_faq);
			$random_products = array_slice($products_for_faq, 0, 3);
			$random_products_urls = [];
			foreach ($random_products as $rand_prod) {
				$random_products_urls[] = '<li><a href="'.$this->url->link('product/product', 'product_id=' . $rand_prod['product_id']).'">'.$rand_prod['name'].'</a></li>';
			}
			$data['random_prods_faq'] = implode(' ', $random_products_urls);


			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);
            $data['symbol_left']      		= $this->currency->getSymbolLeft($this->session->data['currency']);
            $data['symbol_right']      		= $this->currency->getSymbolRight($this->session->data['currency']);
			foreach ($results as $result) {
                if ((int)$result['price']) {//0.000000

                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = (int)$result['rating'];
                    } else {
                        $rating = false;
                    }

                    $images = array();

                    $results1 = $this->model_catalog_product->getProductImages($result['product_id']);

                    foreach ($results1 as $key => $result1) {
                        if ($key < 3) {
                            $images[] = $this->model_tool_image->resize($result1['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'));
                        }
                    }
                    $options1 = [];
                    $optionUnit1 = '';
                    $optionUnit13 = '';
                    $options13 = [];
                    $semil = $this->model_catalog_product->getProductRelated($result['product_id']);
                    if ($semil) {
                        foreach ($semil as $sem) {
                            foreach ($this->model_catalog_product->getProductOptions($sem['product_id']) as $option) {
                                if (in_array($option['option_id'], [1, 13])) {
                                    if ($option['option_id'] == 1) {
                                        $optionUnit1 = $option['units'];
                                        foreach ($option['product_option_value'] as $option_value) {
                                            if (is_numeric(preg_replace("/[^0-9]/", '', $option_value['valuer']))) {
                                                $options1[preg_replace("/[^0-9]/", '', $option_value['valuer'])] = $option_value['valuer'];
                                            }
                                        }
                                    }
                                    if ($option['option_id'] == 13) {
                                        $optionUnit13 = $option['units'];
                                        foreach ($option['product_option_value'] as $option_value) {
                                            if (is_numeric(preg_replace("/[^0-9]/", '', $option_value['valuer']))) {
                                                $options13[preg_replace("/[^0-9]/", '', $option_value['valuer'])] = preg_replace("/[^0-9]/", '', $option_value['valuer']);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($options1) {
                        ksort($options1);
                    }
                    if ($options13) {
                        ksort($options13);
                    }


                    $data['products'][] = array(
                        'product_id' => $result['product_id'],
                        'thumb' => $image,
                        'manufacturer' => $result['manufacturer'],
                        'price_anal' => $result['price'],
                        'name' => $result['name'],
                        'upc' => $result['upc'],
                        'location' => $result['location'],
                        'is_siblings' => $result['is_siblings'],
                        'images' => $images,
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price' => $price,
                        'special' => $special,
                        'tax' => $tax,
                        'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'rating' => $result['rating'],
                        'options1' => $options1,
                        'optionUnit1' => html_entity_decode($optionUnit1, ENT_QUOTES, 'UTF-8'),
                        'optionUnit13' => html_entity_decode($optionUnit13, ENT_QUOTES, 'UTF-8'),
                        'options13' => $options13,
                        'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
                    );
                }
            }

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			);
/*
			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);*/

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
            if($product_total != 0) {
                if($page > ceil($product_total / $limit)){
                    $this->response->redirect($this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page='.ceil($product_total / $limit)), 301);
                }
                if(isset($this->request->get['page']) && $this->request->get['page'] == 1){
                    $this->response->redirect($this->url->link('product/category', 'path=' . $this->request->get['path'] . $url), 301);
                }
                if(isset($this->request->get['page']) && $this->request->get['page'] == 0){
                    $this->response->redirect($this->url->link('product/category', 'path=' . $this->request->get['path'] . $url), 301);
                }
            }

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
			if ($page > 1) $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'canonical');
				
			if ($page == 1) {
			    
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'prev');
			} else {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page - 1), true), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page + 1), true), 'next');
			}

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('product/category', $data));
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}
