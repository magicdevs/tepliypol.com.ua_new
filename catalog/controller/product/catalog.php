<?php

class ControllerProductCatalog extends Controller
{
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $this->load->language('product/catalog');

        $this->document->setTitle($this->language->get('heading_title'));
    }

    public function index()
    {
        $this->document->addLink($this->url->link('product/category', 'category_id=59' ), 'canonical');
        $data['breadcrumbs'] = [
            ['text' => $this->language->get('text_home'), 'href' => $this->url->link('common/home')],
            ['text' => $this->language->get('heading_title'), 'href' => $this->url->link('product/catalog')],
        ];

        $language_keys = [
            'heading_title', 'text_empty', 'text_product_quantity', 'text_from', 'text_more', 'text_specifications', 'text_product_description',
            'column_order_id', 'column_customer', 'column_product', 'column_total', 'column_status', 'column_date_added', 'column_model',
            'button_view', 'button_continue', 'button_reorder', 'button_collapse',
        ];

        $this->addLanguageVars($data, $language_keys);

        $data['categories_with_products'] = $this->getCategoriesWithProducts(0);

        $this->loadDefaultControllers($data);

        $this->response->setOutput($this->load->view('product/catalog', $data));
    }

    protected function addLanguageVars(&$data, array $language_keys)
    {
        foreach ($language_keys as $language_key) {
            $data[$language_key] = $this->language->get($language_key);
        }
    }

    protected function loadDefaultControllers(&$data)
    {
        $data['column_left']    = $this->load->controller('common/column_left');
        $data['column_right']   = $this->load->controller('common/column_right');
        $data['content_top']    = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer']         = $this->load->controller('common/footer');
        $data['header']         = $this->load->controller('common/header');
    }

    protected function getCategoriesWithProducts($cat_id, $level = 99)
    {
        $data = [];

        if ($level < 1) {
            return $data;
        }

        $categories_product = [
            65 => [
                306,
                305,
                330,
                303,
            ],
            64 => [
                310,
                427,
                309,
            ],
            63 => [
                313,
                312,
                424,
                504,
                426,
                307,
            ],
            62 => [
                346,
                341,
                316,
                315,
                344,
            ],
            69 => [
                99,
                348,
                98,
                347,
            ],
            70 => [
                101,
                349,
                350,
                351,
                358,
                355,
                352,
                359,
            ],
            71 => [
                496,
                497,
                361,
                103,
                498,
                499,
                501,
                502,
            ],
            68 => [
                473,
                317,
                339,
                330,
                439,
                335,
                338,
            ],
            67 => [
                340,
                455,
                335,
                338,
                439,
                339,
            ],
            66 => [
                317,
                330,
                473,
            ],
        ];

        foreach ($categories_product as $category_id => $product_ids) {
            $category = $this->model_catalog_category->getCategory($category_id);

            if (!$category) {
                continue;
            }

            $products = [];

            foreach ($product_ids as $product_id) {
                $result = $this->model_catalog_product->getProduct($product_id);

                if (!$result/* || !(int)$result['price']*/) {
                    continue;
                }

                $products[] = $this->getProductData($result);
            }

            $data[] = [
                'children'  => [],
                'name'      => $category['name'],
                'products'  => $products,
                'text_more' => $this->language->get('text_more'),
                'text_specifications' => $this->language->get('text_specifications'),
                'text_product_description' => $this->language->get('text_product_description'),
                'href'      => $this->url->link('product/category', 'path=' . $category['category_id']),
            ];
        }

        return $data;
    }

    protected function getProductData($result)
    {
        $product = [];

        if (!$result) {
            return $product;
        }

        $price = $special = $tax = $rating = false;

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        }

        if ((float)$result['special']) {
            $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        }

        if ($this->config->get('config_tax')) {
            $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
        }

        $options1     = [];
        $optionUnit1  = '';
        $optionUnit13 = '';
        $options13    = [];
        $semil        = $this->model_catalog_product->getProductRelated($result['product_id']);

        if ($semil) {
            foreach ($semil as $sem) {
                foreach ($this->model_catalog_product->getProductOptions($sem['product_id']) as $option) {
                    if (in_array($option['option_id'], [1, 13])) {
                        if ($option['option_id'] == 1) {
                            $optionUnit1 = $option['units'];
                            foreach ($option['product_option_value'] as $option_value) {
                                if (is_numeric(preg_replace("/[^0-9]/", '', $option_value['valuer']))) {
                                    $options1[preg_replace("/[^0-9]/", '', $option_value['valuer'])] = $option_value['valuer'];
                                }
                            }
                        }
                        if ($option['option_id'] == 13) {
                            $optionUnit13 = $option['units'];
                            foreach ($option['product_option_value'] as $option_value) {
                                if (is_numeric(preg_replace("/[^0-9]/", '', $option_value['valuer']))) {
                                    $options13[preg_replace("/[^0-9]/", '', $option_value['valuer'])] = preg_replace("/[^0-9]/", '', $option_value['valuer']);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($options1) {
            ksort($options1);
        }
        if ($options13) {
            ksort($options13);
        }

        $attribute_groups = $this->model_catalog_product->getProductAttributes($result['product_id']);

        $product = [
            'product_id'       => $result['product_id'],
            'attribute_groups' => $attribute_groups,
            'manufacturer'     => $result['manufacturer'],
            'price_anal'       => $result['price'],
            'name'             => $result['name'],
            'upc'              => $result['upc'],
            'location'         => $result['location'],
            'is_siblings'      => $result['is_siblings'],
            'description'      => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
            'price'            => $price,
            'special'          => $special,
            'tax'              => $tax,
            'minimum'          => $result['minimum'] > 0 ? $result['minimum'] : 1,
            'rating'           => $result['rating'],
            'options1'         => $options1,
            'optionUnit1'      => html_entity_decode($optionUnit1, ENT_QUOTES, 'UTF-8'),
            'optionUnit13'     => html_entity_decode($optionUnit13, ENT_QUOTES, 'UTF-8'),
            'options13'        => $options13,
            'href'             => $this->url->link('product/product', 'product_id=' . $result['product_id']),
        ];

        return $product;
    }
}