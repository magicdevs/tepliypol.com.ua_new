<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.01.2019
 * Time: 13:04
 */

class ControllerExtensionModuleSimilar extends Controller {

    public function index(){
        function array_value_recursive($key, array $arr){
            $val = array();
            array_walk_recursive($arr, function($v, $k) use($key, &$val){
                if($k == $key) array_push($val, $v);
            });
            return count($val) > 1 ? $val : array_pop($val);
        }
        $this->load->language('product/compare');
        $data['text_price'] = $this->language->get('text_price');
        if (!empty($this->request->get['product_id'])) {
            $this->load->model('catalog/product');
            $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
            if ($product_info){

                $data['product_id'] = (int)$this->request->get['product_id'];
                $prod_id = (int)$this->request->get['product_id'];
                $data['products'] = [];
                $results = $this->model_catalog_product->getProductRelated($prod_id);

                $data['oname'] = [];

                foreach ($results as $result) {

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    $options = array();
                    $optionsname = array();
                    $optionsMainName = [];
                    $optionsAll = [];
                    foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                        $product_option_value_data = array();

                        foreach ($option['product_option_value'] as $option_value) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    $price1 = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                } else {
                                    $price1 = false;
                                }

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id'         => $option_value['option_value_id'],
                                    'name'                    => $option_value['name'],
                                    'valuer'                  => $option_value['valuer'],
                                    'price'                   => $price1,
                                    'price_prefix'            => $option_value['price_prefix']
                                );
                            $optionsname[] = $option_value['name'];

                        }

                        $options[] = array(
                            'product_option_id'    => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id'            => $option['option_id'],
                            'name'                 => $option['name'],
                            'units'                 => $option['units'],
                            'odesc'                 => $option['odesc'],
                            'type'                 => $option['type'],
                            'value'                => $option['value'],
                            'required'             => $option['required']
                        );
                        if($product_option_value_data) {
                            $optionsMainName[] = $option['name'];
                        }
                        $optionsAll[] = $option['name'];
                        $data['oname'][] = ['name'=>$option['name'], 'product_option_value'=>((isset($product_option_value_data)&&count($product_option_value_data)>1)?array_value_recursive('name',$product_option_value_data):''), 'units'=>html_entity_decode($option['units'], ENT_QUOTES, 'UTF-8'), 'odesc'=>$option['odesc']];
                    }



                    $data['products'][] = array(
                        'product_id'  => $result['product_id'],
                        'name'        => $result['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price'       => preg_replace("/[^,.0-9]/", '', $price),
                        'special'     => $special,
                        'options'     => $options,
                        'optionsAll'     => $optionsAll,
                        'optionsname'     => $optionsname,
                        'optionsmainname'     => $optionsMainName,
                        'tax'         => $tax,
                        'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                    );
                }

                $oname = array_intersect_key($data['oname'], array_unique(array_map('serialize', $data['oname'])));

                $oname = array_values($oname);
                   $data['oname'] = $oname;
                   /*$data['oname'] = [];
                    for ($key = 0; $key < count($oname); $key++) {
                    //foreach ($oname as $key => $name) {
                        if($key!=count($oname)-1) {
                            if ($oname[$key]['name'] == $oname[$key + 1]['name']) {
                                $data['oname'][] = array_merge($oname[$key], $oname[$key + 1]);//array_merge($oname[$key], $oname[$key + 1]);
                            } else {
                                $data['oname'][] = $oname[$key];
                            }
                        }
                    }*/




//                print_r(array_merge_recursive($oname));
//                print_r(call_user_func_array('array_merge_recursive', $oname));
               /* $data['oname'] = [];
                for ($i = 0; $i < count($oname); $i++) {
                for ($j = count($oname); $j > 0; $j--) {
//                    if($key!=count($oname)-1) {
                        if ($oname[$i]['name'] == $oname[$j]['name']) {
                            $data['oname'][] = array_merge($oname[$i], $oname[$j]);
                        } else {
                            $data['oname'][] = $oname[$i];
                        }
//                    }
                }
                }*/
                $data['select_sqrt'] = $this->language->get('select_sqrt');



                $this->response->setOutput($this->load->view('extension/module/similar', $data));
            }

        }
    }

}