<?php
// Text
//$_['text_items']    = 'Товаров %s (%s)';
$_['text_items']    = 'В корзине <span class="strong-style">%s</span> товаров на сумму <span class="strong-style">%s</span>';
$_['text_cart_name'] = 'Корзина товаров';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Перейти в корзину';
$_['text_checkout'] = 'Оформить заказ';
$_['text_recurring']  = 'Платежный профиль';

