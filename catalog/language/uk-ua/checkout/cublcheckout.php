<?php

$_['heading_title'] = 'Оформлення замовлення';
$_['text_delivery'] = 'Доставка';
$_['text_deliveries'] = 'Спосіб доставки';
$_['text_payment'] = 'Оплата';
$_['text_payments'] = 'Спосіб оплати';
$_['text_comments'] = 'Коментар на замовлення';
$_['text_cart'] = 'Товари';
$_['text_address'] = 'Адреса';
$_['text_city'] = 'Місто';
$_['text_fio'] = 'ПІБ';
$_['text_tel'] = 'Телефон';
$_['text_back_cart'] = 'Повернутись до кошика';
$_['btn_send'] = 'Оформити замовлення';

$_['text_photo'] = 'Фото';
$_['text_new_product'] = 'Товар';
$_['text_new_model'] = 'Модель';
$_['text_new_quantity'] = 'Кількість';
$_['text_new_price'] = 'Ціна';
$_['text_new_total'] = 'Разом';
$_['subject'] = 'Нове замовлення';
$_['text_href'] = 'Посилання на товар';
$_['text_href_product'] = 'Перейти на товар';














// Text
$_['button_remove'] = 'Видалити';
$_['text_continue'] = 'Продовжити покупки';
$_['text_empty'] = 'У кошику порожньо!';
$_['text_total_qucik_ckeckout'] = 'У кошику <big><b>%s</b></big> товарів, на <big><b>%s</b></big>';
$_['sendthis'] = 'Швидке Замовлення';
$_['callback'] = 'Перезвонити:';
$_['namew'] = 'Ваше ім\'я:';
$_['phonew'] = 'Ваш телефон:';
$_['comment_buyer'] = 'Ваш Коментар:';
$_['email_buyer'] = 'Ваш E-mail:';
$_['theme'] = 'Перезвонити:';
$_['messagew'] = 'Повідомлення:';
$_['cancel'] = 'Скасування';
$_['button_send'] = 'Оформити Замовлення';
$_['time'] = 'Ми вам передзвонимо протягом години або в інший вказаний Вами час.';
$_['ok'] = 'Ваше повідомлення надіслано.';
$_['wrongnumber'] = 'Неправильний номер';
$_['mister'] = 'Як до Вас звертатися?';
$_['text_1'] = 'Ви отримали замовлення на ';
$_['name'] = 'Ім\'я: ';
$_['phone'] = 'Телефон:';
$_['comment_buyer'] = 'Коментар: ';
$_['url_callback'] = 'З цієї сторінки оформили швидке замовлення: ';
$_['error_required'] = 'Поле %s має бути заповнене!';

$_['comment_buyer_error'] = 'Потрібно заповнити';
$_['email_buyer_error'] = 'Ваш e-mail?';
$_['error_phone'] = 'Ваш телефон?';
$_['text_column_name'] = 'Я купую';
$_['text_column_price'] = 'За ціною:';
$_['text_column_quantity'] = 'У кількості';

$_['text_column_photo_product'] = 'Фото';
$_['text_column_name_product'] = 'Назва';
$_['text_column_quantity_product'] = 'Кількість';
$_['text_column_price_product'] = 'Ціна';
$_['text_column_total_product'] = 'Разом';



?>
