<?php
// Text
$_['text_items']    = 'В кошику <span class="strong-style">%s</span> товарів на суму <span class="strong-style">%s</span>';
$_['text_empty']    = 'В кошику немає товарів :(';
$_['text_cart_name'] = 'Кошик товарів';
$_['text_cart']     = 'Перейти в кошик';
$_['text_checkout'] = 'Оформлення замовлення';
$_['text_recurring']  = 'Платіжний профіль';