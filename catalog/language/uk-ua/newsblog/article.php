<?php
// Text
$_['text_related'] = 'Схожі статті';
$_['text_related_products'] = 'Рекомендовані товари';
$_['text_attributes'] = 'Додаткові поля';
$_['text_tags'] = 'Теги:';

// Entry
$_['entry_qty'] = 'У';
$_['entry_name'] = 'Ваше ім\'я:';
$_['entry_review'] = 'Ваш відгук';
$_['entry_rating'] = 'Рейтинг';
$_['entry_good'] = 'Добре';
$_['entry_bad'] = 'Погано';

// Error
$_['error_name'] = 'Ім\'я має бути від 3 до 25 символів!';
$_['error_text'] = 'Текст Відгуку має бути від 25 до 1000 символів!';
$_['error_rating'] = 'Будь ласка, поставте оцінку!';