<?php 
$curr_uri = $_SERVER['REQUEST_URI'];
$cities_arr = array(
  'kiev' => ['ru' => 'Киев','ua' => 'Київ'],
  'harkov' => ['ru' => 'Харьков','ua' => 'Харків'],
  'vinnitca' => ['ru' => 'Винница','ua' => 'Вінниця'],
  'dnepr' => ['ru' => 'Днепр','ua' => 'Дніпро'],
  'zhitomir' => ['ru' => 'Житомир','ua' => 'Житомир'],
  'nikolaev' => ['ru' => 'Николаев','ua' => 'Миколаїв'],
  'kherson' => ['ru' => 'Херсон','ua' => 'Херсон'],
  'poltava' => ['ru' => 'Полтава','ua' => 'Полтава'],
  'chernigov' => ['ru' => 'Чернигов','ua' => 'Чернігів'],
  'rovno' => ['ru' => 'Ровно','ua' => 'Рівне'],
  'ternopol' => ['ru' => 'Тернополь','ua' => 'Тернопіль'],
  'lutck' => ['ru' => 'Луцк','ua' => 'Луцьк'],
  'odessa' => ['ru' => 'Одесса','ua' => 'Одеса'],
  'zaporozhe' => ['ru' => 'Запорожье','ua' => 'Запоріжжя'],
  'lvov' => ['ru' => 'Львов','ua' => 'Львів'],
  'krivoi-rog' => ['ru' => 'Кривой рог','ua' => 'Кривий Ріг'],
  'cherkassy' => ['ru' => 'Черкассы','ua' => 'Вінниця'],
  'summy' => ['ru' => 'Сумы','ua' => 'Суми'],
  'khmelnitckii' => ['ru' => 'Хмельницкий','ua' => 'Хмельницький'],
  'kropivnitcki' => ['ru' => 'Кропивницкий','ua' => 'Кропивницький'],
  'ivano-frankovske' => ['ru' => 'Ивано-Франковск','ua' => 'Івано-Франківськ'],
  'uzhgorod' => ['ru' => 'Ужгород','ua' => 'Ужгород'],

  'mariupol' => ['ru' => 'Мариуполь', 'ua' => 'Маріуполь'],
  'chernovtsy' => ['ru' => 'Черновцы', 'ua' => 'Чернівці'],
  'belaya-tserkov' => ['ru' => 'Белая Церковь', 'ua' => 'Біла Церква'],
  'berdyansk' => ['ru' => 'Бердянск', 'ua' => 'Бердянськ'],
  'kremenchug' => ['ru' => 'Кременчуг', 'ua' => 'Кременчук'],
  'melitopol' => ['ru' => 'Мелитополь', 'ua' => 'Мелітополь'],
  'nikopol' => ['ru' => 'Никополь', 'ua' => 'Нікополь'],
  'kamenets-podolskiy' => ['ru' => 'Каменец-Подольский', 'ua' => "Кам'янець-Подільський"],
  'severodonetsk' => ['ru' => 'Северодонецк', 'ua' => 'Сєвєродонецьк'],
  'pavlograd' => ['ru' => 'Павлоград', 'ua' => 'Павлоград'],
  'lisichansk' => ['ru' => 'Лисичанск', 'ua' => 'Лисичанськ'],
  'kramatorsk' => ['ru' => 'Краматорск', 'ua' => 'Краматорськ'],
  'konstantinovka' => ['ru' => 'Константиновка', 'ua' => 'Костянтинівка'],
  'kamenskoye' => ['ru' => 'Каменское', 'ua' => "Кам'янське"],
  'brovary' => ['ru' => 'Бровары', 'ua' => 'Бровари'],
  'aleksandriya' => ['ru' => 'Александрия', 'ua' => 'Олександрія'],
);

$style = '<style>.cities_list span {
              display: inline-block;
              padding: 0px 10px 5px 5px;
          }</style>';
$h1_replacer = '';
if ($curr_uri == '/elektricheskij-teplyj-pol'){
  $h1_replacer = 'Электрический теплый пол';
}
$breadcrumb_replacer = '';

$cities_ul = '<p><div class="cities_list"><span>Интернет магазин «Теплый Пол» осуществляет доставку по всем городам Украины: </span> ';
foreach ($cities_arr as $cities_slug => $cities_rus) {
    if ($curr_uri == '/'.$cities_slug){
      $h1_replacer = 'Оплата и доставка в '.$cities_rus['ru'];
      $breadcrumb_replacer = '<li><a href="/'.$cities_slug.'">'.$cities_rus['ru'].'</a></li>';
      continue;
    } else {
      if (strpos($_SERVER['REQUEST_URI'], '/ua') !== false) {
        $cities_ul .= '<span style="display: inline-block;"><a href="/ua/'.$cities_slug.'">'.$cities_rus['ua'].'</a></span>';
      } else {
        $cities_ul .= '<span style="display: inline-block;"><a href="/'.$cities_slug.'">'.$cities_rus['ru'].'</a></span>';
      }
    }
} 
$cities_ul .= '</div></p>';
?>
<style>
.cities_list span {
  display: inline-block;
  padding: 0px 10px 5px 5px;
}</style>
<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container">
  <?php echo $content_top; ?>
  <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1><?php echo $h1_replacer; ?></h1>
      <ul class="breadcrumbs">
        <?php 
$ldJson = array(
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => array()
);
        ?>
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) { 
  if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
$ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          );
          ?>
        <? if($key == count($breadcrumbs)){ ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <? } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <? } ?>
        <?php } ?>
      </ul>
       <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 page--content">
      <?php
        if ($curr_uri != '/elektricheskij-teplyj-pol'){
        ?>
        <?=$cities_ul ?>
        <?php
      }
      ?>
      <!--seo_text_start-->
      <?php echo $description; ?>
      <!--seo_text_end-->
    </div>
  </div>
  <?php echo $column_right; ?>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>