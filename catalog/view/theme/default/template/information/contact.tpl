<?php echo $header; ?>
<script>
  $("main").addClass("contacts");
</script>
<?php echo $column_left; ?>
<div class="container">
  <?php echo $content_top; ?>
  <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumbs">
        <?php 
$ldJson = array(
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => array()
);
        ?>
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) {
        if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
$ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          );
        ?>
        <? if($key == count($breadcrumbs)){ ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <? } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <? } ?>
        <?php } ?>
      </ul>
      <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
    </div>
  </div>
  <div class="page-content row">
    <? if($maps) { ?>
    <div class="col-xs-12">
      <div class="map">
        <div class="map-dots">
          <div class="map-dots--header">
            <h3>Адреса магазинов:</h3>
          </div>
          <div class="map-dots--scroll">
            <? foreach($maps as $key => $map) { ?>
            <div class="map-dot">
              <p><strong><?=$key+1;?>. <?php echo (!empty($map['name']) ? '«' . $map['name'] . '»' : ''); ?></strong> г. <?=$map['city'];?></p>

              <address><?=$map['address'];?></address>
              <? foreach ($map['tels'] as $tel) { ?>
              <p style="padding: 0;"><a href="tel:<?=$tel;?>"><?=$tel;?></a></p>
              <? } ?>
              <button class="btn btn-light_y" data-address="<?=$key+1;?>">Показать на карте</button>
            </div>
            <? } ?>
          </div>
        </div>
        <div id="map"></div>
        <div id="content">
          <address></address>
          <p class="day0"></p>
          <p class="day1"></p>
          <p class="day2"></p>
          <p class="day3"></p>
          <p class="day4"></p>
          <p class="day5"></p>
          <p class="day6"></p>
          <p class="day7"></p>
          <p class="day8"></p>
          <p class="day9"></p>
          <p class="day10"></p>
        </div>
        <script>
            var markersData = [
                <? foreach($maps as $key => $map) { ?>
                {
                    lat: <?=$map['geo'][0]; ?>,
                    lng: <?=$map['geo'][1]; ?>,
                    name: "<?=$key+1;?>. <?=$map['name']; ?>",
                    address:"<?=$map['address']; ?>",
                    <? foreach($map['time_work'] as $key=> $date) { ?>
                    day<?=$key;?>:"<?=trim($date); ?>",
                    <? } ?>
                },
                <? } ?>
            ];
            </script>
        <script>
            var map, infoWindow;
            $('.map-dot').on('click', '.btn', function(){
                var dot = $(this).attr('data-address');
                dot--;
                console.log(dot);
                map.setCenter({lat: markersData[dot].lat+0.01, lng: markersData[dot].lng},{ checkZoomRange: true });
                google.maps.event.trigger(map, 'click');
            });
            function initMap() {
                var centerLatLng = new google.maps.LatLng(56.2928515, 43.7866641);
                var mapOptions = {
                    center: centerLatLng,
                    zoom: 8
                };
                map = new google.maps.Map(document.getElementById("map"), mapOptions);
                infoWindow = new google.maps.InfoWindow();
                google.maps.event.addListener(map, "click", function() {
                    infoWindow.close();
                });
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < markersData.length; i++){
                    var latLng 	= new google.maps.LatLng(markersData[i].lat, markersData[i].lng),
                        lat 		= markersData[i].lat,
                        lng 		= markersData[i].lng,
                        name 		= markersData[i].name,
                        address 	= markersData[i].address,
                        day0 		= markersData[i].day0,
                        day1 	    = markersData[i].day1,
                        day2 		= markersData[i].day2,
                        day3 		= markersData[i].day3,
                        day4 		= markersData[i].day4,
                        day5 		= markersData[i].day5,
                        day6 		= markersData[i].day6,
                        day7 		= markersData[i].day7,
                        day8 		= markersData[i].day8,
                        day9 		= markersData[i].day9,
                        day10 		= markersData[i].day10,
                        j 			= i;

                    j++;

                    addMarker(latLng, lat, lng,  name, address, day0, day1, day2, day3, day4, day5, day6, day7, day8, day9, day10, j);
                    bounds.extend(latLng);
                }map.fitBounds(bounds);
            }

            var image = "img/map-icon.png",
                activeMarker, active_dot;

            google.maps.event.addDomListener(window, "load", initMap);

            function addMarker(latLng, lat, lng, name, address, day0, day1, day2, day3, day4, day5, day6, day7, day8, day9, day10, i) {
                definePopupClass();
                var marker 			= new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: name,
                    icon: image,
                    label: {color: '#fff', fontSize: '12px', fontWeight: '600', text: ''+i+''},
                });
                google.maps.event.addListener(marker, "click", function() {
                    activeMarker && activeMarker.setIcon('img/map-icon.png');
                    activeMarker && activeMarker.setLabel({color: '#fff', fontSize: '12px', fontWeight: '600', text: ''+active_dot+''},);
                    map.setCenter({lat: lat+0.02, lng: lng},{ checkZoomRange: true });
                    definePopupClass();
                    marker.setIcon('img/map-icon1.png');
                    marker.setLabel('');

                    $('#content').show();
                    $('#content').find('h3').text(name);
                    $('#content').find('address').text(address);
                    $('#content').find('.day0').text(day0);
                    $('#content').find('.day1').text(day1);
                    $('#content').find('.day2').text(day2);
                    $('#content').find('.day3').text(day3);
                    $('#content').find('.day4').text(day4);
                    $('#content').find('.day5').text(day5);
                    $('#content').find('.day6').text(day6);
                    $('#content').find('.day7').text(day7);
                    $('#content').find('.day8').text(day8);
                    $('#content').find('.day9').text(day9);
                    $('#content').find('.day10').text(day10);

                    var popup = new Popup(
                        latLng,
                        document.getElementById('content'));
                    popup.setMap(map);
                    activeMarker = marker;
                    active_dot = i;
                });
                google.maps.event.addListener(map, "click", function() {
                    marker.setIcon('img/map-icon.png');
                    marker.setLabel({color: '#fff', fontSize: '12px', fontWeight: '600', text: ''+i+''},);
                    $('#content').hide();
                });
            }
            function definePopupClass() {
                Popup = function(position, content) {
                    this.position = position;

                    content.classList.add('popup-bubble-content');

                    var pixelOffset = document.createElement('div');
                    pixelOffset.classList.add('popup-bubble-anchor');
                    pixelOffset.appendChild(content);

                    this.anchor = document.createElement('div');
                    this.anchor.classList.add('popup-tip-anchor');
                    this.anchor.appendChild(pixelOffset);
                    this.stopEventPropagation();
                };
                Popup.prototype = Object.create(google.maps.OverlayView.prototype);

                Popup.prototype.onAdd = function() {
                    this.getPanes().floatPane.appendChild(this.anchor);
                };

                Popup.prototype.onRemove = function() {
                    if (this.anchor.parentElement) {
                        this.anchor.parentElement.removeChild(this.anchor);
                    }
                };

                Popup.prototype.draw = function() {
                    var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
                    var display =
                        Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                            'block' :
                            'none';

                    if (display === 'block') {
                        this.anchor.style.left = divPosition.x + 'px';
                        this.anchor.style.top = divPosition.y + 'px';
                    }
                    if (this.anchor.style.display !== display) {
                        this.anchor.style.display = display;
                    }
                };

                Popup.prototype.stopEventPropagation = function() {
                    var anchor = this.anchor;
                    anchor.style.cursor = 'auto';

                    ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
                        'pointerdown']
                        .forEach(function(event) {
                            anchor.addEventListener(event, function(e) {
                                e.stopPropagation();
                            });
                        });
                };
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDM2NuSo5ZC3AW6ZYgxnoe6Zn3jI1sEu-U&callback=initMap"></script>
      </div>
      <div class="socials contacts">
        <!--<p>Мы в соцсетях:</p>
        <a href="" target="_blank" class="icon icon-inst"></a>
        <a href="" target="_blank" class="icon icon-twit"></a>
        <a href="" target="_blank" class="icon icon-face"></a>-->
        <p>Почта:</p>
        <a href="mailto:<?=$conf_mail;?>?subject=contact-In-therm" class="email"><?=$conf_mail;?></a>
      </div>
    </div>
    <? } ?>
  </div>
  <?php echo $content_bottom; ?>
</div>
    <?php echo $column_right; ?>
<?php echo $footer; ?>
