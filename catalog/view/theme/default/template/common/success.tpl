<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container">
  <?php echo $content_top; ?>
  <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumbs">
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
        <? if($key == count($breadcrumbs)){ ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <? } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <? } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="row page--content">
    <div class="col-xs-12">



      <?php echo $text_message; ?>

      <?php if (isset($order_id)) { ?>
      <div id="print" style="display: none">
        <div style="padding: 0px 0px 20px 0px;">
          <?php echo $store_name; ?><br />
          <?php echo $store_address; ?><br />
          <?php echo $store_emal; ?><br />
          <?php echo $store_tel; ?>
        </div>
        <table class="table table-bordered table-hover">
          <thead>
          <tr>
            <td class="text-left" colspan="2"><?php echo $text_order_detail; ?></td>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
              <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
              <?php } ?>
              <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
              <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
            <td class="text-left"><?php if ($payment_method) { ?>
              <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
              <?php } ?>
              <?php if ($shipping_method) { ?>
              <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
              <?php } ?></td>
          </tr>
          </tbody>
        </table>
        <table class="table table-bordered table-hover">
          <thead>
          <tr>
            <td class="text-left" style="width: 50%;"><?php echo $text_payment_address; ?></td>
            <?php if ($shipping_address) { ?>
            <td class="text-left"><?php echo $text_shipping_address; ?></td>
            <?php } ?>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td class="text-left"><?php echo $payment_address; ?></td>
            <?php if ($shipping_address) { ?>
            <td class="text-left"><?php echo $shipping_address; ?></td>
            <?php } ?>
          </tr>
          </tbody>
        </table>
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <td class="text-left"><?php echo $column_name; ?></td>
              <td class="text-left"><?php echo $column_model; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="text-left"><?php echo $product['name']; ?>
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } ?></td>
              <td class="text-left"><?php echo $product['model']; ?></td>
              <td class="text-right"><?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['price']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-left"><?php echo $voucher['description']; ?></td>
              <td class="text-left"></td>
              <td class="text-right">1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="3"></td>
              <td class="text-right"><b><?php echo $total['title']; ?></b></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
            </tfoot>
          </table>
        </div>
        <?php if ($comment) { ?>
        <table class="table table-bordered table-hover">
          <thead>
          <tr>
            <td class="text-left"><?php echo $text_comment; ?></td>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
          </tbody>
        </table>
        <?php } ?>
        <?php if ($histories) { ?>
        <h3><?php echo $text_history; ?></h3>
        <table class="table table-bordered table-hover">
          <thead>
          <tr>
            <td class="text-left"><?php echo $column_date_added; ?></td>
            <td class="text-left"><?php echo $column_status; ?></td>
            <td class="text-left"><?php echo $column_comment; ?></td>
          </tr>
          </thead>
          <tbody>
          <?php foreach ($histories as $history) { ?>
          <tr>
            <td class="text-left"><?php echo $history['date_added']; ?></td>
            <td class="text-left"><?php echo $history['status']; ?></td>
            <td class="text-left"><?php echo $history['comment']; ?></td>
          </tr>
          <?php } ?>
          </tbody>
        </table>
        <?php } ?>
      </div>
      <?php } ?>

      <script>
        dataLayer.push({
          'ecommerce': {
            'purchase': {
              'actionField': {
                'id': '<?php echo $order_id; ?>',
                'affiliation': '<?php echo $store_name; ?>',
              },
              'products': [{
                'name': '<?php echo $product['name']; ?>',
                'id': '<?php echo $product['product_id']; ?>',
                'price': '<?php echo $product['price_anal']; ?>',
                'brand': '<?php echo $product['manufacturer']; ?>',
                'category': 'Каталог',
                'variant': '<?php echo $product['location']; ?>',
                'quantity': <?=$product['quantity']; ?>,
              }]
            }
          }
        });
      </script>

      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
    </div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<?php echo $column_right; ?>
<?php echo $footer; ?>