<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5LHXFN9');</script>

    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="plugins/owl.carousel.min.css">
    <link rel="stylesheet" href="plugins/owl.theme.default.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="plugins/owl.carousel.min.js"></script>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ac-style.css">
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <script src="js/script.js"></script>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
    <style>
        .video-banner a {
            width: 100%;
            height: 100%;
        }
    </style>
	<!-- <?php if ($lang == 'uk') { ?>
   <style>
   .text_gen_on_prods{display:none;}
   </style>
	 <?php } ?> -->
    <?php if($robots) { ?>
        <meta name="robots" content="noindex, nofollow" />
    <?php } ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(84842521, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
       });
    </script>
    <meta name="google-site-verification" content="81Cz0C6IvBirn5MDyNTyqd1gxJN0d-TQgS-uuj-3Mq4" />
</head>


    

<body class="<?php echo $class; ?>">
<noscript><div><img src="https://mc.yandex.ru/watch/84842521" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LHXFN9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<input type="hidden" value="<?=$curRight; ?>" id="curRight">
<header class="fixed">
    <div class="container">
        <div class="row">
            <div class="col-xs-2 col-sm-1 hidden-md hidden-lg">
                <div class="burger">
                    <div class="burger-btn"><span></span><span></span><span></span></div>
                </div>
				<?php echo $language; ?>
            </div>
            <div class="col-xs-8 col-sm-10 col-md-3 header-logo">
                <?php if ($logo) { ?>
                <a href="<?php echo $home; ?>" class="logo"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                                                 alt="<?php echo $name; ?>"/></a>
                <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>
            </div>
            <div class="col-xs-4 col-sm-2 col-lg-5 header-menu col-md-5">
                <div class="row row--p-5">
                    <div class="col-md-3">
                        <div class="inline m-15">
						 <div class="hidden-sm hidden-xs">
						<?php echo $language; ?>
						 </div>
                            <?php echo $search; ?>
                            <?=$cart2; ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="menu-btn">
                            <?php foreach ($items as $item) { ?>
                            <?php if (($item['children'])&&($item['type'] != "link")) { ?>
                            <div class="nav-btn">
                                <?php echo $item['name']; ?><div class="arrow"><span></span><span></span></div>
                            </div>
                            <? } ?>
                            <? } ?>
                        </div>
                    </div>
                    <? /* if($tels) { ?>
                    <? $telNames = [' ','','']; ?>
                    <div class="col-md-6 tel-tabs">
                        <ul class="nav nav-tabs" id='tabs-tel'>
                            <? foreach($tels as $key => $tel) { ?>
                            <li class="<?=($key == 0)?'active':''; ?>"><a data-toggle="tab" href="#tel<?=$key;?>"><?=$telNames[$key]; ?></a></li>
                            <? } ?>
                        </ul>
                        <div class="tab-content">
                            <? foreach($tels as $key => $tel) { ?>
                            <div id="tel<?=$key;?>" class="tab-pane fade <?=($key == 0)?'in active':''; ?>">
                                <a href="tel:<?=$tel;?>"><?=$tel; ?></a>
                            </div>
                            <? } ?>
                        </div>
                    </div>
                    <script>
                        $("#tabs-tel a").click(function(e){
                            e.preventDefault();
                            e.stopImmediatePropagation();
                            $(this).tab('show');
                        });
                    </script>
                    <? } */ ?>
                    <? if ($tels) { ?>
                    <div class="col-md-6 tel-tabs">
                        <div class="tab-content">
                            <? foreach($tels as $tel) { ?>
                            <a href="tel:<?=$tel;?>"><?=$tel; ?></a>
                            <? } ?>
                        </div>
                    </div>
                    <? } ?>
                </div>
                <div class="row row--p-5">
                    <nav>
                        <ul>
                            <?php foreach ($items as $item) { ?>
                                <?if(($item['type']=="category")&&(!empty($item['children']))){?>
                                <? } else { ?>
                                    <li><a <?php if($item['use_target_blank'] == 1) { echo ' target="_blank" ';} ?> href="<?php echo $item['href']; ?>"><?php echo $item['name']; ?></a></li>
                                <? } ?>
                            <? } ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="info-box" id="info">
                <div class="info-box--content product-added"></div>
                <div class="info-box--content product-comparison"></div>
            </div>
            <?php foreach ($items as $item) { ?>
            <?if(($item['type']=="category")&&(!empty($item['children']))){?>
            <div  class="dropdown-nav">
                <div class="flex-row">
                    <ul class="dropdown-nav--menu">
                        <? foreach($item['children'] as $child) { ?>
                        <li><a href="<?=$child['href'];?>"><?=$child['name'];?></a></li>
                        <? } ?>
                    </ul>
                    <div class="dropdown-nav--img">
                        <? foreach($item['children'] as $key => $child) { ?>
                        <img src="<?=$child['thumb'];?>" alt="<?=$child['name'];?>"
                             class="<?=($key==0)?'active':'';?>">
                        <? } ?>
                    </div>
                </div>
            </div>
            <? } ?>
            <? } ?>
            <?=$cart; ?>
        </div>
    </div>
</header>
<main>