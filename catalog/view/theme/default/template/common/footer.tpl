</main>
<footer>
  <div class="container">
    <!--Button to top-->
    <div class="to-top">
      <span class="btn to-top-btn"><span class="owl-arrow transform-90 black"></span></span>
      <span class="text">Наверх</span>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-5 col-md-4 organization">
        <a href="/" class="logo footer--title"><img src="img/logo-w.png" alt="logo"></a>
        <p><?=$config_name;?></p>
		<div>
			<span>г. Киев</span>
			<span>ул. Радищева 12/16</span>
        </div>
		<div>
			<span>г. Харьков</span>
			<span>пр. Московский 247</span>
      <div><span>Получить консультацию по теплому полу:</span></div>
          <div><span><a href="tel:0970116139">(097) 011 61 39;</a> <a href="tel:0951448018">(095) 144 80 18;</a></span></div>
     
      <div><span>По вопросам дилерства:</span></div>
          <div><span><a href="tel:(067) 573 71 44">(067) 573 71 44;</a> <a href="tel:(095) 420 64 83">(095) 420 64 83;</a></span></div>
      <div><span><a href="tel:(093) 99 44 665">(093) 99 44 665.</a> </span></div>

        </div>
        <? if($tels) { ?>
        <? foreach($tels as $tel) { ?>
        <a href="tel:<?=$tel;?>"><span><?=$tel;?></span></a>
        <? } ?>
        <? } ?>
        <!--<div class="socials">
          <a href="" target="_blank" class="icon icon-inst-white"></a>
          <a href="" target="_blank" class="icon icon-twit-white"></a>
          <a href="" target="_blank" class="icon icon-face-white"></a>
        </div>-->
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4">
        <?php if ($informations) { ?>
        <div class="footer--title">Навигация</div>
        <ul class="nav">
              <?php foreach ($informations as $information) { ?>
              <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
              <?php } ?>
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
		  <?php if ($lang != 'uk') { ?>
          <li><a href="/sitemap/">Карта сайта</a></li>
	    <?php } else { ?>
          <li><a href="/ua/sitemap/">Карта сайту</a></li>
      <?php } ?>
          
          <li><a href="/elektricheskij-teplyj-pol">Электрический теплый пол</a></li>
        </ul>
        <?php } ?>
        <!--<a href="javascript:void(0);" class="btn white-btn">Расчет стоимости</a>-->
      </div>
      <!--<div class="col-xs-12 col-sm-5 col-md-3">
        <div class="footer--title">Представительства</div>
        <a href="https://www.tepliypol.com.ua/kiev" class="footer--article">Киев</a>
        <a href="https://www.tepliypol.com.ua/harkov" class="footer--article">Харьков</a>
        <a href="/vinnitca" class="footer--article">Винница</a>
        <a href="/dnepr" class="footer--article">Днепр</a>
        <a href="/zhitomir" class="footer--article">Житомир</a>
        <a href="/zaporozhe" class="footer--article">Запорожье</a>
        <a href="/ivano-frankovske" class="footer--article">И.Франковск</a>
        <a href="/kropivnitcki" class="footer--article">Кропивницкий</a>
        <a href="/lutck" class="footer--article">Луцк</a>
        <a href="/lvov" class="footer--article">Львов</a>
        <a href="/nikolaev" class="footer--article">Николаев</a>
        <a href="/odessa" class="footer--article">Одесса</a>
        <a href="/poltava" class="footer--article">Полтава</a>
          <a href="/rovno" class="footer--article">Ровно</a>
          <a href="/summy" class="footer--article">Суммы</a>
          <a href="/ternopol" class="footer--article">Тернополь</a>
          <a href="/uzhgorod" class="footer--article">Ужгород</a>
          <a href="/kherson" class="footer--article">Херсон</a>
          <a href="/khmelnitckii" class="footer--article">Хмельницкий</a>
          <a href="/chernigov" class="footer--article">Чернигов</a>
          <a href="/cherkassy" class="footer--article">Черкассы</a>
          <a href="/krivoi-rog" class="footer--article">Кривой Рог</a>
      </div>-->
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="footer--title">Новостная рассылка</div>
        <p>Подпишитесь на рассылку и будьте всегда в курсе последних событий</p>
        <form class="input newsletter" >
          <input class="inp-text" id="input-newsletter" type="email" name="email" placeholder="">
          <label>Электронная почта</label>
          <span id="error-msg"></span>
          <span class="focus-border"></span>

          <button type="submit" id="subcribe" class="btn send-btn"><span class="owl-arrow black"></span></button>
        </form>
        <!--a href="#" onclick="document.getElementById('pricelist').submit();"  class="download">Скачать прайс.xls</a-->
        <a href="/pricelist.xls" class="download">Скачать прайс.xls</a>
        <form id="pricelist" action="/index.php?route=tool/export_import/download" method="post" enctype="multipart/form-data" style="display:none">
          <input type="hidden" name="export_type" value="p" checked="checked" />
        </form>
        <div class="site-info">
          <p>© Теплые полы 2018</p>
        </div>
      </div>
    </div>
  </div>
    <!--footers_block-->
</footer>
<script type="text/javascript" language="javascript">
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
    $(document).ready(function() {
        //footer
        $('#subcribe').click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $('#error-msg').html('');
            var email = $('#input-newsletter').val();
            if (email == '') {
                var error = 'Введите e-mail';
            }
            if (!validateEmail(email)) {
                var error = 'Не верно введен e-mail';
            }
            if (error != null) {
                $('#error-msg').html('');
                $('#error-msg').append('<b style=\"color:red\">' + error + '</b>');
            } else {
                var dataString = 'email=' + email;
                $.ajax({
                    url: 'index.php?route=common/footer/addToNewsletter',
                    type: 'post',
                    data: dataString,
                    success: function (html) {
                        $('#error-msg').append('<b style=\"color:green\">' + html + '</b>');
                    }
                });
            }
        });
    });
    $(document).on("click",'.close_info-box',function(){
        $('.info-box').stop().fadeOut();
        $('.info-box--content').delay(750).hide();
    });
</script>

<script>
  let form = '<form action="" class="form feedbackform feedback-form_main">' +
  '<div class="same-col-width">' +
  '<div class="input">' +
  '        <input class="inp-text" type="text" name="name" placeholder="">' +
  '            <label><?=$callAskMe['name'];?></label>' +
  '            <span class="focus-border">' +
  '            <i></i>' +
  '            </span>' +
  '        </div>' +
  '        <div class="input">' +
  '        <input class="inp-text" type="tel" name="tel" placeholder="">' +
  '            <label><?=$callAskMe['tel'];?></label>' +
  '            <span class="focus-border">' +
  '            <i></i>' +
  '            </span>' +
  '        </div>' +
  '</div>' +
  '<div class="same-col-width align-end">' +
  '<div class="agree">' +
  '<input type="checkbox" id=\'agree_main\'><label for="agree_main">Даю согласие на обработку личных данных</label>' +
  '</div>' +
  '<div>' +
  '<button type="reset" class="hidden"></button>' +
  '<input type="submit" name="send" value="Отправить" class="btn btn-yellow submit">' +
  '</div>' +
  '</div>' +
  '</form>';
  $(document).ready(function () {
    if ($(".formfeed").length) {
        $(".formfeed").after(form).remove();
    }
  });


</script>

<div class="forms common-forms">
  <div class="flex-center">
    <div class="overlay"></div>
    <form action class="form feedback-form feedbackform">
      <div class="form--title">
        <div class="h2-style"><?=$callAskMe['header'];?></div><div class="close-form"><span></span><span></span></div>
      </div>
      <div class="form--scroll">
        <div class="form--description">
          <p><?=strip_tags($callAskMe['header_after']);?></p>
        </div>
        <div class="form--content">
          <div class="same-col-width">
            <div class="input">
              <input class="inp-text" type="text" name="name" placeholder="">
              <label><?=$callAskMe['name'];?></label>
              <span class="focus-border"></span>
            </div>
            <div class="input">
              <input class="inp-text" type="tel" name="tel" placeholder="">
              <label><?=$callAskMe['tel'];?></label>
              <span class="focus-border"></span>
            </div>
          </div>
          <div class="same-col-width align-end">
            <div class="agree">
              <input type="checkbox" required id='agree'><label for="agree"><?=$callAskMe['name_after'];?></label>
            </div>
            <div>
              <button type="reset" class="hidden"></button>
              <input type="submit" name="send" value="<?=$callAskMe['btn_ok'];?>" class="btn btn-yellow">
            </div>
          </div>
        </div>
      </div>
    </form>
    <div class="form info-form">
      <div class="form--title">
        <div class="h2-style"><?=$callAskMe['complete_send'];?></div><div class="close-form"><span></span><span></span></div>
      </div>
      <div class="form--description">
        <p><?=$callAskMe['text_after'];?></p>
      </div>
    </div>
  </div>
</div>

<div class="forms page-forms" id="msimilar">
  <div class="flex-center">
    <div class="overlay"></div>
    <div class="insert"></div>
  </div>
</div>

<div class="forms page-forms checkout-payment" id="payment">
  <div class="flex-center">
    <div class="overlay"></div>
    <div class="insert"></div>
  </div>
</div>

<style>
  .has-error{
    color: #f00!important;
    border: solid 1px #f00!important;
  }
  .has-error * {
    color: #f00!important;
  }
</style>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
<script src='plugins/jquery.hyphen.ru.min.js'></script>
<script src='plugins/jquery.inputmask.min.js'></script>
<script src="catalog/view/javascript/lazyload/jquery.lazyload.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('img.lazy-load').lazyload({
            effect: "fadeIn",
            effectspeed: 500,
            threshold: 100,
            placeholder : "catalog/view/javascript/lazyload/loading.gif"
        });
    });
</script>
<script>
    function similar(product_id) {
        $( "#msimilar .flex-center .insert" ).load( "index.php?route=extension/module/similar&product_id="+product_id );
    }
</script>

<?php if ($live_search_ajax_status):?>
<script type="text/javascript"><!--
var live_search = {
    selector: '#search input[name=\'search\']',
    text_no_matches: '<?php echo $text_empty; ?>',
    height: '50px'
}

$(document).ready(function() {
    var html = '';
    html += '<div class="live-search table_search">';
    html += '	<div class="live-search--content">';
    html += '	</div>';
    html += '<div class="live-search--text"></div>';
    html += '</div>';

    //$(live_search.selector).parent().closest('div').after(html);
    $(live_search.selector).after(html);

    $(live_search.selector).autocomplete({
        'source': function(request, response) {
            var filter_name = $(live_search.selector).val();
            var live_search_min_length = '<?php echo (int)$live_search_min_length; ?>';
            if (filter_name.length < live_search_min_length) {
                $('.live-search').css('display','none');
            }
            else{
                var html = '';
                html += '<div class="live-search--row">';
                html +=	'<img class="loading" src="catalog/view/theme/default/image/loading.gif" />';
                html +=	'</div>';
                $('.live-search .live-search--content').html(html);
                $('.live-search').css('display','block');
/*
*<div class="live-search table_search">
            <div class="live-search--content">
              <div class="live-search--row">


                <div class="live-search--img"><img src="img/products/4.png" alt=""></div>
                <a href="/product.html">Тонкий нагревательный мат Hemstedt DH 1,5 м кв./225 Вт</a>
                <big>3 487 грн</big>


              </div>
            </div>
            <div class="live-search--text">
              <a href="/search-result.html" class="button btn-light_y">Показать все результаты</a>
            </div>
          </div>

* */
                $.ajax({
                    url: 'index.php?route=product/live_search&filter_name=' +  encodeURIComponent(filter_name),
                    dataType: 'json',
                    success: function(result) {
                        var products = result.products;
                        $('.live-search .live-search--content *').remove();
                        $('.result-text').html('');
                        if (!$.isEmptyObject(products)) {
                            var show_image = <?php echo $live_search_show_image;?>;
                            var show_price = <?php echo $live_search_show_price;?>;
                            var show_description = <?php echo $live_search_show_description;?>;
                            $('.live-search--text').html('<a href="<?php echo $live_search_href;?>'+filter_name+'" class="button btn-light_y"><?php echo $text_view_all_results;?> ('+result.total+')</a>');

                            $.each(products, function(index,product) {
                                var html = '';

                                html += '<div class="live-search--row">';
                                if(product.image && show_image){
                                    html += '	<div class="live-search--img"><img src="' + product.image + '"></div>';
                                }
                                html += '<a href="' + product.url + '"><div class="product-name">'+ product.name +'</div></a>';

                                if(show_description){
                                    html += '<p>' + product.extra_info + '</p>';
                                }
                               // html += '</div>';//div s old.price /s p big new.price /big /p /div
                                if(show_price){
                                    if (product.special) {
                                        html += '	<div class="product-price"><s><big>' + product.price + '</big></s><span class="price">' + product.special + '</span class="price"></div>';
                                    } else {
                                        html += '	<big>' + product.price + '</big>';
                                    }
                                }
                                html += '</div>';
                                $('.live-search .live-search--content').append(html);
                            });
                        } else {
                            var html = '';
                            html += '<div class="live-search--row nogrid">';
                            html +=	live_search.text_no_matches;
                            html +=	'</div>';

                            $('.live-search .live-search--content').html(html);
                        }
                        // $('.live-search ul li').css('height',live_search.height);
                        $('.live-search').css('display','block');
                        return false;
                    }
                });
            }
        },
        'select': function(product) {
            $(live_search.selector).val(product.name);
        }
    });

    $(document).bind( "mouseup touchend", function(e){
        var container = $('.live-search');
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
});
//--></script>
<?php endif;?>
<?php if($_SERVER['REQUEST_URI'] == '/') { ?>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://www.tepliypol.com.ua/",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://www.tepliypol.com.ua/search/?search={search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
</script>
<?php } ?>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "https://www.tepliypol.com.ua",
        "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+38 (067) 35 33 999",
            "contactType": "customer service"
        },{
            "@type": "ContactPoint",
            "telephone": "+38 (099) 251 43 41",
            "contactType": "customer service"
        },{
            "@type": "ContactPoint",
            "telephone": "+38 (063) 522 3 111",
            "contactType": "customer service"
        }],
        "address" : [
            {
                "@type": "PostalAddress",
                "addressLocality": "Киев",
                "streetAddress": "ул. Радищева 12/16"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Харьков",
                "streetAddress": "проспект Московский, 247 оф.3"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Винница",
                "streetAddress": "ул. Соборная, 4"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Житомир",
                "streetAddress": "ул. Покровская, 73"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Ивано-Франковск",
                "streetAddress": "ул. Черновола 99"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Луцк",
                "streetAddress": "проспект Воли 6 оф.9"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Львов",
                "streetAddress": "ул. Княгини Ольги 120"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Николаев",
                "streetAddress": "проспект Центральный 67, оф. 420"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Одесса",
                "streetAddress": "ул. Мельницкая 3"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Полтава",
                "streetAddress": "ул. Фрунзе 146"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Сумы",
                "streetAddress": "ул. Перекопская 11"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Тернополь",
                "streetAddress": "ул. Живова 9"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Хмельницкий",
                "streetAddress": "проспект Мира, 63"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Черкассы",
                "streetAddress": "ул. Кирова 73"
            },
            {
                "@type": "PostalAddress",
                "addressLocality": "Кривой Рог",
                "streetAddress": "ул. Харцызская, 15"
            }
        ]
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Store",
        "priceRange":"UAH",
        "name": "Интернет магазин “Теплый пол”",
        "image": "https://www.tepliypol.com.ua/image/catalog/inTerm-logo-RGB(1).png",
        "openingHoursSpecification": [
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница"
                ],
                "opens": "09:00",
                "closes": "18:00"
            },
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Суббота",
                    "Воскресенье"
                ],
                "opens": "10:00",
                "closes": "16:00"
            }],
        "telephone": "+38 (067) 35 33 999",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "ул. Радищева 12/16",
            "addressLocality": "Киев",
            "addressCountry": "Украина"
        }
    }
</script>
<script type="text/javascript">
    $(function () {
        $("#spoc-accordion .toggle").on("click", function () {
            $(this).next("div").slideToggle();
            $(this).parent().toggleClass("active");
            $(this).toggleClass("active")
        })
    });
</script>

</body></html>
