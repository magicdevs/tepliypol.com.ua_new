<div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control"/>
  <div class="input-group-btn">
    <button type="submit" class="btn btn-lg"></button>
  </div>
</div>