<div id="cart">
  <div class="cart-block">
  <div class="cart-block--header">
    <div class="h3-style"><?=$text_cart_name;?></div>
    <p><?php echo $text_items; ?></p>
  </div>
  <div class="cart-block--content">
    <div class="cart-block--scroll">
      <?php if ($products) { ?>
      <?php foreach($products as $product) { ?>

      <div class="cart-block--row">
        <?php if ($product['thumb']) { ?>
        <div class="cart-block--img"><img src="<?=$product['thumb'];?>" alt="<?=$product['name'];?>"></div>
        <? } ?>
        <div class="cart-block_text flex-col-wrap">
          <p><?=$product['name'];?></p>
          <div class="flex-row-wrap">
            <div class="num">
              <div class="num-btn minus" onclick="updateQuantity('<?php echo $product['cart_id']; ?>','<?php echo $product['quantity']-1; ?>');"></div>
              <p><?=$product['quantity'];?></p>
              <div class="num-btn plus" onclick="updateQuantity('<?php echo $product['cart_id']; ?>','<?php echo $product['quantity']+1; ?>');"></div>
            </div>
            <big><?=$product['total'];?></big>
          </div>
        </div>
      </div>
      <? } ?>
      <?php } else { ?>
      <div class="cart-block--row text-center"><?php echo $text_empty; ?></div>
      <?php } ?>
    </div>
  </div>
  <div class="cart-block--footer">
    <a href="<?php echo $checkout; ?>" class="btn btn-light_y"><?php echo $text_checkout; ?></a>
    <a href="<?php echo $cart; ?>" class="btn btn-yellow"><?php echo $text_cart; ?></a>
  </div>
</div>
</div>