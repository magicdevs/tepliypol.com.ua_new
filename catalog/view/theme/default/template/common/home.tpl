<?php echo $header; ?>
  <div class="container">
    <?php echo $column_left; ?>
    <?php echo $content_top; ?>
  </div>
<?php echo $content_bottom; ?>
<? if($comments) { ?>
    <div class="container">
  <section class="comments">
    <div class="row">
      <div class="col-xs-12">
        <div class="h2-style">Отзывы</div>
      </div>
    </div>
    <div class="owl-carousel owl-comments">
      <? foreach($comments as $comment) { ?>
      <div class="comment">
        <div class="flex-row">
          <div class="comment--photo"><img src="<?=$comment['thumb'];?>" alt="<?=$comment['name'];?>"></div>
          <div class="comment--header">
            <div class="h3-style"><?=$comment['name'];?></div>
            <span class="i-style"><?=$comment['city'];?></span>
          </div>
        </div>
        <div class="comment--text">
          <p><?=$comment['description'];?></p>
        </div>
      </div>
      <? } ?>
    </div>
    <script>
        function comments_function(){$('.owl-comments').owlCarousel({
            loop: false,
            margin: 30,
            stagePadding: 10,
            nav: true,
            dots: false,
            navText: ['<span class="owl-arrow transform-180"></span>', '<span class="owl-arrow"></span>'],
            responsive:{
                0:{
                    items:1,
                    touchDrag: true,
                    autoHeight: true,
                },
                767:{
                    items:2,
                    mouseDrag: true,
                }
            }
        });}

        let comment = $('.owl-comments').html();
        var comments = function(){
            if($(window).width()<768){
                $('.owl-comments').owlCarousel('destroy');
                $('.owl-comments').html(comment);
                var j = 0,
                    comments_num = $('.comment').length;
                do{
                    $('.owl-comments').append('<div class="comment-col"></div>');
                    j++;
                }while(Math.ceil(comments_num/2) != j);
                j=0;
                while(j!=comments_num){
                    if(comments_num%2 && j==Math.ceil(comments_num/2)-1){
                        $($('.comment-col')[j]).append('<div>'+ $($('.comment')[0]).html()+'</div>');
                        $($('.comment')[0]).remove();
                    }else{
                        $($('.comment-col')[j]).append('<div>'+ $($('.comment')[0]).html()+'</div>');
                        $($('.comment')[0]).remove();
                        $($('.comment-col')[j]).append('<div>'+ $($('.comment')[0]).html()+'</div>');
                        $($('.comment')[0]).remove();
                    }
                    j++;
                }
                $('.comment-col').find('>div').addClass('comment');
                comments_function();
            }else{
                $('.owl-comments').owlCarousel('destroy');
                $('.owl-comments').html(comment);
                comments_function();
            }
        };
        $(window).resize(function(){
            //comments();
        });
        //comments();
        comments_function();
    </script>
  </section>
</div>
<? } ?>
<?php echo $column_right; ?>
<?php echo $footer; ?>