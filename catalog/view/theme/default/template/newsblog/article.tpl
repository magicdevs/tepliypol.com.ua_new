<?php echo $header; ?>
<style>
    .share_center {
       width: 290px;
    }
</style>
<?php echo $column_left; ?>
<?php $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<div itemscope itemtype="http://schema.org/Article" class="container">
	<meta itemprop="author" content="Admin" />
	<meta itemprop="datePublished" content="<?php echo $date_iso; ?>" />
	<meta itemprop="dateModified" content="<?php echo $date_modified_iso; ?>" />
    <meta itemprop="image" content="/image/catalog/img/logo.png" />
	<meta itemprop="mainEntityOfPage" content="<?php echo $actual_link; ?>" />
	<div class="hidden-block" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
		<meta itemprop="name" content="Ин Терм">
		<span itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
			<img itemprop="url image" alt="Техно Рейтинг" src="/image/catalog/img/logo.png">
		</span>
	</div>
<!--seoshield_formulas--novost-->
    <?php echo $content_top; ?>
    <div class="row">
        <div class="col-xs-12 page--header">
            <div class="floor-header"><img src="img/page-title.png" alt=""></div>
            <h1 itemprop="headline"><?php echo $heading_title; ?></h1>
            <ul class="breadcrumbs">
                <?php 
$ldJson = array(
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => array()
);
        ?>
                <?php foreach ($breadcrumbs as $key=>$breadcrumb) {
                if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
$ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          );
        ?>
                <? if($key == count($breadcrumbs)){ ?>
                <li><?php echo $breadcrumb['text']; ?></li>
                <? } else { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>
                <? } ?>
                <?php } ?>
            </ul>
             <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
        </div>
    </div>
    <article>
        <div class="row">
            <div class="col-xs-12">
                <div class="article--header">
                    <?php if ($popup) { ?>
                        <img itemprop="image" src="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                    <? } ?>
                    <div class="article--description"><p><?=$date;?></p><p><?=$viewed;?> просмотров</p>
                        <div class="share" id="share" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="img/share.png" alt=""></div>
                        <div class="dropdown-menu" aria-labelledby="share">
                            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_telegram"></a>
                                <a class="a2a_button_viber"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_skype"></a>
                                <a class="a2a_button_facebook_messenger"></a>
                                <a class="a2a_button_linkedin"></a>
                                <a class="a2a_button_twitter"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <span itemprop="articleBody" style="display: none;"><?=$description;?></span>
                <?=$description;?>
                <div class="share share_center"><p>Поделитесь статьей с друзьями</p>

                        <!-- AddToAny BEGIN -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_telegram"></a>
                        <a class="a2a_button_viber"></a>
                        <a class="a2a_button_whatsapp"></a>
                        <a class="a2a_button_skype"></a>
                        <a class="a2a_button_facebook_messenger"></a>
                        <a class="a2a_button_linkedin"></a>
                        <a class="a2a_button_twitter"></a>
                    </div>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->

                </div>

                <div class="article--footer flex-between">
                    <? if($prevname) { ?>
                    <a href="<?=$prevhref;?>" class="prev-article">
                        Предыдущая статья
                        <span><?=$prevname;?></span>
                    </a>
                    <? } ?>

                    <a href="<?=$categoryMain;?>">Ко всем статьям</a>
                    <? if($nextname) { ?>
                    <a href="<?=$nexthref;?>" class="next-article">
                        Следующая статья
                        <span><?=$nextname;?></span>
                    </a>
                    <? } ?>
                </div>
            </div>
        </div>
    </article>
    <?php echo $content_bottom; ?>
</div>

<?php echo $column_right; ?>



      <?php if ($comments_vk) { ?>
      <div class="row">
        <div class="col-md-12">
			<div id="vk_comments"></div>
			<script type="text/javascript">
			VK.init({apiId: <?php echo $comments_vk; ?>, onlyWidgets: true});
			VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"});
			</script>
        </div>
      </div>
      <?php } ?>

      <?php if ($comments_fb) { ?>
      <div class="row">
        <div class="col-md-12">
            <div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-comments" data-href="<?php echo $canonical; ?>" data-width="100%" data-numposts="10"></div>
        </div>
      </div>
      <?php } ?>

      <?php if ($comments_dq) { ?>
      <div class="row">
        <div class="col-md-12">
        	<div id="disqus_thread"></div>
			<script>
			var disqus_config = function () {
				this.page.url = "<?php echo $canonical; ?>";
			};

			(function() { // DON'T EDIT BELOW THIS LINE
			var d = document, s = d.createElement('script');
			s.src = 'https://<?php echo $comments_dq; ?>.disqus.com/embed.js';
			s.setAttribute('data-timestamp', +new Date());
			(d.head || d.body).appendChild(s);
			})();
			</script>
			<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>
      </div>
      <?php } ?>


<script type="text/javascript"><!--
$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>