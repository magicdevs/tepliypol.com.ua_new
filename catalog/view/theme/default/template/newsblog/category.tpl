<?php echo $header; ?>

<div class="container">
  <div class="row">
    <div class="col-sm-12 page--header">
      <?php if ($thumb) { ?>
      <div class="floor-header"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"></div>
      <?php } ?>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumbs">
        <?php 
$ldJson = array(
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => array()
);
        ?>
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) {
        if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
$ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          ); ?>
          <? if($key == count($breadcrumbs)){ ?>
            <li><?php echo $breadcrumb['text']; ?></li>
          <? } else { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <? } ?>
        <?php } ?>
      </ul>
       <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
    </div>
  </div>
  <section class="articles">
    <?php if ($articles) { ?>
    <div class="row">
      <div class="col-xl-12">
        <? foreach($articles as $key => $article) { ?>
          <? if (($key%2 == 0)) { ?>
            <? if ($key != 0) { ?>
              </div>
            <? } ?>
            <div class="same-col-width_grid">
          <? } ?>
              <div class="article-link">
                <div class="article-hover">
				  <div class="bg-img" style="background:url(<? echo $article['original']; ?>);"></div>
                  <h3><?php echo $article['name']; ?></h3>
                  <div class="flex-between">
                    <p><?php echo $article['date']; ?></p>
                    <a href="<?php echo $article['href']; ?>" class="read-more-btn">Читать статью</a>
                  </div>
                </div>
              </div>
        <? } ?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="flex-center">
          <?php echo $pagination; ?>
        </div>
      </div>
    </div>
    <? } else { ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="flex-center">
          <p><?php echo $text_empty; ?></p>
        </div>
      </div>
    </div>
    <? } ?>
  </section>
</div>
      <?php if ($comments_vk) { ?>
      <div class="row">
        <div class="col-md-12">
			<div id="vk_comments"></div>
			<script type="text/javascript">
			VK.init({apiId: <?php echo $comments_vk; ?>, onlyWidgets: true});
			VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"});
			</script>
        </div>
      </div>
      <?php } ?>

      <?php if ($comments_fb) { ?>
      <div class="row">
        <div class="col-md-12">
            <div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-comments" data-href="<?php echo $canonical; ?>" data-width="100%" data-numposts="10"></div>
        </div>
      </div>
      <?php } ?>

      <?php if ($comments_dq) { ?>
      <div class="row">
        <div class="col-md-12">
        	<div id="disqus_thread"></div>
			<script>
			var disqus_config = function () {
				this.page.url = "<?php echo $canonical; ?>";
			};

			(function() { // DON'T EDIT BELOW THIS LINE
			var d = document, s = d.createElement('script');
			s.src = 'https://<?php echo $comments_dq; ?>.disqus.com/embed.js';
			s.setAttribute('data-timestamp', +new Date());
			(d.head || d.body).appendChild(s);
			})();
			</script>
			<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?>
    <?php echo $column_right; ?>
<?php echo $footer; ?>