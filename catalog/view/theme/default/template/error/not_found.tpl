<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container">
  <?php echo $content_top; ?>
  <div class="row page-404">
      <?php if ($class == 'checkout-cart') { ?>
          <div class="col-xs-12 col-md-7 col-sm-offset-0 col-md-offset-1">
              <h2><?php echo $heading_title; ?></h2>
              <p><?php echo $text_error; ?></p>
              <p>Вы можете вернуться на главную страницу.</p>
              <a href="/" class="btn btn-yellow">На главную</a>
          </div>
      <? } else { ?>
    <div class="floor-404"><img src="img/cable-404.png" alt=""></div>
    <div class="col-xs-12 col-md-7 col-sm-offset-0 col-md-offset-1">
      <h1>Ошибка</h1><h1>404</h1>
      <h2><?php echo $heading_title; ?></h2>
      <p><?php echo $text_error; ?></p>
      <p>Вы можете вернуться на главную страницу.</p>
      <a href="/" class="btn btn-yellow">На главную</a>
    </div>
  </div>
<?php } ?>
  <?php echo $content_bottom; ?>
</div>
<?php echo $column_right; ?>
<?php echo $footer; ?>
