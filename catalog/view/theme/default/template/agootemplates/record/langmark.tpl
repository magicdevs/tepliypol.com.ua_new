<?php if (count($languages) > 1) { ?>
<?php if (SC_VERSION < 20) { ?>
<div id="langmark">
  <?php echo $text_language; ?>
    <?php foreach ($languages as $language) { ?>
    <?php if ($language['current']) { ?>
      <?php if ($language['main']) { ?>
      <a onclick="lm_deleteCookie('languageauto'); window.location = '<?php echo $language['url']; ?>'" href="<?php echo $language['url']; ?>"><?php if (isset($settings_widget['image_status']) && $settings_widget['image_status']) { ?><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>"><?php } ?> <?php echo $language['name']; ?></a>
      <?php } else { ?>
        <a onclick="lm_setCookie('languageauto', '1', {expires: 180}); window.location = '<?php echo $language['url']; ?>'" href="<?php echo $language['url']; ?>"><?php if (isset($settings_widget['image_status']) && $settings_widget['image_status']) { ?><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>"><?php } ?> <?php echo $language['name']; ?></a>
      <?php } ?> 
    <?php } else { ?>
    
      <?php if ($language['main']) { ?>
      <a onclick="lm_deleteCookie('languageauto'); window.location = '<?php echo $language['url']; ?>'" href="<?php echo $language['url']; ?>"><?php if (isset($settings_widget['image_status']) && $settings_widget['image_status']) { ?><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>"><?php } ?> <?php echo $language['name']; ?></a>
      <?php } else { ?>
      <a onclick="lm_setCookie('languageauto', '1', {expires: 180}); window.location = '<?php echo $language['url']; ?>'" href="<?php echo $language['url']; ?>"><?php if (isset($settings_widget['image_status']) && $settings_widget['image_status']) { ?><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>"><?php } ?> <?php echo $language['name']; ?></a>
      <?php } ?>   

    <?php } ?>
    <?php } ?>
</div>
<?php } else { ?>
<div class="pull-left">
  <div class="btn-group language" id="lang">
  <?php foreach ($languages as $language) { ?>
  <a href="<?php echo $language['url']; ?>" class="<?php if (!$language['current']) { echo 'disable-lang-wrap'; }?> <?php echo $language['code']; ?>" onclick="window.location = '<?php echo $language['url']; ?>'"> 
		<?php echo $language['name']; ?>
				</a>
				
  
	 
  <?php } ?>
  </div>
</div>
<?php } ?>

 
<?php } ?>