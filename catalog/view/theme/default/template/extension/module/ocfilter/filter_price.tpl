<?php if ($show_price) { ?>
<div class="list-group-item ocfilter-option filter--block" data-toggle="popover-price">
    <div class="ocf-option-name"><?php echo $text_price; ?>, <?php echo $symbol_right; ?>.:</div>
        <div class="range" id="price-range"></div>
        <div class="ocf-option-values">
            <div id="scale-price" class="range scale ocf-target" data-option-id="p"
                 data-start-min="<?php echo $min_price_get; ?>"
                 data-start-max="<?php echo $max_price_get; ?>"
                 data-range-min="<?php echo $min_price; ?>"
                 data-range-max="<?php echo $max_price; ?>"
                 data-element-min="#price-from"
                 data-element-max="#price-to"
                 data-control-min="#min-price-value"
                 data-control-max="#max-price-value"
            ></div>
        </div>
        <div class="from-to">
            <label>От: <input name="price[min]" value="<?php echo $min_price_get; ?>" data-index="0" placeholder="<?php echo $min_price; ?>" type="text" class="sliderValue" id="min-price-value" /></label>
            <label>До: <input name="price[max]" value="<?php echo $max_price_get; ?>" data-index="1" placeholder="<?php echo $max_price; ?>"  type="text" class="sliderValue" id="max-price-value" /></label>
            <span class="i-style"><?php echo $symbol_right; ?></span>
        </div>

</div>
<?php } ?>