<div id="scale-<?php echo $option['option_id']; ?>" class="scale ocf-target"
  data-option-id="<?php echo $option['option_id']; ?>" data-option-inputs="true"
  data-start-min="<?php echo $option['slide_value_min_get']; ?>"
  data-start-max="<?php echo $option['slide_value_max_get']; ?>"
  data-range-min="<?php echo $option['slide_value_min']; ?>"
  data-range-max="<?php echo $option['slide_value_max']; ?>"
  data-element-min="#left-value-<?php echo $option['option_id']; ?>"
  data-element-max="#right-value-<?php echo $option['option_id']; ?>"
     <? if($option['type'] == 'slide_dual') { ?>
     data-control-min="#left-value-<?php echo $option['option_id']; ?>"
     data-control-max="#right-value-<?php echo $option['option_id']; ?>"
<? } ?>
></div>
<? if($option['type'] == 'slide_dual') { ?>
<div class="from-to">
    <label>От: <input   name="option<?php echo $option['option_id']; ?>[min]" value="<?php echo $option['slide_value_min_get']; ?>" data-index="0" placeholder="<?php echo $option['slide_value_min']; ?>" type="text" id="left-value-<?php echo $option['option_id']; ?>" /></label>
    <label>До: <input name="option<?php echo $option['option_id']; ?>[max]" value="<?php echo $option['slide_value_max_get']; ?>" data-index="1" placeholder="<?php echo $option['slide_value_max']; ?>"  type="text" id="right-value-<?php echo $option['option_id']; ?>" /></label>
</div>
<? } ?>
<? /* onchange="ocfilter.update($('#scale-<?php echo $option['option_id']; ?>'));" */ ?>