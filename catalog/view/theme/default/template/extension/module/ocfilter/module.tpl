<?php if ($options || $show_price) { ?>
<div class="ocf-offcanvas ocfilter-mobile hidden-sm hidden-md hidden-lg ">
  <button class="btn btn-light_b filter-btn" data-toggle="offcanvas"></button>

  <div class="ocfilter-mobile-handle">
    <button type="button" class="btn btn-primary" data-toggle="offcanvas"><i class="fa fa-filter"></i>
      <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           viewBox="0 0 247.46 247.46" style="enable-background:new 0 0 247.46 247.46;" xml:space="preserve">
<path d="M246.744,13.984c-1.238-2.626-3.881-4.301-6.784-4.301H7.5c-2.903,0-5.545,1.675-6.784,4.301
	c-1.238,2.626-0.85,5.73,0.997,7.97l89.361,108.384v99.94c0,2.595,1.341,5.005,3.545,6.373c1.208,0.749,2.579,1.127,3.955,1.127
	c1.137,0,2.278-0.259,3.33-0.78l50.208-24.885c2.551-1.264,4.165-3.863,4.169-6.71l0.098-75.062l89.366-108.388
	C247.593,19.714,247.982,16.609,246.744,13.984z M143.097,122.873c-1.105,1.34-1.711,3.023-1.713,4.761l-0.096,73.103
	l-35.213,17.453v-90.546c0-1.741-0.605-3.428-1.713-4.771L23.404,24.682h200.651L143.097,122.873z"/>
</svg></button>
  </div>

  <div class="ocf-offcanvas-body"></div>
</div>

<div class="panel ocfilter panel-default 123" id="ocfilter">
  <div class="panel-heading"><?php echo $heading_title; ?></div>
  <div class="hidden" id="ocfilter-button">
    <button class="btn btn-primary disabled" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Загрузка.."></button>
  </div>
  <div class="list-group">
    <?php include 'selected_filter.tpl'; ?>

    <?php include 'filter_price.tpl'; ?>

    <?php include 'filter_list.tpl'; ?>
  </div>
  <!-- links_block -->
</div>
<script type="text/javascript"><!--
$(function() {
    $('body').append($('.ocfilter-mobile').remove().get(0).outerHTML);

    var options = {
        mobile: $('.filter-btn').is(':visible'),
        php: {
            searchButton : <?php echo $search_button; ?>,
    showPrice    : <?php echo $show_price; ?>,
    showCounter  : <?php echo $show_counter; ?>,
    manualPrice  : <?php echo $manual_price; ?>,
    link         : '<?php echo $link; ?>',
        path         : '<?php echo $path; ?>',
        params       : '<?php echo $params; ?>',
        index        : '<?php echo $index; ?>'
},
    text: {
        show_all: '<?php echo $text_show_all; ?>',
            hide    : '<?php echo $text_hide; ?>',
            load    : '<?php echo $text_load; ?>',
            any     : '<?php echo $text_any; ?>',
            select  : '<?php echo $button_select; ?>'
    }
};

    if (options.mobile) {
        $('.ocf-offcanvas-body').html($('#ocfilter').remove().get(0).outerHTML);
    }

    $('[data-toggle="offcanvas"]').on('click', function(e) {
        $(this).toggleClass('active');
        $('body').toggleClass('modal-open');
        $('.ocfilter-mobile').toggleClass('active');
    });

    setTimeout(function() {
        $('#ocfilter').ocfilter(options);
    }, 1);
});
//--></script>
<?php } ?>