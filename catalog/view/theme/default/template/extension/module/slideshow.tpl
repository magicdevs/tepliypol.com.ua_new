<div class="banner row">
  <div class="col-xs-12 col-sm-12 col-md-2 height_100">
    <div class="shop-description">
      <div class="text">
        <h1><?=$title;?></h1>
        <p><?=$subtitle;?></p>
          <? if ($tels) { ?>
            <div class="numbers">
                <? foreach($tels as $tel) { ?>
                    <a href="tel:<?=$tel;?>"><?=$tel; ?></a>
                <? } ?>
            </div>
          <? } ?>
      </div>
      <a href="<?=$link;?>" class="btn btn-yellow"><?=$namelink;?></a>
    </div>
    <a href="#products" class="btn btn-xs"><span class="grey-arrow"></span>К товарам</a>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-10">
    <div class="video-banner">
      <div class="floor"><img src="img/yellow-truba.svg" alt=""><img src="img/orange-truba.svg" alt=""></div>
      <div class="owl-carousel owl-banner" id="slideshow<?php echo $module; ?>">
        <?php foreach ($banners as $banner) { ?>
        <div class="overflow-hidden">
          <?php if ($banner['link']) { ?>
          <a href="<?php echo $banner['link']; ?>">
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="video">
          </a>
          <? } else { ?>
          <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="video">
          <? } ?>
        </div>
        <? } ?>
      </div>
    </div>
    <!--<div class="socials">
      <a href="" target="_blank" class="icon icon-inst"></a>
      <a href="" target="_blank" class="icon icon-twit"></a>
      <a href="" target="_blank" class="icon icon-face"></a>
    </div>-->
  </div>
</div>

<script>
    $('#slideshow<?php echo $module; ?>').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 1,
        touchDrag: true,
        mouseDrag: true,
        autoplay: true,
        autoplaySpeed: 750,
    });
</script>