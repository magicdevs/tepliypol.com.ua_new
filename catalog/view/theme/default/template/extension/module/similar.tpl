<? if($products) { ?>
<form action="" class="form model-form">
        <div class="form--title">
            <h2>Выбор площади</h2><div class="close-form"><span></span><span></span></div>
        </div>
        <div class="form--description">
            <p>С помощью данной таблицы вы можете выбрать различные варианты товара, в зависимости от типа укладки и мощности.</p>
        </div>
        <div class="form--table">
            <div class="table">
                <div class="table--header">
                    <div class="t-row_model">
                        <? if($oname) { ?>
                            <? foreach($oname as $op) { ?>
                                <div>
                                    <? if($op['odesc']) { ?>
                                    <div class="header-info"><p><?=$op['name']; ?></p><i class="icon-info-y"></i>
                                        <div class="form-info"><p><?=$op['odesc']; ?></p></div>
                                    </div>
                                    <? } else { ?>
                                    <p><?=$op['name']; ?></p>
                                    <? } ?>
                                    <? if(count($op['product_option_value']) > 1) { ?>
                                    <div class="same-col-width">
                                        <? foreach ($op['product_option_value'] as $optionval) { ?>
                                        <div><p><?=$optionval; ?></p></div>
                                        <? } ?>
                                    </div>
                                    <? } ?>
                                </div>
                            <? } ?>
                        <? } ?>

                        <div><p><?= $text_price ?></p></div>
                        <div></div>
                    </div>
                </div>
                <? if($oname) { ?>
                <div class="table--measure">
                    <div class="t-row_model">

                        <? foreach($oname as $op) { ?>
                        <? if(count($op['product_option_value']) > 1) { ?>
                        <div class="t-row">
                            <div class="same-col-width">
                                <? foreach ($op['product_option_value'] as $optionval) { ?>
                                <div><p><?=$op['units']; ?></p></div>
                                <? } ?>
                            </div>
                        </div>
                        <? } else { ?>
                        <div><p><?=$op['units']; ?></p></div>
                        <? } ?>
                        <? } ?>

                        <div><p>грн</p></div>
                        <div></div>
                    </div>
                </div>
                <? } ?>
                <div class="table--content">
                    <? foreach($products as $product) { ?>
                    <div class="t-row_model">

                        <? foreach($oname as $op) { ?>
                        <? if (in_array($op['name'],$product['optionsAll'])) { ?>
                        <? foreach($product['options'] as $option) { ?>
                        <? if(count($option['product_option_value']) > 1) { ?>
                                <? if((count($op['product_option_value']) > 1) && ($op['name'] == $option['name'])) { ?>
                        <a href="<?=$product['href'];?>"><div class="t-row">
                                    <div class="same-col-width">
                                        <? foreach ($op['product_option_value'] as $optionval) { ?>
                                        <? if(in_array($optionval,$product['optionsname'])) { ?>
                                        <? foreach($option['product_option_value'] as $optval) { ?>
                                                <? if($optval['name'] == $optionval) { ?>
                                                <div class="data-label" data-label="<?=($op['name']. $optionval);?>:" data-measure="<?=strip_tags($op['units']); ?>"><p><?=$optval['valuer']; ?></p></div>
                                                <? } ?>
                                        <? } ?>
                                        <? } else { ?>
                                        <div class="data-label" data-label="<?=($op['name']. $optionval);?>:" data-measure="<?=strip_tags($op['units']); ?>"><p>-</p></div>
                                        <? } ?>
                                        <? } ?>
                                    </div>
                        </div></a>
                                <? } ?>
                        <? } else { ?>
                            <? if(in_array($op['name'],$product['optionsmainname'])) { ?>
                                <? if($op['name'] == $option['name']) { ?>
                        <a href="<?=$product['href'];?>"><div class="data-label" data-label="<?=$op['name'];?>:" data-measure="<?=strip_tags($op['units']); ?>"><p><?=$option['product_option_value'][0]['valuer']; ?></p></div></a>
                                <? } ?>
                            <? } else { ?>
                        <a href="<?=$product['href'];?>"><div class="data-label" data-label="<?=$op['name'];?>:" data-measure="<?=strip_tags($op['units']); ?>"><p>-</p></div></a>
                            <? } ?>
                        <? }  ?>

                        <? } ?>
                        <? } else { ?>
                        <a href="<?=$product['href'];?>"><div class="data-label" data-label="<?=$op['name'];?>:" data-measure="<?=strip_tags($op['units']); ?>"><p>-</p></div></a>
                        <? } ?>
                        <? } ?>
                        <a href="<?=$product['href'];?>"><div class="data-label" data-label="Цена:" data-measure="грн"><big><?=$product['price']; ?></big></div></a>
                        <button type="button" class="add_to_cart-btn" onclick="$(this).closest('form').find('.close-form').click();cart.add('<?=$product['product_id']; ?>');">В корзину</button>
                    </div>
                    <? } ?>
                </div>
            </div>
        </div>
    </form>
<? } ?>