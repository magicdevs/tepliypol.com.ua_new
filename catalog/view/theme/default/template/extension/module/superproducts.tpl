<div class="h3-style"><?php echo $heading_title; ?></div>
<div class='category-tab'>
    <?php $i =1; ?>
    <?php foreach ($products as $product) { ?>
        <div class="product-card">
            <script>
                dataLayer.push({
                    'ecommerce': {
                        'detail': {
                            'actionField': {'list': 'Главная'},
                            'products': [{
                                'name': '<?php echo $product['name']; ?>',
                                'id': '<?php echo $product['product_id']; ?>',
                                'price': '<? if($product['is_siblings'] && $product['upc_anal']) { ?><?=html_entity_decode($product['upc_anal']);?><? } else { ?><?=$product['price_anal'];?><? } ?>',
                                'brand': '<?php echo $product['manufacturer']; ?>',
                                'category': '<?php echo $heading_title; ?>',
                                'position': '<?php echo  $i; ?>',
                                'variant': '<?php echo $product['location']; ?>'
                            }]
                        }
                    }
                });
            </script>
            <?php $i++; ?>
            <div class="product-card--side card-front">
                <div class="product-card--img" onclick='location.href="<?php echo $product['href']; ?>"'>
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                </div>
                <div class="product-card--text">
                    <a href="<?php echo $product['href']; ?>?fromhome=true"><p><?php echo $product['name']; ?></p></a>
                    <a href="<?php echo $product['href']; ?>?fromhome=true"><span class="i-style"><?=$product['location'];?></span></a>
                    <span><? if($product['is_siblings'] && $product['upc']) { ?>от <?=html_entity_decode($product['upc']);?> <?=($product['mpn'])?html_entity_decode($product['mpn']):'грн/м<sup>2</sup>';?><? } else { ?><?=$product['price'];?><? } ?></span>
                </div>
            </div>
            <div class="product-card--side card-back">
                <div class="product-card--description">
                    <div class="p-c--d_header">
                        <a href="<?php echo $product['href']; ?>?fromhome=true"><p><?php echo $product['name']; ?></p></a>
                        <a href="<?php echo $product['href']; ?>?fromhome=true"><span class="i-style"><?=$product['location'];?></span></a>
                        <span><? if($product['is_siblings'] && $product['upc']) { ?>от <?=html_entity_decode($product['upc']);?> <?=($product['mpn'])?html_entity_decode($product['mpn']):'грн/м<sup>2</sup>';?><? } else { ?><?=$product['price'];?><? } ?></span>
                    </div>
                    <div class="p-c--d_description">
                        <a href="<?php echo $product['href']; ?>?fromhome=true"><p><?php echo $product['description']; ?></p></a>
                        <a href="<?php echo $product['href']; ?>?fromhome=true" class="btn btn-light_y">Подробнее</a>
                    </div>
                    <div class="p-c--d_select">
                        <? if($product['is_siblings']) { ?>
                            <div class="btn btn-model-select" onclick="similar('<?php echo $product['product_id']; ?>')"><?=$select_sqrt;?> <div class="arrow"><span></span><span></span></div></div>
                        <? } ?>
                    </div>
                    <div class="p-c--d_btns">
                        <div class="btn btn-comparison" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                            <div class="yellow-arrow transform-180"></div>
                            <div class="yellow-arrow"></div>
                        </div>
                        <button class="btn btn-yellow to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>');">В корзину</button>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
</div>

