    <form action="" class="form model-form">
        <div class="form--title">
            <h2>Выбор модели</h2><div class="close-form"><span></span><span></span></div>
        </div>
        <div class="form--description">
            <p>С помощью данной таблицы вы можете выбрать различные варианты товара, в зависимости от типа укладки и мощности.</p>
        </div>
        <div class="form--table">
            <div class="table">
                <div class="table--header">
                    <div class="t-row_model">
                        <? foreach($products as $prod) { ?>
                        <? if($prod['options']){ ?>
                        <? foreach($prod['options'] as $opt){ ?>
                        <div>
                            <? if($opt['odesc']) { ?>
                                <div class="header-info"><p><?=$opt['name']; ?></p><i class="icon-info-y"></i>
                                    <div class="form-info"><p><?=$opt['odesc']; ?></p></div>
                                </div>
                            <? } else { ?>
                                <p><?=$opt['name']; ?></p>
                            <? } ?>
                            <? if(count($opt['product_option_value']) > 1) { ?>
                            <div class="same-col-width">
                                <? foreach ($opt['product_option_value'] as $optionval) { ?>
                                <div><p><?=$optionval['name']; ?></p></div>
                                <? } ?>
                            </div>
                            <? } ?>
                        </div>
                        <? } ?>
                        <? } ?>
                        <? } ?>
                        <div><p>Цена</p></div>
                        <div></div>
                    </div>
                </div>
                <div class="table--measure">
                    <div class="t-row_model">
                        <div><p>Вт</p></div>
                        <div><p>м</p></div>
                        <div class="t-row">
                            <div class="same-col-width">
                                <div><p>м<sup>2</sup></p></div>
                                <div><p>м<sup>2</sup></p></div>
                                <div><p>м<sup>2</sup></p></div>
                            </div>
                        </div>
                        <div><p>грн</p></div>
                        <div></div>
                    </div>
                </div>
                <div class="table--content">
                    <div class="t-row_model">
                        <div class="data-label" data-label="Мощность:" data-measure="Вт"><p>170</p></div>
                        <div class="data-label" data-label="Длинна:" data-measure="м"><p>8</p></div>
                        <div class="t-row">
                            <div class="same-col-width">
                                <div class="data-label" data-label="Площадь, при шаге (16 см):" data-measure="м2"><p>1,3</p></div>
                                <div class="data-label" data-label="Площадь, при шаге (14 см):" data-measure="м2"><p>1,1</p></div>
                                <div class="data-label" data-label="Площадь, при шаге (12 см):" data-measure="м2"><p>1,0</p></div>
                            </div>
                        </div>
                        <div class="data-label" data-label="Цена:" data-measure="грн"><big>1 250</big></div>
                        <button class="add_to_cart-btn">В корзину</button>
                    </div>
                    <div class="t-row_model">
                        <div class="data-label" data-label="Мощность:" data-measure="Вт"><p>170</p></div>
                        <div class="data-label" data-label="Длинна:" data-measure="м"><p>8</p></div>
                        <div class="t-row">
                            <div class="same-col-width">
                                <div class="data-label" data-label="Площадь, при шаге (16 см):" data-measure="м2"><p>1,3</p></div>
                                <div class="data-label" data-label="Площадь, при шаге (14 см):" data-measure="м2"><p>1,1</p></div>
                                <div class="data-label" data-label="Площадь, при шаге (12 см):" data-measure="м2"><p>1,0</p></div>
                            </div>
                        </div>
                        <div class="data-label" data-label="Цена:" data-measure="грн"><big>1 250</big></div>
                        <button class="add_to_cart-btn">В корзину</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
