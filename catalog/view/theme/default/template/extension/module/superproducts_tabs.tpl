<section class="category">
    <div class="row">
        <div class="col-xs-12">
            <div class="category--header">
                <div class="h2-style"><?php echo $heading_title; ?></div>
            </div>
            <div class="category--info"></div>
            <div class="category--categories same-col-width" id="floor-tab" role="tablist">

                <?php $n =0; foreach($tabs as $tab) { ?>
                <div class="btn category-btn <? if(!$tab['desc']) { ?>no-icon<? } ?> <?php if (!$n) { ?>active<?php } ?>"><?php echo $tab['head']; ?><? if($tab['desc']) { ?><i class="icon icon-info"></i>
                <div class="text--info">
                    <p><?=$tab['desc']; ?></p>
                </div>
                    <? } ?>
                </div>
                <?php $n++; } ?>
            </div>
        </div>
    </div>
    <div class="owl-carousel owl-categ owl-cat-<?=$module; ?>"></div>
    <div class="category-tabs">
        <?php foreach($tabs as $tab) { ?>
            <?php echo $tab['body']; ?>
        <?php } ?>
    </div>
</section>
