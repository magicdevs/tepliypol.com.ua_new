<h3><?php echo $heading_title; ?></h3>
  <div class='category-tab'>
    <?php foreach ($products as $product) { ?>
    <div class="product-card">
      <div class="product-card--side card-front">
        <div class="product-card--img">
          <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
        </div>
        <div class="product-card--text">
          <a href="<?php echo $product['href']; ?>"><p><?php echo $product['name']; ?></p></a>
          <a href="<?php echo $product['href']; ?>"><i><?=$product['location'];?></i></a>
          <?php if ($product['price']) { ?>
          <span><? if($product['is_siblings']) { ?>от <? } ?><?=$product['price'];?><?=html_entity_decode($product['upc']);?></span>
          <? } ?>
        </div>
      </div>
      <div class="product-card--side card-back">
        <div class="product-card--description">
          <div class="p-c--d_header">
            <a href="<?php echo $product['href']; ?>"><p><?php echo $product['name']; ?></p></a>
            <a href="<?php echo $product['href']; ?>"><i><?=$product['location'];?></i></a>
            <?php if ($product['price']) { ?>
            <span><? if($product['is_siblings']) { ?>от <? } ?><?=$product['price'];?><?=html_entity_decode($product['upc']);?></span>
            <? } ?>
          </div>
          <div class="p-c--d_description">
            <a href="<?php echo $product['href']; ?>"><p><?php echo $product['description']; ?></p></a>
            <a href="<?php echo $product['href']; ?>" class="btn btn-light_y">Подробнее</a>
          </div>
          <div class="p-c--d_select">
            <? if($product['is_siblings']) { ?>
            <div class="btn btn-model-select" onclick="similar('<?php echo $product['product_id']; ?>')">Выбор площади <div class="arrow"><span></span><span></span></div></div>
            <? } ?>
          </div>
          <div class="p-c--d_btns">
            <div class="btn btn-comparison" onclick="compare.add('<?php echo $product['product_id']; ?>');">
              <div class="yellow-arrow transform-180"></div>
              <div class="yellow-arrow"></div>
            </div>
            <button class="btn btn-yellow to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>');">В корзину</button>
          </div>
        </div>
      </div>
    </div>
    <? } ?>
  </div>
