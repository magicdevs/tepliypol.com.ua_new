<style>
  #payment form button[type="submit"]{
    width: auto;
  }
  #payment form {
    min-height: 160px;
  }
</style>
<form action="<?php echo $action; ?>" method="post" class="form model-form">
  <div class="form--title">
    <h2>Оплатить через LiqPay?</h2><div class="close-form"><span></span><span></span></div>
  </div>
  <div class="form--description">
  <input type="hidden" name="operation_xml" value="<?php echo $xml; ?>">
  <input type="hidden" name="signature" value="<?php echo $signature; ?>">
  <div class="buttons">
    <div class="pull-right">
      <button type="submit" class="btn btn-primary"><?php echo $button_confirm; ?></button>
    </div>
  </div>
  </div>

</form>
