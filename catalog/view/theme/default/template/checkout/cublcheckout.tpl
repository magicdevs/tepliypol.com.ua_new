<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container">
    <?php echo $content_top; ?>
    <div class="row">
        <div class="col-xs-12 page--header">
            <div class="floor-header"><img src="img/page-title.png" alt=""></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumbs">
                <?php foreach ($breadcrumbs as $key=>$breadcrumb) {
                    if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
            ?>
                <? if($key == count($breadcrumbs)){ ?>
                <li><?php echo $breadcrumb['text']; ?></li>
                <? } else { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <? } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="row page--content">
        <div class="col-xs-12">
            <form class="form order-form">
                <div class="order-form--content">
                    <div>
                        <?php if ($shipping_methods) { ?>
                        <h3><?=$text_delivery; ?></h3>
                        <select name="shipping_method" title="<?=$text_deliveries; ?>" onchange="shipping_method_save(this.value)" required class="selectpicker">
                            <? if(!$code_shipping_method){ ?>
                            <option value="" selected="selected"><?=$text_select; ?></option>
                            <? } ?>
                            <?php foreach ($shipping_methods as $shipping_method) { ?>
                            <?php foreach ($shipping_method['quote'] as $quote) { ?>
                                    <?php if ($quote['code'] == $code_shipping_method) { ?>
                                    <?php $code = $quote['code']; ?>
                            <option value="<?php echo $quote['code']; ?>" selected="selected" ><?php echo $quote['title']; ?> <?=($quote['text'])? ' ('.$quote['text'].')':''; ?></option>
                                    <?php } else { ?>
                            <option value="<?php echo $quote['code']; ?>" ><?php echo $quote['title']; ?>  <?=($quote['text'])? ' ('.$quote['text'].')':''; ?></option>
                                    <?php } ?>
                            <?php } ?>
                            <?php } ?>
                        </select>
                        <? } ?>
                        <div class="input">
                            <input class="inp-text saves <?=($fio)?'has-content':''; ?>" type="text" name="fio" id="saves-fio" value="<?=$fio; ?>" placeholder="">
                            <label><?=$text_fio; ?></label>
                            <span class="focus-border">
						            	<i></i>
						            </span>
                        </div>
                        <div class="input">
                            <input class="inp-text saves <?=($city)?'has-content':''; ?>" type="text" name="city" id="saves-city" value="<?=$city; ?>" placeholder="">
                            <label><?=$text_city; ?></label>
                            <span class="focus-border">
						            	<i></i>
						            </span>
                        </div>
                        <div class="input">
                            <input class="inp-text saves <?=($address)?'has-content':''; ?>" type="text" name="address" id="saves-address" value="<?=$address; ?>" placeholder="">
                            <label><?=$text_address; ?></label>
                            <span class="focus-border">
						            	<i></i>
						            </span>
                        </div>
                        <div class="input">
                            <input class="inp-text saves <?=($tel)?'has-content':''; ?>" type="tel" name="tel" id="saves-tel" value="<?=$tel; ?>" placeholder="">
                            <label><?=$text_tel;?></label>
                            <span class="focus-border">
						            	<i></i>
						            </span>
                        </div>
                    </div>
                    <div>
                        <? if($payment_methods) { ?>
                        <h3><?=$text_payment; ?></h3>
                        <select name="payment_method" title="<?=$text_payments; ?>" onchange="payment_method_save(this.value)" class="selectpicker">
                            <? if(!$code_payment_methods){ ?>
                            <option value="" selected="selected"><?=$text_select; ?></option>
                            <? } ?>
                            <?php foreach ($payment_methods as $payment_method) { ?>
                                    <?php if ($payment_method['code'] == $code_payment_methods) { ?>
                                    <option value="<?php echo $payment_method['code']; ?>" selected="selected" ><?php echo $payment_method['title']; ?>
                                        <?php if ($payment_method['terms']) { ?>
                                        (<?php echo $payment_method['terms']; ?>)
                                        <?php } ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $payment_method['code']; ?>" ><?php echo $payment_method['title']; ?>
                                        <?php if ($payment_method['terms']) { ?>
                                        (<?php echo $payment_method['terms']; ?>)
                                        <?php } ?></option>
                                    <?php } ?>
                            <?php } ?>
                        </select>
                        <? } ?>
                        <div class="input input-text">
                            <textarea class="inp-text saves <?=($comment)?'has-content':''; ?>" id="saves-comment" name="comment" value="<?=$comment; ?>" placeholder=""><?=$comment; ?></textarea>
                            <label><?=$text_comments; ?></label>
                            <span class="focus-border">
						            	<i></i>
						            </span>
                        </div>
                    </div>
                    <div>
                        <h3><?=$text_cart; ?></h3>
                        <div class="cart-block--content" id="checkout-cart">
                            <div class="cart-block--scroll">
                                <? if($products) { ?>
                                <? foreach($products as $product) { ?>
                                <script>

                                        dataLayer.push({
                                            'event': 'checkout',
                                            'ecommerce': {
                                                'checkout': {
                                                    'actionField': {'step': 1, 'option': 'Visa'},
                                                    'products': [{
                                                        'name': '<?=$product['name']; ?>',
                                                        'id': '<?=$product['product_id']; ?>',
                                                        'price': '<?php echo $product['price_anal']; ?>',
                                                        'brand': '<?php echo $product['manufacturer']; ?>',
                                                        'category': 'Каталог',
                                                        'variant': '<?php echo $product['location']; ?>',
                                                        'quantity': <?=$product['quantity']; ?>,
                                    }]
                                    }
                                    },

                                    });
                                </script>
                                <div class="cart-block--row">
                                    <div class="cart-block--img"><img src="<?=$product['thumb']; ?>" alt="<?=$product['name']; ?>"></div>
                                    <div class="cart-block_text flex-col-wrap">
                                        <p><?=$product['name']; ?></p>
                                        <div class="flex-row-wrap">
                                            <div class="num">
                                                <div class="num-btn minus" onclick="updateQuantity('<?php echo $product['cart_id']; ?>','<?php echo $product['quantity']-1; ?>');"></div>
                                                <p><?=$product['quantity']; ?></p>
                                                <div class="num-btn plus" onclick="updateQuantity('<?php echo $product['cart_id']; ?>','<?php echo $product['quantity']+1; ?>');"></div>
                                            </div>
                                            <big><?=$product['total']; ?></big>
                                        </div>
                                    </div>
                                </div>
                                <? } ?>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="order-form--footer">
                    <div><a href="<?=$cart; ?>" class="btn btn-grey"><?=$text_back_cart; ?></a></div>
                    <div><big><?php echo $totals[count($totals)-1]['title']; ?>: <?php echo $totals[count($totals)-1]['text']; ?></big></div>
                    <div><input type='submit' class="btn btn-yellow" id="submit-confirm" value="<?=$btn_send; ?>"></div>
                </div>

            </form>
        </div>

    </div>
</div>

<script>
    /*$(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });*/

    function payment_method_save(code) {
        $.ajax({
            url: 'index.php?route=checkout/cublcheckout/payment_method_save',
            type: 'post',
            data: 'payment_method='+code,
            dataType: 'json',
            beforeSend: function () {
                $('#submit-confirm').button('loading');
            },
            success: function (json) {
                console.log(json);
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $("select[name=payment_method]").before('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
                    $('#submit-confirm').button('reset');

                    if(json['instruction']){
                        $(".cart-block--content").after('<h3>' + json['instruction'] + '</h3>');
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function shipping_method_save(code) {
        $.ajax({
            url: 'index.php?route=checkout/cublcheckout/shipping_method_save',
            type: 'post',
            data: 'shipping_method='+code,
            dataType: 'json',
            beforeSend: function() {
                $('#submit-confirm').button('loading');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {

                    if (json['error']['warning']) {
                        $("select[name=shipping_method]").before('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
                    $('#submit-confirm').button('reset');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    $(".saves").on("change",function () {
        let name = $(this).attr('name');
        let val = $(this).val();
        $.ajax({
            url: 'index.php?route=checkout/cublcheckout/saves',
            type: 'post',
            data: 'name='+name+'&value='+val,
            dataType: 'json',
            beforeSend: function() {
                $('#submit-confirm').button('loading');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {

                    if (json['error']['warning']) {
                        $("#saves-"+name).parent().after('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
                    $('#submit-confirm').button('reset');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $("body.checkout-cublcheckout").on("click", "#submit-confirm", function () {
        $.ajax({
            url: 'index.php?route=checkout/cublcheckout/confirm',
            type: 'post',
           // data: 'name='+name+'&value='+val,
            data: '',
            dataType: 'json',
            beforeSend: function() {
                $('#submit-confirm').button('loading');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    if($('select[name=payment_method]').val() == 'liqpay') {
                        $( "#payment .flex-center .insert" ).load('/index.php?route=extension/payment/liqpay');
                        openCommon_form('#payment');
                    } else {
                        location = json['redirect'];
                    }

                } else if (json['error']) {
                    if (json['error']['logs']) {
                        console.log(json['error']['logs']);
                    }
                    if (json['error']['warning']) {

                        // console.log(json['error']['warning']);
                        for (key in json['error']['warning']) {
                            if(key == 'payment_method' || key == 'shipping_method'){
                                $("select[name="+key+"]").before('<div class="alert alert-danger">' + json['error']['warning'][key] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            } else {
                                $("input[name="+key+"]").parent().after('<div class="alert alert-danger">' + json['error']['warning'][key] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            }
                        }
                    }
                }
                $('#submit-confirm').button('reset');

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>
<?php echo $footer; ?>
