<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container">
  <?php echo $content_top; ?>
  <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumbs">
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) {
          if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
         ?>
        <? if($key == count($breadcrumbs)){ ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <? } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <? } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="page--content row">
    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <? if(!empty($products)) { ?>
    <div class="col-xs-12 col-md-9">
      <div class="cart-table">
        <div class="cart-table_row header">
          <div><i>Фото</i></div>
          <div class="text-left"><i>Наименование</i></div>
          <div><i>Цена</i></div>
          <div><i>Количество</i></div>
          <div><i>Сумма</i></div>
        </div>
        <?php foreach ($products as $product) { ?>
        <div class="cart-table_row">
          <? if($product['thumb']) { ?>
          <div data-label="Фото"><a href="<?=$product['href']; ?>"><div class="cart-block--img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></div></div></a>
          <? } ?>
          <div data-label="Наименование"><a href="<?=$product['href']; ?>"><p><?php echo $product['name']; ?></p></a>
            <?php if (!$product['stock']) { ?>
            <p><span class="text-danger">***</span></p>
            <?php } ?>
          </div>
          <div data-label="Цена"><a href="<?=$product['href']; ?>"><big><?php echo $product['price']; ?></big></a></div>
          <div data-label="Количество">
            <div class="num">
              <div class="num-btn minus" onclick="updateQuantity('<?php echo $product['cart_id']; ?>','<?php echo $product['quantity']-1; ?>');"></div>
              <p><?php echo $product['quantity']; ?></p>
              <div class="num-btn plus" onclick="updateQuantity('<?php echo $product['cart_id']; ?>','<?php echo $product['quantity']+1; ?>');"></div>
            </div>
          </div>
          <div data-label="Сумма"><a href="<?=$product['href']; ?>"><big><?php echo $product['total']; ?></big></a></div>
          <div style="cursor: pointer;"><div onclick="updateQuantity('<?php echo $product['cart_id']; ?>','0');" class="remove-product"><span></span><span></span></div></div>
        </div>
        <? } ?>
      </div>
    </div>
    <div class="col-xs-12 col-md-3">
      <div class="cart-info">
        <i>В корзине <?=$totalP; ?> товар</i>
        <div class="cart-info-box">
          <big><?php echo $totals[count($totals)-1]['title']; ?>: <?php echo $totals[count($totals)-1]['text']; ?></big>
          <a href="<?=$checkout; ?>" class="btn btn-yellow">Оформление заказа</a>
        </div>
        <a href="<?php echo $continue; ?>" class="btn btn-light">Продолжить покупки</a>
      </div>
    </div>
    <? } else { ?>
    <h2>Корзина пуста!</h2>
       <? } ?>
  </div>
  <?php echo $content_bottom; ?>
</div>
<?php echo $column_right; ?>


<?php echo $footer; ?>
