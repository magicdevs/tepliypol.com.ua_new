<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container">
  <?php echo $content_top; ?>
  <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumbs">
        <?php 
$ldJson = array(
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => array()
);
        ?>
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) {
        if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
$ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          ); ?>
        <? if($key == count($breadcrumbs)){ ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <? } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <? } ?>
        <?php } ?>
      </ul>
       <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
    </div>
  </div>
  <div class="page-content row">
    <div class="col-xs-12">
      <?php if ($products) { ?>
      <div class="form filter">
        <div>
          <label for="input-sort">Сортировать по:</label>
          <select id="input-sort" class="selectpicker" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>

      </div>
      <div class="result-table">
        <div class="res-row">
          <div><span class="i-style">Наименование</span></div>
          <div><span class="i-style">Площадь, м<sup>2</sup>:</span></div>
          <div><span class="i-style">Мощность, Вт:</span></div>
          <div><span class="i-style">Цена, <?=$symbol_right; ?>:</span></div>
          <div></div>
        </div>
        <? foreach($products as $product) { ?>
        <script>
          dataLayer.push({
            'ecommerce': {
              'detail': {
                'actionField': {'list': 'Поиск'},
                'products': [{
                  'name': '<?php echo $product['name']; ?>',
                  'id': '<?php echo $product['product_id']; ?>',
                  'price': '<?php echo $product['price_anal']; ?>',
                  'brand': '<?php echo $product['manufacturer']; ?>',
                  'category': '<?php echo $heading_title; ?>',
                  'position': '<?php echo  ++$offset; ?>',
                  'variant': '<?php echo $product['location']; ?>'
                }]
              }
            }
          });
        </script>
        <a href="<?=$product['href'];?>" class="res-row">
          <span data-label="Наименование"><?=$product['name'];?></span>
          <span data-label="Площадь, м2:">
            <? if($product['options1']) { ?>
            <? if(count($product['options1']) > 1) { ?>
            <?=(array_shift($product['options1']) . ' - ' .array_pop($product['options1'])); ?>
            <? } else { ?>
            <?=array_shift($product['options1']); ?>
            <? } ?>
            <? } else { ?>
            -
            <? } ?>
          </span>
          <span data-label="Мощность, Вт:">
            <? if($product['options13']) { ?>
            <? if(count($product['options13']) > 1) { ?>
            <?=(array_shift($product['options13']) . ' - ' .array_pop($product['options13'])); ?>
            <? } else { ?>
            <?=array_shift($product['options13']); ?>
            <? } ?>
            <? } else { ?>
            -
            <? } ?>
          </span>
          <span data-label="Цена" class="bold"><?=(!$product['special'])?$product['price']:$product['special'];?><?=html_entity_decode($product['upc']);?></span>
          <span class="btn btn-light_y to-cart" onclick="cart.add('<?=$product['product_id'];?>');">Купить</span>
        </a>
        <? } ?>
      </div>
      <div class="result-footer">
        <?php echo $pagination; ?>
        <p><small><?php echo $results; ?></small></p>
      </div>
      <? } else { ?>
      <p><?php echo $text_empty; ?></p>
      <? } ?>
    </div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<?php echo $column_right; ?>
<?php echo $footer; ?>