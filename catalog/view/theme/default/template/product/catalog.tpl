<?php echo $header; ?>
    <div class="container">
        <ul class="breadcrumb">
            <?php 
$ldJson = array(
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => array()
);
        ?>
            <?php foreach ($breadcrumbs as $breadcrumb) {
            if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
$ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          ); ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
         <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="category-block">
                            <?php printCategories($categories_with_products); ?>
                        </div>
                    </div>
                </div>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>


    <script>
        $(document).ready(function () {
            $('.category-block .products button .arrow:not(.arrow-more)').on('click', function (e) {
                e.preventDefault();
                console.log(`index.php?route=extension/module/similar&product_id="${$(this).closest('.product-table-button').data('product_id')}"`);
                if (!$(this).closest('.product-table-button').hasClass('loaded')) {
                    $(this)
                    		.closest('.product-table-button')
                    		.addClass('loaded')
                    		.next()
                    		.load(`index.php?route=extension/module/similar&product_id=${$(this).closest('.product-table-button').data('product_id')}`)
                    		.show();
                } else {
                    $(this).closest('.product-table-button').removeClass('loaded').next().html("").hide();
                }
                // $(this).next().load( "index.php?route=extension/module/similar&product_id=" + $(this).data('product_id'));
            });

            $('.category-block .category-item button').click(function () {
            	console.log(true)
                if ($(this).hasClass('open')) {
                    $(this).removeClass('open').parent().next().slideDown();
                } else {
                    $(this).addClass('open').parent().next().slideUp();
                }
            });

            $('.category-block .products button .arrow-more').click(function() {
                $(this).closest('.product-table-button').next().next().slideToggle();
            });
        });
    </script>
<?php echo $footer; ?>


<?php function printCategories($categories)
{ ?>
    <?php foreach ($categories as $category) { ?>
    <?php if ($category['products']) { ?>
        <div>
            <div class="category-item">
                <button type="button">
                		<div>	
                			 <span><?php echo $category['name']; ?></span>
		                    <!-- <img src="/img/accent-arrow.svg" alt=""> -->
		                    <div class="arrow">
		                        <img src="img/angle-arrow-down.svg" alt="">
		                    </div>
                		</div>
                </button>
<!--                 <a href="<?php echo $category['href']; ?>">
                    <?php echo $category['text_more']; ?>
                </a> -->
            </div>

            <div class="products">
                <?php foreach ($category['products'] as $product) { ?>
                    <button type="button" class="product-table-button" data-product_id="<?php echo $product['product_id']; ?>">
                    		<div>
	                    		<div>	
															<span><?php echo $product['name']; ?></span>
	                    		</div>
	                    		<div>
	                        <div class="arrow">
	                            <img src="img/angle-arrow-down.svg" alt="">
	                        </div>
	                        <div class="arrow arrow-more">
	                            <img src="img/i.svg" alt="">
	                        </div>
	                    		</div>
                    	</div>
                    </button>
                    <div class="product-table">
                    </div>
                    <div class="product-description">
                        <h2><?php echo $category['text_specifications']; ?></h2>
                        <div class="table-responsive">
                            <table>
                                <tbody>
                                    <?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                            <tr>
                                                <td><?php echo $attribute['name']; ?>:</td>
                                                <td><?php echo $attribute['text']; ?></td>
                                            </tr>
                                        <? } ?>
                                    <? } ?>
                                </tbody>
                            </table>
                        </div>
                        <h2><?php echo $category['text_product_description']; ?></h2>
                        <div class="description">
                            <?php echo $product['description'];?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php if ($category['children']) { ?>
        <?php printCategories($category['children']); ?>
    <?php } ?>
<?php } ?>
<?php } ?>