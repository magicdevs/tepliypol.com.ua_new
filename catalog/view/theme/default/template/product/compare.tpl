<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row">
    <div class="col-sm-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumbs">
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
        <? if($key == count($breadcrumbs)){ ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <? } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <? } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <?php if ($products) { ?>
      <div class="page--content page--table comparison-table">
        <div class="same-col-width">
          <div><p>Товар</p></div>
          <?php foreach ($products as $product) { ?>
          <div><a href="<?php echo $product['href']; ?>"><big><?php echo $product['name']; ?></big></a></div>
          <?php } ?>
        </div>
        <div class="same-col-width">
          <div><p>Изображение</p></div>
          <?php foreach ($products as $product) { ?>
          <?php if ($product['thumb']) { ?>
          <div><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a></div>
            <?php } ?>
          <?php } ?>
        </div>
        <div class="same-col-width">
          <div><p>Наличие</p></div>
          <?php foreach ($products as $product) { ?>
          <div><p><?php echo $product['availability']; ?></p></div>
          <?php } ?>
        </div>
        <? if($attribute_groups) { ?>
        <?php foreach ($attribute_groups as $attribute_group) { ?>
        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
        <div class="same-col-width">
          <div><p><?php echo $attribute['name']; ?></p></div>
          <?php foreach ($products as $product) { ?>
          <?php if (isset($product['attribute'][$key])) { ?>
          <div><p><?php echo $product['attribute'][$key]; ?></p></div>
          <?php } else { ?>
          <div></div>
          <?php } ?>
          <?php } ?>
        </div>
        <? } ?>
        <? } ?>
        <? } ?>

        <div class="same-col-width">
          <div></div>
          <?php foreach ($products as $product) { ?>
          <div><button class="btn btn-yellow to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" >В корзину</button>
            <button class="btn btn-light" onclick="location.href = '<?php echo $product['remove']; ?>';">Убрать из сравнения</button></div>
          <?php } ?>
        </div>
      </div>
      <? } else { ?>
      <p><?php echo $text_empty; ?></p>
      <? } ?>
    </div>
  </div>
  <?php echo $content_bottom; ?>
</div>


    <?php echo $column_right; ?>
<?php echo $footer; ?>
