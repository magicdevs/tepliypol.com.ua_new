<?php echo $header; ?>
<div class="container">
  <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt="page-title"></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumbs">
         <?php 
$ldJson = array(
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => array()
);
        ?>

        <?php foreach ($breadcrumbs as $key=>$breadcrumb) { 
          if ($breadcrumb['text'] == 'Каталог') {
            continue;
            }
            if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
$ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          ); ?>

        <? if($key == count($breadcrumbs)){ ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <? } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <? } ?>
        <!--dg_crumb_name:<?php echo $breadcrumb['text']; ?>;;dg_crumb_href:<?php echo $breadcrumb['href']; ?>-->
        <?php } ?>
      </ul>
      <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
    </div>
  </div>
  <div class="row page--content">
    <?php echo $column_left; ?>
    <? if ($column_left) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div class="col-xs-12 <?=$class;?>"><?php echo $content_top; ?>
      <div class="form filter filter_card">
        <div>
          <p>Отображение товаров:</p>
          <button type="button" class="btn-display active" id="cards"></button>
          <button type="button" class="btn-display" id="list"></button>
          <button type="button" class="btn-display" id="grid"></button>

        </div>
        <div>
          <label for="input-sort">Сортировать по:</label>
          <select id="input-sort" class="selectpicker" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <? /*
        <div class="flex-row">
          <p>Построение списка:</p>
          <div class="btn btn-list">
            <div class="yellow-arrow transform-180"></div>
            <div class="yellow-arrow"></div>
          </div>
        </div>
      */ ?>
      </div>
      <? if ($column_left) { ?>
      <? } ?>
      <? if($products) { ?>
          <!--isset_listing_page-->
    <div class="product-list">

        <? foreach($products as $product) { ?>
            <!--product_in_listingEX-->
      <script>
        dataLayer.push({
          'ecommerce': {
            'detail': {
              'actionField': {'list': 'Каталог'},
              'products': [{
                'name': '<?php echo $product['name']; ?>',
                'id': '<?php echo $product['product_id']; ?>',
                'price': '<?php echo $product['price_anal']; ?>',
                'brand': '<?php echo $product['manufacturer']; ?>',
                'category': '<?php echo $heading_title; ?>',
                'position': '<?php echo  ++$offset; ?>',
                'variant': '<?php echo $product['location']; ?>'
              }]
            }
          }
        });
      </script>
        <div class="product-card">
          <div class="product-card--side card-front">
            <a href="<?=$product['href'];?>">
            <div class="product-card--img">
              <?php if (isset($dg_prod_name_part)) { ?>
              <img data-original="<?php echo $product['thumb']; ?>" alt="<?php echo $dg_prod_name_part.' '.$product['name']; ?>" title="<?php echo $dg_prod_name_part.' '.$product['name']; ?>" class="lazy-load"/>
            <?php } else { ?>
              <img data-original="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="lazy-load"/>
            <?php } ?>
            </div>
            <div class="product-card--text">
              <?php if (isset($dg_prod_name_part) && !empty($dg_prod_name_part)) { ?>
              <!--TESTT-->
                <p><?=$dg_prod_name_part;?></p>
              <?php } ?>
              <p><?=$product['name'];?></p>
              <span class="i-style"><?=$product['location'];?></span>
              <?php if ($product['price']) { ?>
              <span><?=$text_price.' '.$product['price'];?></span>
              <? } ?>
            </div>
            </a>
          </div>
          <div class="product-card--side card-back">
            <div class="product-card--description">
              <a href="<?=$product['href'];?>">
                <?php if (isset($dg_prod_name_part) && !empty($dg_prod_name_part)) { ?>
                  <p><?=$dg_prod_name_part;?></p>
                <?php } ?>
                <div class="p-c--d_header">
                  <p><?=$product['name'];?></p>
                  <span class="i-style"><?=$product['location'];?></span>
                  <?php if ($product['price']) { ?>
                  <span><?=$product['price'];?></span>
                  <? } ?>
                </div>
              </a>
              <div class="revers">
                <div class="p-c--d_description">
                  <a href="<?=$product['href'];?>">
                    <div class="p-c--d_overflow">
                    <p><?=$product['description'];?></p>
                  </div>
                  </a>
                  <a href="<?=$product['href'];?>" class="btn btn-light_y">Подробнее</a>
                </div>
                <div>
                  <?php if($product['images']) { ?>
                  <div class="p-c--d_imgs">
                    <? foreach($product['images'] as $image) { ?>
                    <div><img src="<?=$image;?>" alt="<?=$product['name'];?>"></div>
                    <? } ?>
                  </div>
                  <? } ?>
                  <div class="p-c--d_select">
                    <? if($product['is_siblings']) { ?>
                    <div class="btn btn-model-select" onclick="similar('<?php echo $product['product_id']; ?>')"><?=$select_sqrt;?> <div class="arrow"><span></span><span></span></div></div>
                    <? } ?>
                  </div>
                  <div class="p-c--d_btns">
                    <div class="btn btn-comparison" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                      <div class="yellow-arrow transform-180"></div>
                      <div class="yellow-arrow"></div>
                    </div>
                    <button class="btn btn-yellow to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>');">Купить</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="product-card--grid sm-row">
            <a href="<?=$product['href'];?>"></a>
            <div data-label="Наименование:"><span><?=$product['name'];?></span></div>

            <div data-label="Площадь, м2:"><span>
              <? if($product['options1']) { ?>
              <? if(count($product['options1']) > 1) { ?>
                <?=(array_shift($product['options1']) . ' - ' .array_pop($product['options1'])); ?> <? /*$product['optionUnit1']; */ ?>
              <? } else { ?>
                <?=array_shift($product['options1']); ?> <? /*$product['optionUnit1']; */ ?>
              <? } ?>
              <? } else { ?>
              -
              <? } ?>
            </span></div>
            <div data-label="Мощность, Вт:"><span><? if($product['options13']) { ?>
              <? if(count($product['options13']) > 1) { ?>
              <?=(array_shift($product['options13']) . ' - ' .array_pop($product['options13'])); ?> <? /*$product['optionUnit13']; */ ?>
              <? } else { ?>
              <?=array_shift($product['options13']); ?> <? /*$product['optionUnit13']; */ ?>
              <? } ?>
              <? } else { ?>
              -
              <? } ?></span></div>
            <div  data-label="Модель:" class="p-c--d_select">
              <? if($product['is_siblings']) { ?>
              <div class="btn btn-model-select " onclick="similar('<?php echo $product['product_id']; ?>')">Модель <div class="arrow"><span></span><span></span></div></div>
              <? } ?>
            </div>
            <? if($product['price']) { ?>
            <div data-label="Цена, <?=$symbol_right;?>:"><span class="bold"> <?=$product['price'];?></span></div>
            <? } ?>
            <span data-label="" class="btn btn-light_y to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>');">Купить</span>
          </div>
        </div>
        <? } ?>
      </div>
      <? } else { ?>
      <p><?php echo $text_empty; ?></p>
      <? } ?>
    </div>
    </div>
  <div class="row">
    <div class="col-xl-12 col-md-9 col-xs-offset-0 col-md-offset-3 catalog-pag">
      <div class="flex-center">
        <?php echo $pagination; ?>
      </div>
    </div>
  </div>
  <!--text_generation_filters-->
<div>
<?php 
  if (strpos($_SERVER['REQUEST_URI'], 'page=')===false) {
    echo '<!--seo_text_start-->'.$description.'<!--seo_text_end-->';
  }
  ?>
</div>
  <?php if ($lang != 'uk') { ?>
<div itemscope="" itemtype="" id="spoc-accordion" class="container">
  
    <h2>Ответы на часто задаваемые вопросы:</h2>
    <div itemprop="mainEntity" itemscope="" itemtype="" class="content-wrapper">
      <div itemprop="name" class="toggle">
      🔻Почему стоит купить <!--getH1--> в интернет магазине tepliypol.com.ua?</div>
      <div itemprop="acceptedAnswer" itemscope="" itemtype="">
        <div itemprop="text" class="text">
          Причины, по которым стоит оформить заказ на <!--getH1--> у именно нас:
          <ul>
            <li>высокое качество товара</li>
            <li>разнообразие ассортимента</li>
            <li>европейское производство</li>        
          <ul>
        </div>
      </div>
    </div>
  
    <div itemprop="mainEntity" itemscope="" itemtype="" class="content-wrapper">
      <div itemprop="name" class="toggle">
      🔻 Какие товары из раздела <!--getH1--> самые популярные?</div>
      <div itemprop="acceptedAnswer" itemscope="" itemtype="">
        <div itemprop="text" class="text">
          Среди наших товаров, самые популярные:
          <?=$random_prods_faq?>
        </div>
      </div>
    </div>
  
    <div itemprop="mainEntity" itemscope="" itemtype="" class="content-wrapper">
      <div itemprop="name" class="toggle">
      🔻 В какие города Украины вы доставляете товар?</div>
      <div itemprop="acceptedAnswer" itemscope="" itemtype="">
        <div itemprop="text" class="text">
          Мы работаем с покупателями со всей Украины и с радостью доставим наш товар в любой город. Также во многих городах мы имеем собственные представительства. С более подробной информацией можно ознакомиться в разделе <a href="/contact/">Контакты</a> и <a href="/delivery">Доставка и оплата</a>.        </div>
      </div>
    </div>
  
    <div itemprop="mainEntity" itemscope="" itemtype="" class="content-wrapper">
      <div itemprop="name" class="toggle">
      🔻Я сомневаюсь, какие товары категории <!--getH1--> мне подойдут?</div>
      <div itemprop="acceptedAnswer" itemscope="" itemtype="" style="display: none;">
        <div itemprop="text" class="text">
          Наши менеджеры проконсультируют Вас про особенности любого интересующего товара по телефонам: (097) 011 61 39, (099) 777 70 02, (063) 52 23 111.
</div>
      </div>
    </div>
  </div>
  <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Почему стоит купить <!--getH1--> в интернет магазине tepliypol.com.ua?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Причины, по которым стоит оформить заказ на <!--getH1--> у именно нас: высокое качество товара разнообразие ассортимента европейское производство"
    }
  }, {
    "@type": "Question",
    "name": "Какие товары из раздела <!--getH1--> самые популярные?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<?=strip_tags($random_prods_faq)?>"
    }
  }, {
    "@type": "Question",
    "name": "В какие города Украины вы доставляете товар?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Мы работаем с покупателями со всей Украины и с радостью доставим наш товар в любой город. Также во многих городах мы имеем собственные представительства. С более подробной информацией можно ознакомиться в разделе Контакты и Доставка и оплата"
    }
  }, {
    "@type": "Question",
    "name": "Я сомневаюсь, какие товары категории <!--getH1--> мне подойдут?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Наши менеджеры проконсультируют Вас про особенности любого интересующего товара по телефонам: (097) 011 61 39, (099) 777 70 02, (063) 52 23 111."
    }
  }]
  }
</script>
	 <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?>
<?php echo $footer; ?>