<?php echo $header; ?>
<?php echo $column_left; ?>
<div class="container" >
  <!--seoshield_formulas--kartochka-->
  <?php echo $content_top; ?>
  <div class="row">
    <div class="col-xs-12 page--header">
      <div class="floor-header"><img src="img/page-title.png" alt=""></div>
      <h1><?php echo (isset($dg_prod_name_part) ? $dg_prod_name_part.' ' : '').$heading_title; ?></h1>
      <?php
        $jsonProd = array(
          "@context" => "https://schema.org",
          "@type" => "Product",
          "name" => "<!--getH1-->",
          "description" => "<!--desc-->"
        );
      ?>
      <ul class="breadcrumbs">
        <?php
            $ldJson = array(
              "@context" => "http://schema.org",
              "@type" => "BreadcrumbList",
              "itemListElement" => array()
            );
        ?>
        <?php foreach ($breadcrumbs as $key=>$breadcrumb) {
          if ($breadcrumb['text'] == 'Каталог') {
            continue;
          }
          if ($breadcrumb['text'] == 'Интернет-магазин') {
            $breadcrumb['text'] = 'Теплый пол';
            } else if ($breadcrumb['text'] == 'Інтернет-магазин') {
                $breadcrumb['text'] = 'Тепла підлога';
            }
          $ldJson['itemListElement'][] = array(
            "@type" => "ListItem",
            "position" => count($ldJson['itemListElement']) + 1,
            "name" => $breadcrumb['text'],
            "item" => $breadcrumb['href']             
          );
          ?>
        <? if($key + 1 === count($breadcrumbs)){ ?>
        <li>
          <span><?php echo $breadcrumb['text']; ?></span>
        </li>
        <? } else { ?>
        <li> 
            <a href="<?php echo $breadcrumb['href']; ?>">
              <span><?php echo $breadcrumb['text']; ?></span>
            </a>
        </li>
        <? } ?>
        <!--dg_crumb_url:<?php echo $breadcrumb['href']; ?>;;dg_crumb_name:<?php echo $breadcrumb['text']; ?>-->
        <!--dg_crumb_name:<?php echo $breadcrumb['text']; ?>;;dg_crumb_href:<?php echo $breadcrumb['href']; ?>-->
        <?php } ?>
      </ul>
      <script type="application/ld+json"><?= json_encode($ldJson, JSON_PRETTY_PRINT); ?></script>
    </div>
  </div>
  <div class="page--content">
    <div class="row">
      <? if ($thumb || $images) { ?>
      <div class="col-xs-12 col-sm-5 col-md-4">
        <?php if ($thumb) { ?>
          <?php
            $size = getimagesize($thumb);
            $imageMarkup = array(
              "@context" => "http://schema.org",
              "@type" => "ImageObject",
              "contentUrl" => "$thumb;",
              "name" => "$heading_title; фото",
              "description" => "<!--desc-->",
              "width" => isset($size[0]) ? "$size[0]" : "",
              "height" => isset($size[1]) ? "$size[1]" : "",
            );
          ?>
        <?php if (isset($imageMarkup)) { ?>
          <script type="application/ld+json"><?= json_encode($imageMarkup, JSON_PRETTY_PRINT); ?></script>
        <?php } ?>
        <div class="product--img">
          <?php $jsonProd['image'] = $thumb; ?>
          <?php if (isset($dg_prod_name_part)) { ?>
            <img src="<?php echo $thumb; ?>" title="<?php echo $dg_prod_name_part.' '.$heading_title; ?>" alt="<?php echo $dg_prod_name_part.' '.$heading_title; ?> фото" />
          <?php } else { ?>
            <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?> фото" />
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($images) { ?>
        <div class="product--imgs">
          <?php foreach ($images as $image) { ?>
          <div>
            <?php if (isset($dg_prod_name_part)) { ?>
              <img src="<?php echo $image['thumb']; ?>" title="<?php echo $dg_prod_name_part.' '.$heading_title; ?>" alt="<?php echo $dg_prod_name_part.' '.$heading_title; ?>" />
            <?php } else { ?>
              <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
            <?php } ?>
            </div>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
      <? } ?>

      <!--dg_cat_id:<?php echo $dg_prod_cat_id; ?>-->

      <div class="col-xs-12 col-sm-7 col-md-8 product--footer">
        <? if($is_siblings) { ?>
        <? if($products) { ?>
        <div class="btn btn-model-select" onclick="similar('<?php echo $product_id; ?>')">
        	<?=$select_sqrt;?> 
        	<div class="arrow">
        		<span></span>
        		<span></span>
        	</div>
        </div>
        <? } ?>
        <? } ?>
          <? if(!$upc) { ?>
        <div class="num">
          <div class="num-btn minus" onclick="countMinus(<?php echo $minimum; ?>, $(this));"></div>
          <p><?php echo $minimum; ?></p>
          <div class="num-btn plus" onclick="countPlus(<?=$max; ?>, $(this));"></div>

         <input name="quantity" value="<?php echo $minimum; ?>" type="hidden" />
        </div>
        <? } ?>
        <div class="product--price">
          <?php $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
            $jsonProd['offers'] = array("@type" => "Offer", "url" => $actual_link, "priceCurrency" => "UAH"); ?>
          <? if(!$upc) { ?>
          <p>Цена: <span id="prodPrice" data-price="<?php echo (int)$priceNum; ?>"><?=(!$special)?$priceTotal:$specialTotal; ?></span></p>
          <? } ?>
          <?php 
          if((int)$priceNum > 0){
            $jsonProd['offers']['price'] = (int)$priceNum;
          }elseif($upc){
            $jsonProd['offers']['price'] = html_entity_decode($upc);
          }
          if( $stock_status == 'В наличии' ) {
            $jsonProd['offers']['availability'] = "https://schema.org/InStock";
          } else {
            $jsonProd['offers']['availability'] = "https://schema.org/OutOfStock";
          } ?>
        </div>

        <button class="btn btn-yellow to-cart" onclick="cart.add('<?=$product_id;?>', $(this).parent().find('input[name=quantity]').val())">Купить</button>
      </div>

      <ul class="col-xs-12 col-sm-7 col-md-8">
      <li><span class="h2-style"><?php echo $tab_attribute; ?></span></li>
      <li class="tab--menu_one acteve">
         <?php if ($attribute_groups) { ?>
          <div class="row">
            <div class="col-xs-12">
              <div class="product-table">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                <div class="same-col-width">
                  <div>
                    <p><?php echo $attribute['name']; ?>:</p>
                  </div>
                  <div class="align-end">
                    <p><?php echo $attribute['text']; ?></p>
                  </div>
                </div>
                <? } ?>
                <? } ?>
              </div>
            </div>
          </div>
        <?php } ?>
      </li>
<!--       <li><span class="h2-style">Описание товара</span></li>
      <li  class="tab--menu_two">
        <div class="col-xs-12 col-sm-7 col-md-12 product--description">
        <div class="same-col-width">
          <? if($location) { ?>
          <div>
            <span class="i-style">Страна-производитель: <?=$location; ?></span>
            <span class="i-style">Производитель: <?php echo $manufacturer; ?></span>
          </div>
          <? } ?>
          <div class="text-right">
            <?php if (!$special) { ?>
            <p class=""><span style="color: #000;"><? if($is_siblings && $upc) { ?>от <?=html_entity_decode($upc);?> <?=$curRight;?><?=($mpn)?$mpn:'/м<sup>2</sup>';?><? } ?></span></p>
            <?php } else { ?>
            <p>
              <span><? if($is_siblings && $upc) { ?>от <?=html_entity_decode($upc);?> <?=$curRight;?><?=($mpn)?$mpn:'/м<sup>2</sup>';?><? } ?></span>
              <span style="text-decoration: line-through; margin-right: 10px;"><?php echo $price; ?><?=html_entity_decode($product['upc']);?></span>
              <span id="prodPrice" data-price="<?php echo (int)$specialNum; ?>"><?php echo $special; ?><?=html_entity_decode($upc);?></span></p>
            <? } ?>
          </div>
        </div>
        <? if(!empty(strip_tags($description))) { ?>
          <div itemprop="description"><?=$description; ?></div>
        <? } ?>
      </div>
      </li> -->
    </ul>  
    </div>
    <ul>  
          <li><div class="h2-style">Описание товара</div></li>
      <li  class="tab--menu_two">
        <div class="col-xs-12 col-sm-7 col-md-12 product--description">
        <div class="same-col-width">
          <? if($location) { ?>
          <div>
            <span class="i-style">Страна-производитель: <?=$location; ?></span>
            <span class="i-style">Производитель: <?php echo $manufacturer; ?></span>
          </div>
          <? } ?>
          <div class="text-right">
            <?php if (!$special) { ?>
            <p class=""><span style="color: #000;"><? if($is_siblings && $upc) { ?>от <?=html_entity_decode($upc);?> <?=$curRight;?><?=($mpn)?$mpn:'/м<sup>2</sup>';?><? } ?></span></p>
            <?php } else { ?>
            <p>
              <span><? if($is_siblings && $upc) { ?>от <?=html_entity_decode($upc);?> <?=$curRight;?><?=($mpn)?$mpn:'/м<sup>2</sup>';?><? } ?></span>
              <span style="text-decoration: line-through; margin-right: 10px;"><?php echo $price; ?><?=html_entity_decode($product['upc']);?></span>
              <span id="prodPrice" data-price="<?php echo (int)$specialNum; ?>"><?php echo $special; ?><?=html_entity_decode($upc);?></span></p>
            <? } ?>
          </div>
        </div>
        <? if(!empty(strip_tags($description))) { ?>
          <div class="product-description" itemprop="description"><!--seo_text_start--><?=$description; ?><!--seo_text_end--></div>
        <? } else { ?>
          <div class="product-description" itemprop="description"><!--seo_text_start--><!--seo_text_end--></div>
        <? } ?>
      </div>
      </li>
      </ul>
    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
    <script src="//yastatic.net/share2/share.js"></script>
    <div class="ya-share2" data-services="facebook,twitter,telegram"></div>
    <style type="text/css">
      .ya-share2__item {
        background: none !important;
      }
    </style>
  </div>
  <!--start_reviews-->
  <script src="https://kit.fontawesome.com/16100710b8.js" crossorigin="anonymous"></script>
  <?php if ($reviews) { ?>
    <div id="review_totals">
      <?= $reviewForm['review_rating'].': '.$rating.', '.$count_reviews; ?>
    </div>
    <?php  
      $jsonProd['aggregateRating'] = array("@type" => "AggregateRating", "ratingValue" => $rating, "reviewCount" => $count_reviews_num);
      $jsonProd['review'] = array();
    ?>
    <?php foreach ($reviews as $review) { 
      $jsonProd['review'][] = array(
        "@type" => "Review",
        "author" => $review['author'],
        "datePublished" => $review['date_added'],
        "reviewBody" => $review['text'],
        "reviewRating" => array(
          "@type" => "Rating",
          "bestRating" => "5",
          "ratingValue" => $review['rating'],
          "worstRating" => "1")
        );
    ?>
    <table class="table table-striped table-bordered">
      <tr>
        <td style="width: 50%;"><strong><?php echo $review['author']; ?></strong></td>
        <td class="text-right"><?php echo $review['date_added']; ?></td>
      </tr>
      <tr>
        <td colspan="2"><p><?php echo $review['text']; ?></p>
          <?php for ($i = 1; $i <= 5; $i++) { ?>
            <?php if ($review['rating'] < $i) { ?>
              <i style="color: #fbde1b;" class="fa-regular fa-star"></i>
            <?php } else { ?>
              <i style="color: #fbde1b;" class="fa-solid fa-star"></i>
            <?php } ?>
          <?php } ?></td>
      </tr>
    </table>
    <?php } ?>
    <div class="text-right"><?php echo $pagination; ?></div>
    <?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
    <?php } ?>
    <button id="show_review_form" class="btn btn-yellow" type="button"><?= $reviewForm['show_form'] ?></button>
    <div id="reviewsForm">
      <div id="reviewErrors"></div>
      <form method="POST" action="<?= $reviewSubmitUrl; ?>">
        <div class="form-group">
          <label for="reviewsForm_name"><?= $reviewForm['review_name'] ?></label>
          <input id="reviewsForm_name" name="name" class="form-control" type="text" placeholder="<?= $reviewForm['review_name_placeholder'] ?>">
        </div>
        <div class="form-group">
          <label for="reviewsForm_text"><?= $reviewForm['review_text'] ?></label>
          <textarea id="reviewsForm_text" name="text" class="form-control" rows="4" placeholder="<?= $reviewForm['review_text_placeholder'] ?>"></textarea>
        </div>
        <div class="form-group">
          <label><?= $reviewForm['review_rating'] ?></label>
          <input id="reviewsForm_rating" name="rating" value="0" type="hidden">
          <span id="input_rating">
            <?php for ($i = 1; $i <= 5; $i++): ?>
              <i style="color: #fbde1b;" class="fa-regular fa-star" data-index="<?= $i ?>"></i>
            <?php endfor; ?></td>
          </span>
        </div>
        <div id="reviewsFormFooter">
          <?= $captcha; ?>
          <button class="btn btn-yellow" type="submit"><?= $reviewForm['submit'] ?></button>
        </div>
      </form>
    </div>
    <!--end_reviews-->

  <?php echo $column_right; ?>
  <!--text_generation_product-->
</div>
<?php echo $content_bottom; ?>
<script>
  function countMinus(min, el) {
      let number = parseInt(el.parent().find("p").eq(0).text().trim());
      let curRight = $("#curRight").val();
      let prodPrice = $("#prodPrice").data("price");
      if(number > +min){
          el.parent().find("p").eq(0).text(number-1);
          el.parent().find("input[name=quantity]").eq(0).val(number-1);
          $(".product--price span").text(prodPrice * (number-1) + curRight);
      }
  }
  function countPlus(max, el) {
      let number = parseInt(el.parent().find("p").eq(0).text().trim());
      let curRight = $("#curRight").val();
      let prodPrice = $("#prodPrice").data("price");
      if(number < +max){
          el.parent().find("p").eq(0).text(number+1);
          el.parent().find("input[name=quantity]").eq(0).val(number+1);
          $(".product--price span").text(prodPrice * (number+1) + curRight);
      }
  }
  $(document).ready(function() {
    $.ajax({
      url : 'index.php?route=product/product/session_errors',
      type : 'get',
      success: function(response){
        if (response.message != ''){
          var messageClass = 'alert-danger';
          if (response.status == 'success'){
            messageClass = 'alert-success';
          } else {
            if(response.reviewName){
              $('#reviewsForm_name').val(response.reviewName);
            }
            if(response.reviewText){
              $('#reviewsForm_text').val(response.reviewText);
            }
          }
          $('#reviewErrors').append($('<div/>').addClass('alert ' + messageClass).text(response.message));
        }
      }
    });
    let stars = $("#input_rating i");
    stars.click(function(){
      let oldind = parseInt($("#reviewsForm_rating").val());
      let ind = $(this).data("index");
      $("#reviewsForm_rating").val(ind);
      if (oldind>ind) {
        for(let i=ind+1; i<=oldind; i++){
          $("#input_rating i:nth-child("+i+")").addClass("fa-regular").removeClass("fa-solid");
        }
      }
    });
    stars.mouseenter(function(){
      let rating = parseInt($("#reviewsForm_rating").val());
      let ind = $(this).data("index");
      for(let i=rating+1; i<=ind; i++){
        $("#input_rating i:nth-child("+i+")").addClass("fa-solid").removeClass("fa-regular"); 
      }
    });
    stars.mouseleave(function(){
      let rating = parseInt($("#reviewsForm_rating").val());
      let ind = $(this).data("index");
      for(let i=rating+1; i<=ind; i++){
        $("#input_rating i:nth-child("+i+")").addClass("fa-regular").removeClass("fa-solid");
      }
    });
    $('#show_review_form').click(function(){
      $('#reviewsForm').toggle();
    });
    $(".tab--menu_one").show();
    $("#tab-one").click(function() {
      if ($(".tab--menu_one").is(':visible')) {
         $(".tab--menu_one").hide();
      }
      else {
        $(".tab--menu_one").show();
      }
      $(".tab--menu_two").hide();
    })
     $("#tab-two").click(function() {
      if ($(".tab--menu_two").is(':visible')) {
         $(".tab--menu_two").hide();
      }
      else {
        $(".tab--menu_two").show();
      }
      $(".tab--menu_one").hide();
    })
  })

</script>
<script type="application/ld+json"><?= json_encode($jsonProd, JSON_PRETTY_PRINT); ?></script>
<?php echo $footer; ?>
