<?php
class ModelCheckoutCublCheckout extends Model {

    public function addOrder($data) {
        /*return print_r($data);
        die();*/
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET
        invoice_prefix = 'cubl-', 
        store_id = '" . (int)$data['store_id'] . "', 
        store_name = '" . $this->db->escape($data['store_name']) . "', 
        store_url = '" . $this->db->escape($data['store_url']) . "', 
        customer_id = '" . (int)0 . "', 
        customer_group_id = '" . (int)1 . "', 
        firstname = '" . $this->db->escape($data['firstname']) . "', 
        lastname = '" . $this->db->escape($data['lastname']) . "', 
        email = 'floor2@apricode.tech', 
        telephone = '" . $this->db->escape($data['tel']) . "', 
        fax = '', 
        custom_field = '', 
        payment_firstname = '" . $this->db->escape($data['firstname']) . "', 
        payment_lastname = '" . $this->db->escape($data['lastname']) . "', 
        payment_company = '', 
        payment_address_1 = '" . $this->db->escape($data['address']) . "', 
        payment_address_2 = '', 
        payment_city = '" . $this->db->escape($data['city']) . "', 
        payment_postcode = '', 
        payment_country = 'Страна', 
        payment_country_id = '" . (int)$data['country_id'] . "', 
        payment_zone = 'Область', 
        payment_zone_id = '" . (int)$data['zone_id'] . "', 
        payment_address_format = '', 
        payment_custom_field = '', 
        payment_method = '" . $this->db->escape($data['payment_title']) . "', 
        payment_code = '" . $this->db->escape($data['payment_code']) . "', 
        shipping_firstname = '" . $this->db->escape($data['firstname']) . "', 
        shipping_lastname = '" . $this->db->escape($data['lastname']) . "', 
        shipping_company = '', 
        shipping_address_1 = '" . $this->db->escape($data['address']) . "', 
        shipping_address_2 = '', 
        shipping_city = '" . $this->db->escape($data['city']) . "', 
        shipping_postcode = '', 
        shipping_country = 'Страна', 
        shipping_country_id = '" . (int)$data['country_id'] . "', 
        shipping_zone = 'Область', 
        shipping_zone_id = '" . (int)$data['zone_id'] . "', 
        shipping_address_format = '', 
        shipping_custom_field = '', 
        shipping_method = '" . $this->db->escape($data['shipping_title']) . "',
		shipping_code = '" . $this->db->escape($data['shipping_code']) . "',
        comment = '" . $this->db->escape($data['comment']) . "', 
        total = '" . (float)$data['total_order'] . "', 
        affiliate_id = '', 
        commission = '', 
        marketing_id = '', 
        tracking = '', 
        language_id = '" . (int)$data['language_id'] . "', 
        currency_id = '" . (int)$data['currency_id'] . "', 
        order_status_id = '". $this->config->get('config_order_status_id')."',
        currency_code = '" . $this->db->escape($data['currency_code']) . "', 
        currency_value = '" . (float)$data['currency_value'] . "', 
        ip = '" . $this->db->escape($data['ip']) . "', 
        forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', 
        user_agent = '" . $this->db->escape($data['user_agent']) . "', 
        accept_language = '" . $this->db->escape($data['accept_language']) . "', 
        date_added = NOW(), 
        date_modified = NOW()");

        $order_id = $this->db->getLastId();

        if (isset($data['products'])) {
            foreach ($data['products'] as $product) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");

                $order_product_id = $this->db->getLastId();

                foreach ($product['option'] as $option) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET 
							order_id = '" . (int)$order_id . "',
							order_product_id = '" . (int)$order_product_id . "',
							product_option_id = '" . (int)$option['product_option_id'] . "',
							product_option_value_id = '" . (int)$option['product_option_value_id'] . "',
							name = '" . $this->db->escape($option['name']) . "',
							`value` = '" . $this->db->escape($option['value']) . "',
							`type` = '" . $this->db->escape($option['type']) . "'");
                    $this->db->query("INSERT INTO " . DB_PREFIX . "newfastorder_product_option SET 
								order_id 				= '". (int)$order_id."',
								order_product_id 		= '". (int)$product['product_id']."',
								product_option_id 		= '". (int)$option['product_option_id'] . "',
								product_option_value_id = '". (int)$option['product_option_value_id'] . "',
								type 					= '". $this->db->escape($option['type']) ."',
								name 					= '". $this->db->escape($option['name']) ."',
								`value` 				= '". $this->db->escape($option['value']) . "'");
                }
            }
        }
        if (isset($data['total']['totals'])) {
            foreach ($data['total']['totals'] as $total) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
            }
        }

        return $order_id;

    }

}