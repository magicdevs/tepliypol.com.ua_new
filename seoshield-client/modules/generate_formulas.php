<?php
/***
PLACE THIS FILE INTO /seoshield-client/modules/ DIRECTORY
*/
//error_reporting(E_ERROR);

if(!defined("SEOSHIELD_ROOT_PATH"))define("SEOSHIELD_ROOT_PATH",rtrim(realpath(dirname(__FILE__)),"/"));

class SeoShieldModule_generate_formulas_config extends SeoShieldModule_generate_formulas
{
	public function setup_variables($out_html){
		if (strpos($_SERVER['REQUEST_URI'], '/ua/') === false){
			$out_html = str_replace('</head>', '<!--seoshield--version--site--rus-->'."\n".'</head>', $out_html);
		} elseif (strpos($_SERVER['REQUEST_URI'], '/ua/') !== false) {
			$out_html = str_replace('</head>', '<!--seoshield--version--site--ukr-->'."\n".'</head>', $out_html);
		}

			if (strpos($out_html, '<!--dg_selected_filter_title') !== false){
				preg_match_all('#<!--dg_selected_filter_title:(?<filter_title>.*?);;dg_selected_filter_name:(?<filter_name>.*?)-->#s', $out_html, $finder);
				if (isset($finder[0]) && !empty($finder[0])){
					if (count($finder[0]) > 2){
						$out_html = $this->set_robots_noindex_nofollow($out_html);
					} elseif (count(array_unique($finder['filter_title'])) < count($finder['filter_title'])) {
						$out_html = $this->set_robots_noindex_nofollow($out_html);
					} 
					$out_html = str_replace('</head>', '<!--seoshield_formulas--fil-tr-->'."\n".'</head>', $out_html);
				}
				unset($finder);
			}

			if (strpos($out_html, '<!--isset_listing_page-->') !== false && strpos($out_html, '<!--seoshield_formulas--fil-tr-->') === false){
				$out_html = str_replace('</head>', '<!--seoshield_formulas--kategoriya-->'."\n".'</head>', $out_html);
			}

			$mass_services = array('/about_us', '/montazh', '/akcija', '/contact/');
			$mass_servicesUk = array('/ua/about_us', '/ua/montazh', '/ua/akcija', '/ua/contact/');
			if (in_array($_SERVER['REQUEST_URI'], $mass_services)){
				$out_html = str_replace('</head>', '<!--seoshield_formulas--sluzhebnye-->'."\n".'</head>', $out_html);
			} elseif(in_array($_SERVER['REQUEST_URI'], $mass_servicesUk)){
				$out_html = str_replace('</head>', '<!--seoshield_formulas--sluzhebnye-->'."\n".'</head>', $out_html);
			}

			$mass_services = array('/about_us', '/montazh', '/akcija', '/contact/');
			if (in_array($_SERVER['REQUEST_URI'], $mass_services)){
				$out_html = str_replace('</head>', '<!--seoshield_formulas--sluzhebnye-->'."\n".'</head>', $out_html);
			}

			if (isset($_GET['page'])){
				$out_html = str_replace('</head>', '<!--ss_pagination_page-->'."\n".'</head>', $out_html);
			}

			// preg_match('#<!--dg_cat_id:(.*?)-->#s', $out_html, $prod_cat_id);
			// if (isset($prod_cat_id[1]) && !empty($prod_cat_id[1])){
			// 	$prod_cat_arr = array(
			// 		'65' => '<!--seoshield_formulas--kabel-nyy-teplyy-pol-v-styazhku-->',
			// 		'64' => '<!--seoshield_formulas--tonkiy-teplyy-pol-pod-plitku-->',
			// 		'63' => '<!--seoshield_formulas--nagrevatel-nye-maty-pod-plitku-->',
			// 		'62' => '<!--seoshield_formulas--teplyy-pol-pod-laminat-->',
			// 		'69' => '<!--seoshield_formulas--analogovye-i-cifrovye-termostaty-dlya-teplogo-pola-->',
			// 		'70' => '<!--seoshield_formulas--programmiruemye-termostaty-dlya-teplogo-pola-->',
			// 		'68' => '<!--seoshield_formulas--obogrev-vodostokov-->',
			// 		'80' => '<!--seoshield_formulas--nagrevatel-nye-paneli-->',
			// 	);

			// 	$prod_cat_id = $prod_cat_id[1];
			// 	if (isset($prod_cat_arr[$prod_cat_id])){
			// 		$out_html = str_replace('</head>', $prod_cat_arr[$prod_cat_id]."\n".'</head>', $out_html);
			// 	}
			// }
			
		return $out_html;
	}

	function set_robots_noindex_nofollow($out_html){
		if (strpos($out_html, '<meta name="robots" content="noindex, nofollow" />') === false 
			&& strpos($out_html, "<meta name='robots' content='noindex, nofollow' />") === false){
			if (preg_match('#<meta[^>]*?name=[\'\"]?robots[\'\"]?[^>]*?>#si', $out_html)){
				$out_html = preg_replace('#<meta[^>]*?name=[\'\"]?robots[\'\"]?[^>]*?>#si', '<meta name="robots" content="noindex, nofollow" />', $out_html);
			} else {
				$out_html = str_replace('</head>', '<meta name="robots" content="noindex, nofollow" />'."\n".'</head>', $out_html);
			}
		}
		return $out_html;
	}

	function make_filters_visible_for_robots($out_html){
		if (preg_match('#<link[^>]*?rel=[\'\"]?canonical[\'\"\s]?[^>]*?>#s', $out_html)){
			$out_html = preg_replace('#<link[^>]*?rel=[\'\"]?canonical[\'\"\s]?[^>]*?>#s', '', $out_html);
		}

		// --- Если есть роботс с контентом "nofollow" в любом регистре или его нет вовсе --- // 
		if (preg_match('#<meta[^>]*?name=[\'\"]?robots[\'\"]?[^>]*?content=[\'\"]?[^>]*?noindex[^>]*?>#si', $out_html) || !preg_match('#<meta[^>]*?name=[\'\"]?robots[\'\"]?[^>]*?>#si', $out_html)){
			$out_html = preg_replace('#<meta[^>]*?name=[\'\"]?robots[\'\"]?[^>]*?>#s', '', $out_html);
			$out_html = str_replace('</head>', '<meta name="robots" content="index, follow" />'."\n".'</head>', $out_html);
		}

		return $out_html;
	}

	function parentCat_formulas_generation($out_html)
	{
		$parent_cat = '';
		$parent_cat_url = "";
		$parent_cat_name = "";
		preg_match_all('#<!--dg_crumb_url:(.*?);;dg_crumb_name:(.*?)-->#s', $out_html, $finder);
		if (isset($finder[0]) && !empty($finder[0])){

			$parent_cat_name = array_pop($finder[2]);
			$parent_cat_url = array_pop($finder[1]);
			$parent_cat_name = array_pop($finder[2]);
			$parent_cat_url = array_pop($finder[1]);

			$parent_static_data = $this->get_static_data_by_uri($parent_cat_url);

			if (!empty($parent_static_data[2])){
				$parent_cat = $parent_static_data[2];
			} else {
				$parent_cat = $parent_cat_name;
			}
		}
		unset($finder);

		return $parent_cat;
	}

	function h1_formulas_generation($out_html)
	{
		$curr_h1 = "";
		preg_match('#<h1[^>]*?>(.*?)<\/h1>#s', $out_html, $finder);
		if (isset($finder[0]) && !empty($finder[1])){
			$curr_h1 = strip_tags($finder[1]);
			$curr_h1 = trim($curr_h1);
		}

		if (file_exists(SEOSHIELD_ROOT_PATH."/data/static_meta.cache.php")){
			if(!isset($GLOBALS['SEOSHIELD_DATA']['static_meta']))
				$this->get_static_meta();

			if (isset($GLOBALS['SEOSHIELD_DATA']['static_meta']['h1'])){
				$curr_h1 = $GLOBALS['SEOSHIELD_DATA']['static_meta']['h1'];
			}
		}

		return $curr_h1;
	}

}