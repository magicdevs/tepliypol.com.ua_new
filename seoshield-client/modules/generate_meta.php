<?php
/*
* модуль подменяет динамические мета данные
*/

if (!defined('SEOSHIELD_ROOT_PATH')) {
    define('SEOSHIELD_ROOT_PATH', rtrim(realpath(dirname(__FILE__)), '/'));
}

class SeoShieldModule_generate_meta_config extends SeoShieldModule_generate_meta
{
    public function html_out_buffer($out_html)
    {
        if ((strpos($out_html, '<!--seoshield_formulas--sluzhebnye-->') !== false 
        || strpos($out_html, '<!--seoshield_formulas--novost-->') !== false
        || strpos($out_html, '<!--ss_pagination_page-->') !== false)
        && preg_match('#<meta[^>]*?name=[\'\"]?description[^>]*?>#s', $out_html)){
            $out_html = preg_replace('#<meta[^>]*?name=[\'\"]?description[^>]*?>#s', '', $out_html);
        }

        if (isset($_GET['page'])){
            if (preg_match('#<link[^>]*?rel=[\'\"]?canonical[\'\"\s]?[^>]*?>#s', $out_html)){
                $out_html = preg_replace('#<link[^>]*?rel=[\'\"]?canonical[\'\"\s]?[^>]*?>#s', '', $out_html);
            }
        }

        $out_html = str_replace('</head>', "\n".'<link href="/seoshield-client/shield.css?'.microtime(true).'" rel="stylesheet">'."\n".'</head>', $out_html);

        // $out_html = str_replace('</footer>', '<!--footers_block--></footer>', $out_html);
        // $out_html=$this->replace_page_h1($out_html,$GLOBALS['SEOSHIELD_CONFIG']['page_h1']);
        // $out_html=$this->replace_page_meta_description($out_html,$GLOBALS['SEOSHIELD_CONFIG']['page_meta_description']);
        // $out_html=$this->replace_page_title($out_html,$GLOBALS['SEOSHIELD_CONFIG']['page_title']);
        // $out_html=$this->replace_page_meta_keywords($out_html,$GLOBALS['SEOSHIELD_CONFIG']['page_meta_keywords']);
        $out_html = $this->markup($out_html);
        $out_html = $this->webpImages($out_html);
        $out_html = $this->pagespeed($out_html);
        $out_html = $this->htmlSitemap($out_html);
        $out_html = $this->filtersSitemap($out_html);
        $out_html = $this->text_generation_on_filters($out_html);
        $out_html = $this->text_generation_on_prods($out_html);
        if (strpos($_SERVER['REQUEST_URI'], '/statji/') !== false && $_SERVER['REQUEST_URI'] !== '/statji/') {
            $out_html = $this->blockLangSwitchOnArticles($out_html);
        }
        return $out_html;
    }

    private function getPageH1($out_html)
    {
        $curr_h1 = "";
        preg_match('#<h1[^>]*?>(.*?)<\/h1>#s', $out_html, $finder);
        if (isset($finder[0]) && !empty($finder[1])){
            $curr_h1 = strip_tags($finder[1]);
            $curr_h1 = trim($curr_h1);
        }

        if (file_exists(SEOSHIELD_ROOT_PATH."/data/static_meta.cache.php")){
            if(!isset($GLOBALS['SEOSHIELD_DATA']['static_meta'])){
                $this->get_static_meta();
            }

            if (isset($GLOBALS['SEOSHIELD_DATA']['static_meta']['h1'])){
                $curr_h1 = $GLOBALS['SEOSHIELD_DATA']['static_meta']['h1'];
            }
        }

        return $curr_h1;
    }


    private function markup($out_html)
    {   
        preg_match('#<meta[^>]*?name=[\'\"]?description[^>]*?content=[\'\"](.*?)[\'\"][^>]*?>#s', $out_html, $finder);
        $description = '';
        
        if (isset($finder[0]) && !empty($finder[1])){
            $description = $finder[1];
        }

        if (strpos($out_html, '<!--desc-->') !== false && !empty($description)) {
            $out_html = str_replace('<!--desc-->', $description, $out_html);     
        }
        if (strpos($out_html, '<!--getH1-->') !== false) {
            $out_html = str_replace('<!--getH1-->', $this->getPageH1($out_html), $out_html);
        }
        preg_match_all('#<!--dg_prod_in_listing:(.*?)-->#s', $out_html, $prod_finders);
        if (isset($prod_finders[0]) && !empty($prod_finders[0])){
$sem_razm_category = '<script type="application/ld+json">
{
  "@context":"http://schema.org",
  "@type":"ItemList",
  "itemListElement":[';
                
                $bufer_carusel_arr = array();
              foreach ($prod_finders[1] as $prod_key => $prod_url) {
                $pos = $prod_key + 1;
                  $bufer_carusel_arr[] = 
'{
  "@type":"ListItem",
  "position":"'.$pos.'",
  "url":"'.$prod_url.'"
}';
                if ($pos > 2){
                    break;
                }

              }

              $sem_razm_category .= implode(',', $bufer_carusel_arr);
                
            $sem_razm_category .= ']
            }
            </script>';

            $out_html = str_replace('</head>', $sem_razm_category.'</head>', $out_html);
        }

        return $out_html;
    }
    private function webpImages($out_html){
        $out_html = preg_replace_callback('#<img[^>]+src=[\'"]([^\'"]+)[\'"]#s', function($match){
        if (strpos($match[1], '.webp') === false && strpos($match[1], '.svg') === false){
          $imgPath = $match[1];
          $imgPath = explode($_SERVER['HTTP_HOST'], $imgPath);
          $imgPath = end($imgPath);
          if (!file_exists($_SERVER['DOCUMENT_ROOT'].$imgPath.'.webp')){
            return $match[0];
          }
          return str_replace($match[1], $match[1].'.webp', $match[0]);
      }
      return $match[0];
    }, $out_html);
    return $out_html;
    }
    private function pagespeed($out_html)
    {
        $out_html = str_replace('//netdna.bootstrapcdn.com/bootstrap/3.3.2/css', "css", $out_html);
        $out_html = str_replace('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css', 'css', $out_html);
        $out_html = str_replace('<script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>', '<script src="js/jquery-3.3.1.min.js"></script>', $out_html);
        $out_html = str_replace('/style.css','/style.min.css', $out_html);
        if ( strpos($_SERVER['HTTP_USER_AGENT'],'Chrome-Lighthouse')!==false || strpos($_SERVER['HTTP_USER_AGENT'], 'Page Speed')!==false) {
            $out_html = str_replace(' <link rel="stylesheet" href="plugins/owl.theme.default.min.css">', "", $out_html);
            $out_html = preg_replace('#<!-- Yandex.Metrika counter -->.*?<\/script>#s', '', $out_html);
            $out_html = str_replace('https://www.googletagmanager.com/gtm.js', '', $out_html);
            $tmpOutHtml = $out_html;
            $tmpOutHtml = preg_replace('#<!--[\s\S]*?-->#s', '', $tmpOutHtml);
            $scripts = array();
            preg_match_all('#<script[^>]*?>.*?<\/script>#s', $tmpOutHtml, $match);
            foreach ($match as $m) {
                $tmpOutHtml = str_replace($m[0], $m[0], $tmpOutHtml);
            }
            preg_replace_callback('#<script[^>]*?>.*?<\/script>#s', function($m) use (&$scripts){
                $scripts[] = $m[0];
                return '';
            }, $tmpOutHtml);
            $out_html = str_replace($scripts, '', $out_html);
            $out_html = str_replace('</body>', implode('', $scripts).'</body>', $out_html);
            }
        return $out_html;
    }

    private function htmlSitemap($out_html) {
        $curr_uri = isset($GLOBALS['SEOSHIELD_CONFIG']['page_uri_alt']) ? $GLOBALS['SEOSHIELD_CONFIG']['page_uri_alt'] : $_SERVER['REQUEST_URI'];
        if ($curr_uri === '/sitemap/') {
            $csv_file_name = SEOSHIELD_ROOT_PATH.'/data/sitemap.csv';
        } elseif ($curr_uri === '/ua/sitemap/') {
            $csv_file_name = SEOSHIELD_ROOT_PATH.'/data/sitemap_ua.csv';
        } else {
            return $out_html;
        }
        if (file_exists($csv_file_name)) {
            $delimiter = ';';
            $enclosure = '"';
            $files_lines = file($csv_file_name);
            if (count($files_lines)>0) {
                $html = '<div class="col-sm-12">'."\n".
                            '<ul>'."\n";

                $oldLevel = 0;
                $level = 0;
                foreach ($files_lines as $key=>$line) {
                    if (function_exists('str_getcsv')) {
                        $data = str_getcsv(trim($line), $delimiter, $enclosure);
                    } else {
                        $data = explode($delimiter, trim($line));
                    }
                    $level = 0;
                    if (is_array($data) && count($data)>0) {
                        while (empty(trim($data[$level]))) { $level++; }
                        if ($level > $oldLevel) {
                            $html .= '<ul>'."\n";
                        } elseif ($level < $oldLevel) {
                            $html .= '</li>'."\n";
                            for($i=$level;$i<$oldLevel;$i++){
                                $html .= '</ul>'."\n".'</li>'."\n";
                            }
                        } else {
                            if ($key>0) {
                                $html .= '</li>'."\n";
                            }
                        }
                        $html .= sprintf('<li><a href="%s">%s</a>', $data[$level+1], $data[$level])."\n";
                        $oldLevel = $level;
                    }
                }
                $html .= '</li>'."\n";
                for ($i=$level; $i>0; $i--){
                    $html .= '</ul>'."\n".'</li>'."\n";
                }
                $html .= '</ul>'."\n".'</div>'."\n";
                $out_html = preg_replace('#(<!--ss_sitemap_replace_start-->)(.*)(<!--ss_sitemap_replace_end-->)#s', '$1'.$html.'$3', $out_html);
            }
        }
        return $out_html;
    }

    private function filtersSitemap($out_html){
        $curr_uri = $_SERVER['REQUEST_URI'];
        $curr_host = $_SERVER['HTTP_HOST'];

        // --- В этой проверке нас интересуют ИСКЛЮЧИТЕЛЬНО канонические страницы категорий, исключаем все фильтры, пагинацию, сортировку --- //
        if (strpos($out_html, '<!--isset_listing_page-->') !== false && strpos($out_html, 'dg_selected_filter_title:') === false && !preg_match('#\/[\d\.]+-[\d\.]+\/#', $curr_uri) &&
            strpos($curr_uri, '/where/') === false && strpos($curr_uri, '?') === false){

            $md5_uri = md5($curr_uri);
            $currentTime = time();
            $create_filters_table = $GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_query('CREATE TABLE IF NOT EXISTS `ss_filters_for_sitemap`(
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `page_url_hash` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                `filter_url` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                `time_created` int(11) NOT NULL,
                PRIMARY KEY (`id`),
                INDEX (`page_url_hash`)
            )');

            if (!$create_filters_table){
                $out_html = str_replace('</body>', '<!--SEOSHIELD_INFO:SQL_ERROR:'.var_export($GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_error(), true).'--></body>', $out_html);
            } else {
                $arr_filters_urls = array();
                $insert_values_string = "";
                preg_match_all('#<!--dg_filter_href:(.*?)-->#s', $out_html, $finder);
                if (isset($finder[1]) && !empty($finder[1])){
                    $insert_values_string .= "VALUES ";
                    foreach ($finder[1] as $single_filter_uri) {
                        $arr_filter_urls[] = "('".$md5_uri."', '" . $single_filter_uri . "', ".$currentTime.")";
                    }
                    $insert_values_string .= implode(',', $arr_filter_urls);
                }
                unset($finder);

                $result_if_exist = $GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_query("SELECT time_created FROM `ss_filters_for_sitemap` WHERE page_url_hash = '".$md5_uri."' limit 1");

                if ($result_if_exist !== false && $insert_values_string != ""){
                    $updateExisting = false;
                    if ($GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_num_rows($result_if_exist) != 0 ){
                        $resultRow = $GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_fetch_assoc($result_if_exist);
                        if (time() - $resultRow['time_created'] >= 86400*7){
                            $updateExisting = true;
                            $GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_query("DELETE FROM ss_filters_for_sitemap WHERE page_url_hash = '".$md5_uri."'");
                        }
                    }
                    if ($GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_num_rows($result_if_exist) == 0 || $updateExisting){
                        $add_url_hash_query = $GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_query("INSERT ss_filters_for_sitemap(page_url_hash, filter_url, time_created) ".$insert_values_string);
                    }
                }
            }
        }

        return $out_html;
    }

    private function text_generation_on_filters($out_html){
        if (!isset($_GET['sort']) && !isset($_GET['page']) && strpos($out_html, '<!--dg_selected_filter_title:') !== false){
            $parent_cat = '';
            preg_match_all('#<!--dg_crumb_name:(?<c_name>.*?);;dg_crumb_href:(?<c_href>.*?)-->#s', $out_html, $crumb_inf);
            if (isset($crumb_inf[0]) && !empty($crumb_inf[0])){
                $static_parent = explode('?', $_SERVER['REQUEST_URI']);
                $static_h1 = $this->get_static_data_by_uri($static_parent[0]);
                 if (!empty($static_h1[2])){
                    $parent_cat = $static_h1[2];
                 } else {
                    $parent_cat = array_pop($crumb_inf['c_name']);
                    $parent_cat = array_pop($crumb_inf['c_name']);
                 }
            }
            if (strpos($_SERVER['REQUEST_URI'], '/ua') === false) {
                $ss_content = seo_shield_init_generate_content(array(
                'type' => 'filters',
                'dynamic_markers' => array(
                    '{GET_PAGE_H1}' => $this->get_current_h1_only_text_value($out_html),
                    '{Category_H1} ' => $parent_cat,
                    ),
                ));
            } else {
                $ss_content = seo_shield_init_generate_content(array(
                'type' => 'filters_ua',
                'dynamic_markers' => array(
                    '{GET_PAGE_H1}' => $this->get_current_h1_only_text_value($out_html),
                    '{Category_H1} ' => $parent_cat,
                    ),
                ));
            }
            $ss_content->start();
            $text_generate_tpl = $ss_content->get_start();

            $seo_text = false;
            if (file_exists(SEOSHIELD_ROOT_PATH."/data/static_meta.cache.php")){
                if(!isset($GLOBALS['SEOSHIELD_DATA']['static_meta']))
                    $this->get_static_meta();

                if (isset($GLOBALS['SEOSHIELD_DATA']['static_meta']['content'])){
                    $seo_text = true;
                }
            }
            if (!$seo_text){
                $out_html = str_replace('<!--text_generation_filters-->', '<div class="text_gen_on_filter" style="width: 95%; text-align: justify;">'.$text_generate_tpl.'</div>', $out_html);
            } 
        }
        return $out_html;
    }

    private function text_generation_on_prods($out_html){
        if (strpos($out_html, '<!--text_generation_product-->') !== false){
            $parent_cat = '';
            preg_match_all('#<!--dg_crumb_name:(?<c_name>.*?);;dg_crumb_href:(?<c_href>.*?)-->#s', $out_html, $crumb_inf);
            if (isset($crumb_inf[0]) && !empty($crumb_inf[0])){
                $static_parent = explode('?', $_SERVER['REQUEST_URI']);
                $static_h1 = $this->get_static_data_by_uri($static_parent[0]);
                 if (!empty($static_h1[2])){
                    $parent_cat = $static_h1[2];
                 } else {
                    $parent_cat = array_pop($crumb_inf['c_name']);
                    $parent_cat = array_pop($crumb_inf['c_name']);
                 }
            }
            if (strpos($_SERVER['REQUEST_URI'], '/ua') === false) {
                $ss_content = seo_shield_init_generate_content(array(
                'type' => 'products',
                'dynamic_markers' => array(
                    '{GET_PAGE_H1}' => $this->get_current_h1_only_text_value($out_html),
                    '{Main_Category_H1} ' => $parent_cat,
                    ),
                ));
            } else {
                $ss_content = seo_shield_init_generate_content(array(
                'type' => 'products_ua',
                'dynamic_markers' => array(
                    '{GET_PAGE_H1}' => $this->get_current_h1_only_text_value($out_html),
                    '{Main_Category_H1} ' => $parent_cat,
                    ),
                ));
            }
            $ss_content->start();
            $text_generate_tpl = $ss_content->get_start();

            $out_html = str_replace('<!--text_generation_product-->', '<div class="text_gen_on_prods" style="text-align: justify; float: left;">'.$text_generate_tpl.'</div>', $out_html); 
        }

        return $out_html;
    }

    private function blockLangSwitchOnArticles($out_html){
        if (preg_match('#<div[^>]*id="lang"[^>]*>#', $out_html, $m)) {
            if (isset($m[0]) && !empty($m[0])) {
                $replaceStr = str_replace('>', ' style="display:none;">', $m[0]);
                $out_html = str_replace($m[0], $replaceStr, $out_html);
            }
        }
        return $out_html;
    }
}
