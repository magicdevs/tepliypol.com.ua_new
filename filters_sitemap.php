<?php
header('Content-type: application/xml');
if (file_exists(__DIR__.'/seoshield-client/main.php')){
    include_once(__DIR__.'/seoshield-client/main.php');
    if(function_exists('seo_shield_start_cms')){
        seo_shield_start_cms();
    }
    $query = $GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_query("SELECT filter_url FROM `ss_filters_for_sitemap`");
    $urls = [];
    while($row = $GLOBALS['SEOSHIELD_CONFIG']['mysql']->mysql_fetch_array($query)){
        $urls[] = $row['filter_url'];
    }

    $urls = array_unique($urls);

    $output = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    foreach ($urls as $filter_url) {
        $output .= "\n\t".'<url>'."\n\t\t".'<loc>'.$filter_url.'</loc>'."\n\t".'</url>';
    }
    $output .= "\n".'</urlset>';
    echo $output;
    exit();
}
