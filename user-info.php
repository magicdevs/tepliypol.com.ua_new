<?php 
if (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR'])) {
    $userIP = $_SERVER['REMOTE_ADDR'];
    $apiURL = 'http://ip-api.com/json/'.$userIP;  
    $ch = curl_init($apiURL); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    $apiResponse = curl_exec($ch); 
    curl_close($ch);
    $ipData = json_decode($apiResponse, true);
    if (isset($ipData['country']) && $ipData['country'] == 'Ukraine') {
        echo 'Ukraine';
    } 
}
