function IMCallMeAskMe_getQueryParam(name){var results=new RegExp('[\\?&]'+name+'=([^&#]*)').exec(window.location.href.split('#')[0]);if(results)
return results[1];else
return'';}
function IMCallMeAskMe_collectParams(form){var sendArray=form.serializeArray(),queryArray=['utm_source','utm_medium','utm_campaign','utm_content','utm_term'];for(var cnt=0;cnt<queryArray.length;cnt++){sendArray.push({name:queryArray[cnt],value:IMCallMeAskMe_getQueryParam(queryArray[cnt])});}
return sendArray;}